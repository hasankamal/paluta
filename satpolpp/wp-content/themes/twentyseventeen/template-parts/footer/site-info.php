<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<footer class="footer-distributed">
<div class="footer-left">

<div style="padding-bottom:10px">
	PETA
	</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d498.5576361103282!2d99.61988009559344!3d1.4945152165848195!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302c6a0ed3f875a9%3A0x2175cf3aebc6d323!2sKantor+Satpol+PP+(Damkar-Linmas)!5e0!3m2!1sid!2sid!4v1563246931435!5m2!1sid!2sid" width="450" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
<div class="footer-center">

	<div style="padding-left:12px">
	KONTAK
	</div>
				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>Jl. Gunung Tua-Padang Sidempuan, Km. 3</span> Gunung Tua, Paluta</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>(0635) 5110003</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">satpolpppaluta@gmail.com</a></p>
				</div>

			</div>
	<!--
	<div class="footer-right">

				<p class="footer-company-about">
					Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>

			</div>
-->
</footer>