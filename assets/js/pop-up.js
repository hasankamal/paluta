        jQuery(function($){
        $('document').ready(function(){
	       $('#backgroundPopup').fadeIn(1000);
	       $('#toPopup').fadeIn(1100);

           setTimeout(function(){
                $('#backgroundPopup').fadeOut(1000);
                $('#toPopup').fadeOut(1100);
            },10000);
        });
        $('#close,#backgroundPopup').click(function(){
            $('#toPopup').fadeOut();
            $('#backgroundPopup').fadeOut();
        });
        $(this).keyup(function(e){
            if(e.which == 27){
                $('#toPopup').fadeOut();
                $('#backgroundPopup').fadeOut();
            }   
        });
    });