<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Enkripsi extends CI_Controller {
 
    /**
     * Enkripsi Page for this controller.
     *
     * digunakan untuk membuat library enkripsi codeigniter
     */
 
    public function __construct() {
        parent::__construct();
        $this->load->library('encryption');
    }
 
 
    public function index()
    {
        //sebuah string yang akan kita enkripsi
        $string = "msahidinkey";
        $encript =  $this->encryption->encrypt($string);
 
        //tampilkan hasilnya
        echo $encript;
    }
}