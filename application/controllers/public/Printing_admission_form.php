<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CMS Sekolahku | CMS (Content Management System) dan PPDB/PMB Online GRATIS 
 * untuk sekolah SD/Sederajat, SMP/Sederajat, SMA/Sederajat, dan Perguruan Tinggi
 * @version    2.0.0
 * @author     Anton Sofyan | https://facebook.com/antonsofyan | 4ntonsofyan@gmail.com | 0857 5988 8922
 * @copyright  (c) 2014-2017
 * @link       http://sekolahku.web.id
 * @since      Version 2.0.0
 *
 * PERINGATAN :
 * 1. TIDAK DIPERKENANKAN MEMPERJUALBELIKAN APLIKASI INI TANPA SEIZIN DARI PIHAK PENGEMBANG APLIKASI.
 * 2. TIDAK DIPERKENANKAN MENGHAPUS KODE SUMBER APLIKASI.
 * 3. TIDAK MENYERTAKAN LINK KOMERSIL (JASA LAYANAN HOSTING DAN DOMAIN) YANG MENGUNTUNGKAN SEPIHAK.
 */

class Printing_admission_form extends Public_Controller {

	/**
	 * Constructor
	 * @access  public
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model('m_registrants');
		$this->pk = M_registrants::$pk;
		$this->table = M_registrants::$table;
	}

	/**
	 * Index
	 * @access  public
	 */
	public function index() {
		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('birth_date', 'Tanggal Lahir', 'trim|required');
		$val->set_rules('registration_number', 'Nomor Pendaftaran', 'trim|required');
		$val->set_rules('captcha', 'Captcha', 'trim|required|callback_valid_captcha');
		$val->set_message('required', '{field} harus diisi');
		if ($val->run() == FALSE) {
			$this->load->helper(['captcha', 'string', 'form']);
			$this->vars['title'] = 'Cetak Formulir Penerimaan '. ($this->session->userdata('school_level') == 5 ? 'Mahasiswa' : 'Peserta Didik').' Baru Tahun '.$this->session->userdata('admission_year');
			$this->vars['captcha'] = $this->model->set_captcha();
			$this->vars['content'] = 'themes/'.theme_folder().'/printing-admission-form';
			$this->load->view('themes/'.theme_folder().'/index', $this->vars);
		} else {
			$registration_number = $this->input->post('registration_number', true);
			$birth_date = $this->input->post('birth_date', true);
			# Generate PDF
		}
	}

	/**
    * valid_captcha
    * @return boolean
    */
   public function valid_captcha($str) {
      if ($this->model->is_valid_captcha($str)) {
         return true;
      }
      $this->form_validation->set_message('valid_captcha', 'Kode Keamanan tidak valid.');
      return false;
   }
}