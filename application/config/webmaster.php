<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['apps']      = 'Padang Lawas Utara';
$config['version']   = '1.0';
$config['webmaster'] = 'M. Sahidin';
$config['email']     = 'msahidin22@gmail.com';
$config['website']   = 'https://padanglawasutarakab.go.id';
