<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_messages extends CI_Model {

	/**
	 * Primary key
	 * @var string
	 */
	public static $pk = 'id';

	/**
	 * Table
	 * @var string
	 */
	public static $table = 'comments';

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get data for pagination
	 * @param string
	 * @param int
	 * @param int
	 * @return Query
	 */
	public function get_where($keyword, $limit = 0, $offset = 0, $sort_field = '', $sort_type = 'ASC') {
		$this->db->select('id, comment_author, comment_email, created_at, comment_content, is_deleted');
		$this->db->where('comment_type', 'message');
		$this->db->group_start();
		$this->db->like('comment_author', $keyword);
		$this->db->or_like('comment_email', $keyword);
		$this->db->or_like('created_at', $keyword);
		$this->db->or_like('comment_content', $keyword);
		$this->db->group_end();
		if ($sort_field != '') {
			$this->db->order_by($sort_field, $sort_type);
		}
		if ($limit > 0) {
			$this->db->limit($limit, $offset);
		}
		return $this->db->get(self::$table);
	}

	/**
	 * Get Total row for pagination
	 * @param string
	 * @return int
	 */
	public function total_rows($keyword) {
		return $this->db
			->where('comment_type', 'message')
			->group_start()
			->like('comment_author', $keyword)
			->or_like('comment_email', $keyword)
			->or_like('created_at', $keyword)
			->or_like('comment_content', $keyword)
			->group_end()			
			->count_all_results(self::$table);
	}
}