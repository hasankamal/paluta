<?php
namespace common\components;

use Yii;
use yii\base\Component;

use common\models\TaAksesIzin;

class AksesIzin extends Component {
  public $data;

  public static function cekAkses($data, $izin){
  	$Tahun = $data['Tahun'];
    $Kd_Urusan = $data['Kd_Urusan'];
    $Kd_Bidang = $data['Kd_Bidang'];
    $Kd_Unit = $data['Kd_Unit'];
    $Kd_Sub = $data['Kd_Sub'];
    $Kd_Prog = $data['Kd_Prog'];

    $query = TaAksesIzin::find();
    $query->andFilterWhere([
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Nm_Izin' => $izin,
    ]);

    if (isset($data['Kd_Keg'])) {
        $Kd_Keg = $data['Kd_Keg'];
        $query->andFilterWhere(['Kd_Keg' => $Kd_Keg]);
    }

    if (isset($data['Kd_Rek_1'])) {
        $Kd_Rek_1 = $data['Kd_Rek_1'];
        $query->andFilterWhere(['Kd_Rek_1' => $Kd_Rek_1]);
    }

    if (isset($data['Kd_Rek_2'])) {
        $Kd_Rek_2 = $data['Kd_Rek_2'];
        $query->andFilterWhere(['Kd_Rek_2' => $Kd_Rek_2]);
    }

    if (isset($data['Kd_Rek_3'])) {
        $Kd_Rek_3 = $data['Kd_Rek_3'];
        $query->andFilterWhere(['Kd_Rek_3' => $Kd_Rek_3]);
    }

    if (isset($data['Kd_Rek_4'])) {
        $Kd_Rek_4 = $data['Kd_Rek_4'];
        $query->andFilterWhere(['Kd_Rek_4' => $Kd_Rek_4]);
    }

    if (isset($data['Kd_Rek_5'])) {
        $Kd_Rek_5 = $data['Kd_Rek_5'];
        $query->andFilterWhere(['Kd_Rek_5' => $Kd_Rek_5]);
    }

    if (isset($data['No_Rinc'])) {
        $No_Rinc = $data['No_Rinc'];
        $query->andFilterWhere(['No_Rinc' => $No_Rinc]);
    }

    if (isset($data['No_ID'])) {
        $No_ID = $data['No_ID'];
        $query->andFilterWhere(['No_ID' => $No_ID]);
    }

    // $model = $query->one();
    // return $model->Izin;
    //============================================//
    // if($izin == 'Tambah_Belanja_Rinc' || $izin == 'Hapus_Belanja_Rinc' || $izin == 'Ubah_Belanja_Rinc'){ 
    //     $model_tambah_izin = TaAksesIzin::find()
    //             ->where([
    //                 'Tahun' => $Tahun, 
    //                 'Kd_Urusan' => $Kd_Urusan, 
    //                 'Kd_Bidang' => $Kd_Bidang,
    //                 'Kd_Unit' => $Kd_Unit,
    //                 'Kd_Sub' => $Kd_Sub,
    //                 'Kd_Prog' => $Kd_Prog,
    //                 'Kd_Keg' => $Kd_Keg,
    //                 'Nm_Izin' => 'Tambah_Belanja',
    //             ]);

    //     if($model2 = $model_tambah_izin->one()){
    //         if ($model2->Izin == '1') {
    //             return true;
    //         }
    //     }
    // }
    //============================================//
    // if($izin == 'Tambah_Belanja_Rinc_Sub' || $izin == 'Hapus_Belanja_Rinc_Sub' || $izin == 'Ubah_Belanja_Rinc_Sub'){ 
    //     $model_tambah_izin = TaAksesIzin::find()
    //             ->where([
    //                 'Tahun' => $Tahun, 
    //                 'Kd_Urusan' => $Kd_Urusan, 
    //                 'Kd_Bidang' => $Kd_Bidang,
    //                 'Kd_Unit' => $Kd_Unit,
    //                 'Kd_Sub' => $Kd_Sub,
    //                 'Kd_Prog' => $Kd_Prog,
    //                 'Kd_Keg' => $Kd_Keg,
    //                 'Kd_Keg' => $Kd_Keg,
    //                 'Nm_Izin' => 'Tambah_Belanja',
    //             ]);

    //     if($model3 = $model_tambah_izin->one()){
    //         if ($model3->Izin == '1') {
    //             return true;
    //         }
    //     }
    // }
  	
  	if($model = $query->one()){ //cek hak akses di database
    	if ($model->Izin == '0') { // jika izin bernilai 1 atau diizinkan
    		return false;
    	}
    	else{
    		return true;
    	}
    }
    else{
    	return true; // jika tidak pernah di buat, maka diizinkan
    }
    // return $data['Kd_Rek_1']."|".$data['Kd_Rek_2']."|".$data['Kd_Rek_3']."|".$data['Kd_Rek_4']."|".$data['Kd_Rek_5'];
  }
}
?>