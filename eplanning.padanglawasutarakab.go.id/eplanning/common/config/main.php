<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'id-ID',
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
        ]
    ],
    'components' => [
        // 'cache' => [  
        //    'class' => 'yii\caching\FileCache',
        // ],
        'levelcomponent' => [

            'class' => 'common\components\LevelComponent',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            // 'defaultRoles' => ['guest'],
        ],
        'zultanggal' => [
            'class' => 'common\components\ZULTanggal',
        ],
        'pengaturan' => [

            'class' => 'common\components\Pengaturan',
        ],

        'configcomponent' => [

            'class' => 'common\components\ConfigComponent',
        ],
        'user' => [
            'identityClass' => 'mdm\admin\models\User',
            'loginUrl' => ['site/login'],
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            // '*', 
            'dashboard/*',
            'site/*',
            // 'admin/*',
            // 'gii/*',
            //'debug/*',
            'ajax/*',
            'pra-rka-prov/kirim-data',
            'api/*',
            'monitoring/*',
        ]
    ],
    'params' => [
    'maskMoneyOptions' => [
        'prefix' => '',
        'suffix' => '',
        'affixesStay' => true,
        'thousands' => '.',
        'decimal' => ',',
        'precision' => 2, 
        'allowZero' => false,
        'allowNegative' => false,
    ]
],
];
