<?php
return [
    'adminEmail' => 'info@padanglawasutarakab.go.id',
    'supportEmail' => 'support@padanglawasutarakab.go.id',
    'user.passwordResetTokenExpire' => 3600,
    'softwareBy' => 'Software By Team IT Bappeda Paluta</a>. ',
    'websiteCustomer' => 'http://padanglawasutarakab.go.id/',
    'version' => '1.0-2.1.10',
    
    'sessionTimeoutSeconds' => '100000',
];
