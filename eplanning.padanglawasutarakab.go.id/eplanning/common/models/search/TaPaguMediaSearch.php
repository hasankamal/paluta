<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaPaguMedia;

/**
 * TaPaguMediaSearch represents the model behind the search form about `common\models\TaPaguMedia`.
 */
class TaPaguMediaSearch extends TaPaguMedia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Jenis_Dokumen'], 'safe'],
            [['Kd_User', 'Kd_Media', 'Id_Peraturan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
      
        $No_ID = Yii::$app->getRequest()->getQueryParam('No_ID');
        
        $query = TaPaguMedia::find()
        ->where(['Id_Peraturan'=>$No_ID]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Tahun' => $this->Tahun,
            'Kd_User' => $this->Kd_User,
            'Kd_Media' => $this->Kd_Media,
            'Id_Peraturan' => $this->Id_Peraturan,
        ]);

        $query->andFilterWhere(['like', 'Jenis_Dokumen', $this->Jenis_Dokumen]);

        return $dataProvider;
    }
}
