<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaTujuan;

/**
 * TaTujuanSearch represents the model behind the search form about `common\models\TaTujuan`.
 */
class TaTujuanSearch extends TaTujuan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Ur_Tujuan'], 'safe'],
            [['Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'No_Misi', 'No_Tujuan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $unit = Yii::$app->levelcomponent->getUnit();

        $Posisiunit = [
            'Kd_Urusan' => $unit['Kd_Urusan'], 
            'Kd_Bidang' => $unit['Kd_Bidang'],
            'Kd_Unit' => $unit['Kd_Unit'],
            'Kd_Sub' => $unit['Kd_Sub_Unit'],
        ];

        $query = TaTujuan::find()->where($Posisiunit);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Tahun' => $this->Tahun,
            'Kd_Urusan' => $this->Kd_Urusan,
            'Kd_Bidang' => $this->Kd_Bidang,
            'Kd_Unit' => $this->Kd_Unit,
            'Kd_Sub' => $this->Kd_Sub,
            'No_Misi' => $this->No_Misi,
            'No_Tujuan' => $this->No_Tujuan,
        ]);

        $query->andFilterWhere(['like', 'Ur_Tujuan', $this->Ur_Tujuan]);

        return $dataProvider;
    }
}
