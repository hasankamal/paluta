<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaPaguSubUnitRiwayat;

/**
 * TaPaguSubUnitRiwayatSearch represents the model behind the search form about `common\models\TaPaguSubUnitRiwayat`.
 */
class TaPaguSubUnitRiwayatSearch extends TaPaguSubUnitRiwayat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'Id_Peraturan', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Waktu_Riwayat'], 'integer'],
            [['Tahun', 'Tanggal_Riwayat', 'Keterangan_Riwayat'], 'safe'],
            [['pagu'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaPaguSubUnitRiwayat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'Id_Peraturan' => $this->Id_Peraturan,
            'Tahun' => $this->Tahun,
            'Kd_Urusan' => $this->Kd_Urusan,
            'Kd_Bidang' => $this->Kd_Bidang,
            'Kd_Unit' => $this->Kd_Unit,
            'Kd_Sub' => $this->Kd_Sub,
            'pagu' => $this->pagu,
            'Tanggal_Riwayat' => $this->Tanggal_Riwayat,
            'Waktu_Riwayat' => $this->Waktu_Riwayat,
        ]);

        $query->andFilterWhere(['like', 'Keterangan_Riwayat', $this->Keterangan_Riwayat]);

        return $dataProvider;
    }
}
