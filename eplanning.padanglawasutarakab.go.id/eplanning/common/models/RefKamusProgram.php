<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Ref_Kamus_Program".
 *
 * @property int $Kd_Program
 * @property string $Nm_Program
 * @property int $Status
 * @property string $Sasaran
 *
 * @property RefProgram[] $refPrograms
 * @property RefBidang[] $kdUrusans
 * @property TaRpjmdProgramPrioritas $taRpjmdProgramPrioritas
 * @property ProgramNasional[] $programNasionals
 */
class RefKamusProgram extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ref_Kamus_Program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Program', 'Nm_Program', 'Sasaran'], 'required'],
            [['Kd_Program', 'Status'], 'integer'],
            [['Nm_Program', 'Sasaran'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Program' => 'Kd  Program',
            'Nm_Program' => 'Nm  Program',
            'Status' => 'Status',
            'Sasaran' => 'Sasaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefPrograms()
    {
        return $this->hasMany(RefProgram::className(), ['Kd_Prog' => 'Kd_Program']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdUrusans()
    {
        return $this->hasMany(RefBidang::className(), ['Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang'])->viaTable('Ref_Program', ['Kd_Prog' => 'Kd_Program']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaRpjmdProgramPrioritas()
    {
        return $this->hasOne(TaRpjmdProgramPrioritas::className(), ['Kd_Prog' => 'Kd_Program']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramNasionals()
    {
        return $this->hasMany(ProgramNasional::className(), ['id_program' => 'Kd_Program']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\RefKamusProgramQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\RefKamusProgramQuery(get_called_class());
    }
}
