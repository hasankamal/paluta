<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Ta_Akses_Izin".
 *
 * @property string $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Prog
 * @property int $Kd_Keg
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property string $Izin
 * @property string $Nm_Izin
 */
class TaAksesIzin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Akses_Izin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Prog', 'Kd_Keg', 'Kd_Unit', 'Kd_Sub', 'Nm_Izin'], 'required'],
            [['Tahun'], 'safe'],
            [['Kd_Urusan', 'Kd_Bidang', 'Kd_Prog', 'Kd_Keg', 'Kd_Unit', 'Kd_Sub'], 'integer'],
            [['Izin', 'Nm_Izin'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Prog' => 'Kd  Prog',
            'Kd_Keg' => 'Kd  Keg',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Izin' => 'Izin',
            'Nm_Izin' => 'Nm  Izin',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\TaAksesIzinQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\TaAksesIzinQuery(get_called_class());
    }
}
