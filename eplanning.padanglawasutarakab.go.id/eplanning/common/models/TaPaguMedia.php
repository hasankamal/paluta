<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Ta_Pagu_Media".
 *
 * @property string $Tahun
 * @property int $Kd_User
 * @property int $Kd_Media
 * @property string $Jenis_Dokumen
 *
 * @property RefMedia $kdMedia
 */
class TaPaguMedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Pagu_Media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_User', 'Kd_Media', 'Jenis_Dokumen','Id_Peraturan'], 'required'],
            [['Tahun'], 'safe'],
            [['Kd_User', 'Kd_Media'], 'integer'],
            [['Jenis_Dokumen'], 'string'],
            [['Id_Peraturan'],'integer'],
            [['Kd_Media'], 'exist', 'skipOnError' => true, 'targetClass' => RefMedia::className(), 'targetAttribute' => ['Kd_Media' => 'Kd_Media']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_User' => 'Kd  User',
            'Kd_Media' => 'Kd  Media',
            'Jenis_Dokumen' => 'Jenis  Dokumen',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKdMedia()
    {
        return $this->hasOne(RefMedia::className(), ['Kd_Media' => 'Kd_Media']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\TaPaguMediaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\TaPaguMediaQuery(get_called_class());
    }
}
