<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Ta_Pagu_Sub_Unit_Riwayat".
 *
 * @property string $Tahun
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property double $pagu
 * @property string $Tanggal_Riwayat
 * @property string $Keterangan_Riwayat Tambah, Edit, Hapus
 *
 * @property TaPaguSubUnit $tahun
 */
class TaPaguSubUnitRiwayat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Pagu_Sub_Unit_Riwayat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Id_Peraturan'], 'required'],
            [['Tahun', 'Tanggal_Riwayat'], 'safe'],
            [['Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Id_Peraturan', 'Waktu_Riwayat', 'Kd_User'], 'integer'],
            [['pagu'], 'number'],
            [['Keterangan_Riwayat'], 'string', 'max' => 255],
            [['Tahun', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub'], 'exist', 'skipOnError' => true, 'targetClass' => TaPaguSubUnit::className(), 'targetAttribute' => ['Tahun' => 'Tahun', 'Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang', 'Kd_Unit' => 'Kd_Unit', 'Kd_Sub' => 'Kd_Sub']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'pagu' => 'Pagu',
            'Tanggal_Riwayat' => 'Tanggal  Riwayat',
            'Keterangan_Riwayat' => 'Keterangan  Riwayat',
            'Id_Peraturan' => 'Peraturan',
            'Waktu_Riwayat' => 'Waktu Riwayat'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTahun()
    {
        return $this->hasOne(TaPaguSubUnit::className(), ['Tahun' => 'Tahun', 'Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang', 'Kd_Unit' => 'Kd_Unit', 'Kd_Sub' => 'Kd_Sub']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\TaPaguSubUnitRiwayatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\TaPaguSubUnitRiwayatQuery(get_called_class());
    }
      public function getUrusan()
    {
        return $this->hasOne(\common\models\RefUrusan::className(), ['Kd_Urusan' => 'Kd_Urusan']);
    }

    public function getBidang()
    {
        return $this->hasOne(\common\models\RefBidang::className(), ['Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang']);
    }

    public function getUnit()
    {
        return $this->hasOne(\common\models\RefUnit::className(), ['Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang', 'Kd_Unit' => 'Kd_Unit']);
    }

    public function getSub()
    {
        return $this->hasOne(\common\models\RefSubUnit::className(), ['Kd_Urusan' => 'Kd_Urusan', 'Kd_Bidang' => 'Kd_Bidang', 'Kd_Unit' => 'Kd_Unit', 'Kd_Sub' => 'Kd_Sub']);
    }

    public function getPeraturan()
    {
        return $this->hasOne(\emusrenbang\models\TaPeraturanPagu::className(), ['No_ID' => 'Id_Peraturan']);
    }
}
