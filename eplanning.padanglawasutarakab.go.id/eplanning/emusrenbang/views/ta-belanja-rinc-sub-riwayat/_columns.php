<?php
use yii\helpers\Url;

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    //     [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'Id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Tahun',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Tanggal_Riwayat',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Keterangan_Riwayat',
    ],
    [
        // 'class'=>'\kartik\grid\DataColumn',
        'value' => 'urusan.Nm_Urusan',
        'attribute'=>'Kd_Urusan',
        'format'=>'raw',
        'filter'=> $data_urusan,
        'label'=>'Urusan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Bidang',
        'value' => 'bidang.Nm_Bidang',
        'filter'=> $data_bidang,
        'label'=>'Bidang',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Unit',
        'value' => 'unit.Nm_Unit',
        'filter' => $data_unit,
        'label'=>'Unit',

    ],
        [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Sub',
        'value' => 'sub.Nm_Sub_Unit',
        'filter' => $data_sub,
        'label'=>'Sub Unit',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Prog',
        'value' => 'program.Ket_Program',
        'filter' => $data_program,
        'label'=>'Program',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Keg',
        'value' => 'kegiatan.Ket_Kegiatan',
        'filter'=> $data_kegiatan,
        'label' => 'Kegiatan'
    ],
  
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'ID_Prog',
    // ],
 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Rek_1',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Rek_2',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Rek_3',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Rek_4',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Rek_5',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'No_Rinc',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'No_ID',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Nilai_1',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Sat_1',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Nilai_2',
    ],
  
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Sat_2',
    ],
     [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Nilai_3',
    ],
 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Sat_3',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Satuan123',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Jml_Satuan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Nilai_Rp',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Total',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Keterangan',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'Asal_Biaya',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'Uraian_Asal_Biaya',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'Ref_Usulan_Rincian',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'Uraian_Ref_Usulan',
    // ],
 
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to([$action,'Id, $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc, $No_ID'=>$key]);
    //     },
    //     'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>'Are you sure?',
    //                       'data-confirm-message'=>'Are you sure want to delete this item'], 
    // ],

];   