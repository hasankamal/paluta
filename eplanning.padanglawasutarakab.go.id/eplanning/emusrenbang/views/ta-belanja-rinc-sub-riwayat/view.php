<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emusrenbang\models\TaBelanjaRincSubRiwayat */
?>
<div class="ta-belanja-rinc-sub-riwayat-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'Tahun',
            'Kd_Urusan',
            'Kd_Bidang',
            'Kd_Unit',
            'Kd_Sub',
            'Kd_Prog',
            'ID_Prog',
            'Kd_Keg',
            'Kd_Rek_1',
            'Kd_Rek_2',
            'Kd_Rek_3',
            'Kd_Rek_4',
            'Kd_Rek_5',
            'No_Rinc',
            'No_ID',
            'Sat_1',
            'Nilai_1',
            'Sat_2',
            'Nilai_2',
            'Sat_3',
            'Nilai_3',
            'Satuan123',
            'Jml_Satuan',
            'Nilai_Rp',
            'Total',
            'Keterangan',
            'Asal_Biaya',
            'Uraian_Asal_Biaya:ntext',
            'Ref_Usulan_Rincian',
            'Uraian_Ref_Usulan:ntext',
            'Tanggal_Riwayat',
            'Keterangan_Riwayat',
        ],
    ]) ?>

</div>
