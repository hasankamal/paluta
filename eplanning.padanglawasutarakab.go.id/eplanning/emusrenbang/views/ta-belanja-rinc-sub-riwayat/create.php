<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emusrenbang\models\TaBelanjaRincSubRiwayat */

?>
<div class="ta-belanja-rinc-sub-riwayat-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
