<?php
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use common\models\TaPagumedia;

// $this->registerJsFile(
//         '@web/js/modalperaturan.js', ['depends' => [\yii\web\JqueryAsset::className()]]
// );

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Tahun',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'No_ID',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'No_Peraturan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Tgl_Peraturan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Uraian',
    ], 
    [
        
//      'dropdown' => false,
//         'vAlign'=>'middle',
        'header' => 'Dokumen',
        'format' => 'raw',
        'value' => function ($model) {
            
            // if (isset($model->Kd_Ta_Forum_Lingkungan))
            // {
                
                $tombols ='';

                $foto = $model->getTaPaguMedias()->all();

                foreach ($foto as $value) {

                    $Jenis_Media = $value->kdMedia->Jenis_Media;
                    $Nm_Media = $value->kdMedia->Nm_Media;

                    // $url = "index.php?r=ta-peraturan-pagu/lihat-file&nama_file=".$Nm_Media;

                    // $tombols .= '<button type="button" role="modal-remote" class="btn btn-primary btn-xs" data-url="'.$url.'">'.$Jenis_Media.'</button>';

                   $tombols .= Html::a($Jenis_Media, ["lihat-file","nama_file"=>$Nm_Media], ['role'=>'modal-remote', 'class'=>"btn bg-navy"]).'<br>'; 
                }
                return $tombols;
            //}
            // else {
            //     return '-';
            // }
            
        },  
    ],
// [
//         'class' => 'kartik\grid\ActionColumn',
//         'dropdown' => false,
//         'vAlign'=>'middle',
//         'urlCreator' => function($action, $model, $key, $index) { 
//                 return Url::to([$action,'Tahun, $No_ID'=>$key]);
//         },
//         'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//         'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//     ],

]; 
?>