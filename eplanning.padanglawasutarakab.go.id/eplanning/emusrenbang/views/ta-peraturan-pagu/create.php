<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emusrenbang\models\TaPeraturanPagu */

?>
<div class="ta-peraturan-pagu-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
