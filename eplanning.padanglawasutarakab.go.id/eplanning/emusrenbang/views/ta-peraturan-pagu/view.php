<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emusrenbang\models\TaPeraturanPagu */
?>
<div class="ta-peraturan-pagu-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'No_ID',
            'No_Peraturan',
            'Tgl_Peraturan',
            'Uraian',
        ],
    ]) ?>

</div>
