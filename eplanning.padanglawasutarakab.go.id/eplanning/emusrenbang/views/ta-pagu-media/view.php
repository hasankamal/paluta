<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TaPaguMedia */
?>
<div class="ta-pagu-media-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tahun',
            'Kd_User',
            'Kd_Media',
            'Id_Peraturan',
            'Jenis_Dokumen',
        ],
    ]) ?>

</div>
