<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaPaguMedia */

?>
<div class="ta-pagu-media-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
