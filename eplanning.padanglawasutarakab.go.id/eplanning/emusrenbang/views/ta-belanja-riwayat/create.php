<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emusrenbang\models\TaBelanjaRiwayat */

?>
<div class="ta-belanja-riwayat-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
