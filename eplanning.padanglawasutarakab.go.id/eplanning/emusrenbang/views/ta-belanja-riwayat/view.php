<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emusrenbang\models\TaBelanjaRiwayat */
?>
<div class="ta-belanja-riwayat-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'Tahun',
            'Kd_Urusan',
            'Kd_Bidang',
            'Kd_Unit',
            'Kd_Sub',
            'Kd_Prog',
            'ID_Prog',
            'Kd_Keg',
            'Kd_Rek_1',
            'Kd_Rek_2',
            'Kd_Rek_3',
            'Kd_Rek_4',
            'Kd_Rek_5',
            'Kd_Ap_Pub',
            'Kd_Sumber',
            'Tanggal_Riwayat',
            'Keterangan_Riwayat',
        ],
    ]) ?>

</div>
