<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Nawacita */

$this->title = "Daftar Program";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <?= Html::a('&nbsp;Cetak&nbsp;', ['cetak-program-sasaran'
                
            ], 
                ['class' => 'btn btn-primary', 'target' => '_blank']);
        ?>
    </div>
     <div class="box-header with-border">
        <div class="col-md-1"></div><div class="col-md-10" style="text-align:center;"><h3>Daftar Program beserta Sasaran <br>Kabupaten Paluta <?= date('Y') + 1 ?></h3></div><div class="col-md-1"></div>
        
        <br>
        <div class="col-xs-12">
            <table class="table table-striped table-bordered" id="sample_1">
    <thead>
        <tr>
            <th rowspan="2" class="vcenter text-center"> No. Urut</th>
            <th rowspan="2" class="vcenter text-center">
                Urusan/Bidang Urusan/ Unit <br> Pemerintahan Daerah dan <br>Program
            </th>
            <th rowspan="2" class="vcenter text-center">
                Sasaran Program
            </th>  
        </tr>
    </thead>
    <tbody> 
    <?php $no_urus=0; ?>
    <?php foreach ($urusan as $dataurusan): ?>
       <?php if ($dataurusan->getprogram()->count()<=0) {
                continue;
            } ?>
        <?php $no_urus++; ?>
        <tr>
            <td style="font-size:11px;"> <strong><?= $no_urus; ?></strong></td>
            <td style="font-size:11px;" ><strong><?= $dataurusan->Nm_Urusan ?></strong></td>
            <td></td>
            
        </tr>
            <?php $no_bid=0; ?>
            <?php foreach ($dataurusan->refBidangs as $databidang): ?> 
                <?php if ($databidang->getprogram()->count()<=0) {
                continue;
                } ?>  
            <?php $no_bid++; ?>
                <tr>
                <td style="font-size:11px;"><strong><?= $no_urus ?>.<?= $no_bid ?></strong></td>
                <td style="font-size:11px;" > <strong> Bidang: <?= $databidang->Nm_Bidang ?></strong></td>
                <td></td>    
                </tr>
                <?php $no_unit=0;?>    
                <?php foreach ($databidang->refUnits as $dataunit): ?> 

                 <?php if ($dataunit->getprogram()->count()<=0) {
                    continue;
                    } ?> 
                <?php $no_unit++; ?> 
                    <tr>
                        <td style="font-size:11px;"> <strong><?= $no_urus ?>.<?= $no_bid ?>.<?= $no_unit?></strong></td>
                        <td style="font-size:11px;" > <strong>Unit: <?= $dataunit->Nm_Unit ?></strong></td>
                        <td></td>    
                    </tr>
                    
                    <tr>  
                    <?php foreach ($dataunit->taSubUnits as $datasubunit): ?>

                        <?php if ($datasubunit->getprogram()->count()<=0) {
                            continue;
                        } ?> 

                        <?php $no_prog=0; ?>

                        <?php foreach ($datasubunit->program as $dataprogram): ?>
                        <?php if ($dataprogram->getKegiatans()->count()<=0) {
                            continue;
                        } ?> 
                        

                        <?php $no_prog++; ?>
                            <td style="font-size:11px;" > <?= $no_urus ?>.<?= $no_bid ?>.<?= $no_unit ?>.<?= $no_prog ?></td>
                            <td style="font-size:11px;"> <?= isset($dataprogram->refKamusProgram->Nm_Program) ? $dataprogram->refKamusProgram->Nm_Program : "" ?></td>
                           
                            <td style="font-size:11px; padding-left: 20px;" > <?= isset($dataprogram->refKamusProgram->Sasaran) ? $dataprogram->refKamusProgram->Sasaran : "" ?></td> 
                            </tr>   
                        <?php endforeach; ?> 
                    <?php endforeach; ?>       
                <?php endforeach; ?> 
            <?php endforeach; ?>  
        <?php endforeach; ?>
    </tbody>
    </table>
        </div>
    </div>
</div>
