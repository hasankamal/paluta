<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Nawacita */

$this->title = "Plafon Anggaran Sementara";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <?= Html::a('&nbsp;Cetak&nbsp;', ['cetak-paspbpk2', 
                'urusan' => $subunit->Kd_Urusan,
                'bidang' => $subunit->Kd_Bidang,
                'unit' => $subunit->Kd_Unit,
                'sub' => $subunit->Kd_Sub,
            ], 
                ['class' => 'btn btn-primary', 'target' => '_blank']);
        ?>
    </div>
     <div class="box-header with-border">
        <div class="col-md-1"></div><div class="col-md-10" style="text-align:center;"><h3>Plafon Anggaran Sementara<br>Berdasarkan Program dan Kegiatan Tahun Anggaran <?= date('Y') + 1 ?></h3></div><div class="col-md-1"></div>
        <div class="col-xs-12"><strong>Urusan &ensp;: </strong><?= $subunit->urusan->Nm_Urusan ?></div>
        <div class="col-xs-12"><strong>SKPD&ensp;&ensp;&ensp;: </strong><?= $subunit->Nm_Sub_Unit ?></div>
        <br>
        <div class="col-xs-12">
            <table class="table table-bordered">
                <thead>
                        <tr style="border: 1px solid black;">
                        <th  rowspan="2" style="text-align:center;vertical-align:middle;border: 1px solid black;font-size: 16px;">No</th>
                        <th  rowspan="2" style="text-align:center;vertical-align:middle;border: 1px solid black;">Program/Kegiatan </th>
                        <th  rowspan="2" style="text-align:center;vertical-align:middle;border: 1px solid black;">Sasaran Program/Kegiatan </th>
                        <th  colspan="2" style="text-align:center;vertical-align:middle;border: 1px solid black;">Target </th>
                        <th  rowspan="2" style="text-align:center;vertical-align:middle;border: 1px solid black;">Plafon Anggaran Sementara</th>
                    </tr>
                    <tr style="border: 1px solid black">
                        <th  style="text-align:center;vertical-align:middle; border: 1px solid black;">Volume </th>
                        <th style="text-align:center;vertical-align:middle;border: 1px solid black;">Satuan </th>
                    </tr>

                    <tr>
                    <?php for($i=1;$i<=6;$i++): ?>
                        <td style="text-align:center;vertical-align:middle;border: 1px solid black;">(<?= $i ?>)</td>
                    <?php endfor; ?>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $noProg =0;
                    foreach ($TaProgram as $data1 => $value1): 
                        if ($value1->getKegiatans()->count()<=0) {
                                continue;
                            }
                        $noProg++;
                        
                ?>
                        <tr style="border: 1px solid black;">
                            <!-- <td style="border: 1px solid black;"><strong><?= $value1->Kd_Prog ?></strong></td> -->
                            <td style="border: 1px solid black;"><strong><?= $noProg; ?></strong></td>
    
                            <td style="border: 1px solid black;"><strong><?= ucfirst($value1->Ket_Prog) ?></strong></td>
                            <td style="text-align:left;border: 1px solid black;"><strong><?= ucfirst(($ref_prog = $value1->refKamusProgram) ? $ref_prog->Sasaran : '-') ?></strong></td>
                            <td style="text-align:center;border: 1px solid black;border: 1px solid black">100</td>
                            <td style="text-align:center;border: 1px solid black;border: 1px solid black">%</td>
                            <td style="text-align:right;border: 1px solid black;border: 1px solid black"><strong><?= number_format($value1->paguprograms,0,'.','.')?></strong></td>
                        </tr>

                        <?php
                            // $noProg++;
                            $noKeg=0; 
                            foreach ($value1->taKegiatans as $data2 => $value2):
                                $noKeg++;

                                ?>
                                <tr  style="border: 1px solid black">
                                    <!-- <td><?= $value1->Kd_Prog.'.'.$value2->Kd_Keg ?></td> -->
                                    <td  style="border: 1px solid black;"><?= $noProg.'.'.$noKeg ?></td>
                                    <td style="padding-left:25px;border: 1px solid black;"><?= ucfirst($value2->Ket_Kegiatan) ?></td>
                                   <!--  <td style="border: 1px solid black;"><?= ucfirst(isset($value2->taIndikatorsKinerja->Tolak_Ukur) ? ($value2->taIndikatorsKinerja->Tolak_Ukur) : '-')?></td> -->
                                    <td style="border: 1px solid black;"><?= ucfirst(isset($value2->statusSasaran->Sasaran) ? ($value2->statusSasaran->Sasaran) : '-')?></td>
                                    <td style="text-align:center;border: 1px solid black;"><?= isset($value2->taIndikatorsKinerja->Target_Angka) ? number_format($value2->taIndikatorsKinerja->Target_Angka,0,'.','.') : ''?></td>
                                    <td style="text-align:center;border: 1px solid black;"><?= isset($value2->taIndikatorsKinerja->Target_Uraian) ? ($value2->taIndikatorsKinerja->Target_Uraian) : '-' ?></td>
                                    <td style="text-align:right;border: 1px solid black;"><strong><?= isset($value2->pagukegiatans) ? number_format($value2->pagukegiatans,0,'.','.') : 0 ?></strong></td>
                                </tr>
                                <?php
                            endforeach; ?>
                        <?php 
                    endforeach; 
                ?>
                <tr style="border: 1px solid black;"> 
                    <td style="text-align:center;border: 1px solid black;"> </td>

                    <td style="text-align:center;border: 1px solid black;"><strong> TOTAL </strong></td>
                    <td style="text-align:center;border: 1px solid black;"> </td>
                    <td style="text-align:center;border: 1px solid black;"> </td>
                    <td style="text-align:center;border: 1px solid black;"> </td>
                    <td style="text-align:right;border: 1px solid black;"><strong> <?= number_format($subunit->pagukegiatans,0,'.','.') ?> </strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
