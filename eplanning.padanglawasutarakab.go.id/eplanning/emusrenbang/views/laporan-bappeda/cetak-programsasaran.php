
     <div class="box-header with-border">
        <div class="col-md-1"></div><div class="col-md-10" style="text-align:center;"><h3>Daftar Program beserta Sasaran <br>Kabupaten Paluta <?= date('Y') + 1 ?></h3></div><div class="col-md-1"></div>
        
        <br>
        <div class="col-xs-12">
            <table class="table table-striped table-bordered" id="sample_1">
    <thead>
        <tr>
            <th rowspan="1" class="vcenter text-center"> No. Urut</th>
            <th rowspan="1" class="vcenter text-center">
                Urusan/Program
            </th>
            <th rowspan="1" class="vcenter text-center">
                Sasaran
            </th>  
        </tr>
    </thead>
    <tbody> 
        <?php $no_unit = 0;?>
        <?php foreach ($urusan as $dataurusan): ?>
           <?php if ($dataurusan->getprogram()->count()<=0) {
                continue;
            } ?>

            <?php foreach ($dataurusan->refBidangs as $databidang): ?> 
                <?php if ($databidang->getprogram()->count()<=0) {
                    continue;
                } ?>

                    
                <?php foreach ($databidang->refUnits as $dataunit): ?> 
                    <?php if ($dataunit->getprogram()->count()<=0) {
                        continue;
                    } ?> 
                    <?php $no_unit++; ?> 
                    <tr>
                        <td style="font-size:11px;"> <strong><?= $no_unit?></strong></td>
                        <td style="font-size:11px;" > <strong><?= $dataunit->Nm_Unit ?></strong></td>
                        <td></td>    
                    </tr>

                    <?php foreach ($dataunit->taSubUnits as $datasubunit): ?>
                        <?php if ($datasubunit->getprogram()->count()<=0) {
                            continue;
                        } ?>

                        <?php $no_prog = 0; ?>
                        <?php foreach ($datasubunit->program as $dataprogram): ?>
                            <?php if ($dataprogram->getKegiatans()->count()<=0) {
                                continue;
                            } ?> 
                            <?php $no_prog++; ?>
                            <tr>
                                <td style="font-size:11px;" ><?= $no_unit ?>.<?= $no_prog ?></td>
                                <td style="font-size:11px;"> <?= isset($dataprogram->refKamusProgram->Nm_Program) ? $dataprogram->refKamusProgram->Nm_Program : "" ?></td>
                               
                                <td style="font-size:11px; padding-left: 20px;" > <?= isset($dataprogram->refKamusProgram->Sasaran) ? $dataprogram->refKamusProgram->Sasaran : "" ?></td> 
                            </tr>

                        <?php endforeach; ?> 
                    <?php endforeach; ?>       
                <?php endforeach; ?> 
            <?php endforeach; ?>  
        <?php endforeach; ?>
    </tbody>
    </table>
        </div>
    </div>
