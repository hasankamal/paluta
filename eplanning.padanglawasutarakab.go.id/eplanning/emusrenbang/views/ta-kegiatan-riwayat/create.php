<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emusrenbang\models\TaKegiatanRiwayat */

?>
<div class="ta-kegiatan-riwayat-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
