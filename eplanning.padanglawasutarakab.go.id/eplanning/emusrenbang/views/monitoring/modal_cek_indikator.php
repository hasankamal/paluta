<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Misi */

$this->title = 'Tambah Kegiatan';
$this->params['breadcrumbs'][] = $this->title;




// $this->registerCssFile(
//         '@web/plugins/select2/select2.css', ['depends' => [\yii\web\JqueryAsset::className()]]
// );

// $this->registerJsFile(
//         '@web/plugins/select2/select2.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
// );
$url = ['monitoring/keterangan-kegiatan-proses',
				'Tahun' => $Tahun,
				'Kd_Urusan' => $Kd_Urusan,
				'Kd_Bidang' => $Kd_Bidang,
				'Kd_Unit' => $Kd_Unit,
				'Kd_Sub' => $Kd_Sub,
				'Kd_Prog' => $Kd_Prog,
				'Kd_Keg' => $Kd_Keg,
	];
	if ($isi = $model->statusSasaran) {
		$sasaran = $isi->Sasaran;	
	}
	else{
		$sasaran = '';
	}

  $keluaran = $model->indikatorKeluaran->Tolak_Ukur." ".$model->indikatorKeluaran->Target_Angka." ".$model->indikatorKeluaran->Target_Uraian ;
  $hasil = $model->indikatorHasil->Tolak_Ukur." ".$model->indikatorHasil->Target_Angka." ".$model->indikatorHasil->Target_Uraian ;
  $n1 = $model->indikatorN1->Tolak_Ukur." ".$model->indikatorN1->Target_Angka." ".$model->indikatorN1->Target_Uraian ;
?>
<div class="misi-form">
	<table>
		<tr>
			<td width="150">Sasaran</td>
			<td>:</td>
			<td><?= $sasaran ?></td>
		</tr>
		<tr>
			<td>Keluaran</td>
			<td>:</td>
			<td><?= $keluaran ?></td>
		</tr>
		<tr>
			<td>Hasil</td>
			<td>:</td>
			<td><?= $hasil ?></td>
		</tr>
		<tr>
			<td>N+1</td>
			<td>:</td>
			<td><?= $n1 ?></td>
		</tr>
	</table>
</div>


<script type="text/javascript">
</script>