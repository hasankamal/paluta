<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaPaguSubUnitRiwayat */

?>
<div class="ta-pagu-sub-unit-riwayat-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
