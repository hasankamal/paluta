<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$url = ['akses/akses-dokumen-proses',
				'Tahun' => $Tahun,
				'Kd_Urusan' => $Kd_Urusan,
				'Kd_Bidang' => $Kd_Bidang,
				'Kd_Unit' => $Kd_Unit,
				'Kd_Sub' => $Kd_Sub,
			];

?>
<div class="pagu-form">

	<?php $form = ActiveForm::begin(['action' =>$url, 'id'=>'form_pagu']); ?>
    <?= $form->field($model,'File')->fileInput()->label('Dokumen Berita Acara');  ?>
    <div class="form-group">
        <?= Html::submitButton('Proses', ['class' => 'btn btn-primary', 'id' => 'btn_simpan']) ?>
    </div>
	<?php ActiveForm::end(); ?>

</div>