<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$url_pagu = ['akses/pagu-proses',
        'Tahun' => $Tahun,
        'Kd_Urusan' => $Kd_Urusan,
        'Kd_Bidang' => $Kd_Bidang,
        'Kd_Unit' => $Kd_Unit,
        'Kd_Sub' => $Kd_Sub,
];

?>
<?php
	if($status_terbuka):
	?>
	<?php $form_pagu = ActiveForm::begin(['action' =>$url_pagu, 'id' => 'form-pagu']); ?>
	  <?= $form_pagu->field($model_pagu, 'pagu')->textInput()->label('Pagu SKPD') ?>
	  <?php  echo Html::submitButton('Simpan', ['class' => 'btn btn-primary', 'name' => 'signup-button']); ?>
	<?php ActiveForm::end(); ?>
	<?php
	else:
	?>
		<h2>Silahkan Tekan tombol buka, dan upload berita acara (surat pengajuan) untuk dapat mengubah semua hak akses</h2>
	<?php
	endif;
?>