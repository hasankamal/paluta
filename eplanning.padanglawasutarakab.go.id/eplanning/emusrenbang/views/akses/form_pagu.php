<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kegiatans */
/* @var $form yii\widgets\ActiveForm */

$url = ['akses/ubah-pagu-proses',
				'Tahun' => $Tahun,
				'Kd_Urusan' => $Kd_Urusan,
				'Kd_Bidang' => $Kd_Bidang,
				'Kd_Unit' => $Kd_Unit,
				'Kd_Sub' => $Kd_Sub,
				'Kd_Prog' => $Kd_Prog,
				'Kd_Keg' => $Kd_Keg,
			];

?>

<div class="pagu-form">

	<?php $form = ActiveForm::begin(['action' =>$url, 'id'=>'form_pagu']); ?>
    <?= $form->field($model, 'pagu')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton('Ubah', ['class' => 'btn btn-primary', 'id' => 'btn_simpan']) ?>
    </div>
	<?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">


$("#modal form").submit(function(e){
  e.preventDefault();
  var alamat = $(this).attr('action');
  $('#modalContent').html('proses');
  $.ajax({ 
    type: "POST",
    url:alamat,
    data:$(this).serialize(),
    success: function(isi){
    	$('#modalContent').html(isi);
    	$('#lihat_pagu_kegiatan').trigger('click');
    },
    error: function(){
      alert("Gagal Tambah Kegiatan");
    }
  });
});
</script>