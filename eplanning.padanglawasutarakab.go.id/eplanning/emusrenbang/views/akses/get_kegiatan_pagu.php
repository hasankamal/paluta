<?php
use yii\helpers\Url;

?>
<?php
foreach ($data as $key => $value):
  $pagu_terpakai = $value->getBelanjaRincSubs()->sum('Total');
  $pagu_kegiatan = ($nilai_pagu = $value->pagu->pagu)? $nilai_pagu:0;

  $url_ubah_pagu = Url::to(['akses/ubah-pagu', 
                              'Tahun' => $value['Tahun'],
                              'Kd_Urusan' => $value['Kd_Urusan'],
                              'Kd_Bidang' => $value['Kd_Bidang'],
                              'Kd_Unit' => $value['Kd_Unit'],
                              'Kd_Sub' => $value['Kd_Sub'],
                              'Kd_Prog' => $value['Kd_Prog'],
                              'Kd_Keg' => $value['Kd_Keg'],
                            ]); 
  ?>
    <tr>
      <td class="dat-program">
        <?= $value->Ket_Kegiatan ?>
      </td>
      <td>
        <?= number_format($pagu_terpakai, 0, ",", ".") ?>
      </td>
      <td>
        <?= number_format($pagu_kegiatan, 0, ",", ".") ?>
      </td>
      <td>
        <button class="btn btn-primary btn_ubah_pagu" value="<?= $url_ubah_pagu ?>" title='Ubah Pagu'>Ubah</button>
      </td>
    </tr>
  <?php
endforeach;
?>

<script type="text/javascript">

// $('.btn_ubah_izin').on('click', function () {
//   var alamat = $(this).attr('value');
//   var $this = $(this);
//   $this.html('proses...');
//   // alert(alamat);
//   $.ajax({ 
//     type: "POST",
//     url:alamat,
//     data:{},
//     success: function(isi){
//       // alert(isi);
//       if (isi == '1') {
//         // alert('buka');
//         $this.html('Terbuka');
//         $this.removeClass('btn-danger').addClass('btn-success');
//       }
//       else{
//         // alert('tutup');
//         $this.html('Tertutup');
//         $this.removeClass('btn-success').addClass('btn-danger');
//       }
//     },
//     error: function(){
//       alert("Gagal Ubah Izin");
//     }
//   });
// });

$( ".btn_ubah_pagu" ).click(function() {
  var alamat = $(this).attr('value');
  var judul = $(this).attr('title');
  // alert(alamat);
  $('#modal').find('#modalContent').html('Mengambil Data..');

  if ($('#modal').data('bs.modal').isShown) {
    $('#modal').find('#modalContent')
                .load(alamat);
    $('#modalHeader').html('<h4>'+judul +'</h4>'); 
  }
  else{
     $('#modal').modal('show')
              .find('#modalContent')
              .load(alamat);
    $('#modalHeader').html('<h4>'+judul +'</h4>'); 
  }
});
</script>