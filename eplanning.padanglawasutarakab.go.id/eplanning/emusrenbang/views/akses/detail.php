<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Pengaturan SKPD';

$this->registerJsFile(
    '@web/js/detail_akses.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$url_upload_file = Url::to(['akses/akses-dokumen', 
                              'Tahun' => $Tahun,
                              'Kd_Urusan' => $Kd_Urusan,
                              'Kd_Bidang' => $Kd_Bidang,
                              'Kd_Unit' => $Kd_Unit,
                              'Kd_Sub' => $Kd_Sub,
                            ]); 

$url_selesai = Url::to(['akses/akses-dokumen-selesai', 
                              'Tahun' => $Tahun,
                              'Kd_Urusan' => $Kd_Urusan,
                              'Kd_Bidang' => $Kd_Bidang,
                              'Kd_Unit' => $Kd_Unit,
                              'Kd_Sub' => $Kd_Sub,
                            ]); 
?>
<div class="row">
  <div class="col-md-3">

    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">

        <h3 class="profile-username text-center"><?= $skpd->Nm_Sub_Unit ?></h3>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">

            <?php 
            if (isset($skpd->paguSkpd->pagu))                        
              $pagu = $skpd->paguSkpd->pagu;
            else 
              $pagu = 0;
            ?>

            <b>Pagu</b> <a class="pull-right"><?= number_format($pagu,0,',','.') ?></a>
          </li>
          <li class="list-group-item">
            <b>Program</b> <a class="pull-right"><?= $skpd->getTaPrograms()->count() ?></a>
          </li>
          <li class="list-group-item">
            <b>Kegiatan</b> <a class="pull-right"><?= $skpd->getJlhKegiatan() ?></a>
          </li>
        </ul>

        <!-- <a href="#" class="btn btn-danger btn-block"><b>Selesai</b></a> -->
        <?php
          if($status_terbuka):
          ?>
            <button class="btn btn-danger btn-block btn_modal" value="<?= $url_selesai ?>" title='Ubah Akses'><b>Selesai</b></button>
          <?php
          else:?>
            <button class="btn btn-success btn-block btn_modal" value="<?= $url_upload_file ?>" title='Ubah Akses'><b>Buka</b></button>
          <?php
          endif;
        ?>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        <h3 class="profile-username text-center">Riwayat Terakhir</h3>
        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <?= $riwayat['username'] ?>
          </li>
          <li class="list-group-item">
            <?= $riwayat['nama_lengkap'] ?>
          </li>
          <li class="list-group-item">
            Buka
            <a class="pull-right"><?= $riwayat['waktu_buka'] ?></a>
          </li>
          <li class="list-group-item">
            Tutup
            <a class="pull-right"><?= $riwayat['waktu_tutup'] ?></a>
          </li>
        </ul>
      </div>
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#pagu_skpd" data-toggle="tab">Pagu SKPD</a></li>
        <?php
          if($status_terbuka):
          ?>
          <li><a href="#pagu_kegiatan" data-toggle="tab">Pagu Kegiatan</a></li>
          <li><a href="#akses_program" data-toggle="tab">Akses Program</a></li>
          <li><a href="#akses_kegiatan" data-toggle="tab">Akses Kegiatan</a></li>
          <li><a href="#akses_belanja" data-toggle="tab">Akses Belanja</a></li>
          <li><a href="#akses_belanja_rinc" data-toggle="tab">Akses Belanja Rinc</a></li>
          <li><a href="#akses_belanja_rinc_sub" data-toggle="tab">Akses Belanja Rinc Sub</a></li>
          <?php
          endif;
        ?>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="pagu_skpd">
          <?= $this->render('tab_pagu_skpd', [
                  'Tahun' => $Tahun,
                  'Kd_Urusan' => $Kd_Urusan,
                  'Kd_Bidang' => $Kd_Bidang,
                  'Kd_Unit' => $Kd_Unit,
                  'Kd_Sub' => $Kd_Sub,
                  'skpd' => $skpd,
                  'model_pagu' => $model_pagu,
                  'status_terbuka' => $status_terbuka
              ]) 
          ?>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="pagu_kegiatan">
          <?= $this->render('tab_pagu_kegiatan', [
              'status_terbuka' => $status_terbuka,
              'TaProgramArr'=> $TaProgramArr,
          ]) ?>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="akses_program">
          <?= $this->render('tab_akses_program', [
              'status_terbuka' => $status_terbuka,
              'TaProgram'=>$TaProgram,
          ]) ?>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="akses_kegiatan">
          <?= $this->render('tab_akses_kegiatan', [
              'Tahun' => $Tahun, 
              'Kd_Urusan' => $Kd_Urusan, 
              'Kd_Bidang' => $Kd_Bidang, 
              'Kd_Unit' => $Kd_Unit, 
              'Kd_Sub' => $Kd_Sub,
              'status_terbuka' => $status_terbuka,
              'TaProgramArr'=>$TaProgramArr,
          ]) ?>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="akses_belanja">
          <?= $this->render('tab_akses_belanja', [
              'status_terbuka' => $status_terbuka,
              'TaProgramArr'=>$TaProgramArr,
          ]) ?>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="akses_belanja_rinc">
          <?= $this->render('tab_akses_belanja_rinc', [
              'status_terbuka' => $status_terbuka,
              'TaProgramArr'=>$TaProgramArr,
          ]) ?>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="akses_belanja_rinc_sub">
          <?= $this->render('tab_akses_belanja_rinc_sub', [
              'status_terbuka' => $status_terbuka,
              'TaProgramArr'=>$TaProgramArr,
          ]) ?>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<?php
    yii\bootstrap\Modal::begin([
        'headerOptions' => ['id' => 'modalHeader'],
        'id' => 'modal',
        // 'size' => 'modal-lg',
        //keeps from closing modal with esc key or by clicking out of the modal.
        // user must click cancel or X to close
        // 'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);
    echo "<div id='modalContent'></div>";
    yii\bootstrap\Modal::end();
?>