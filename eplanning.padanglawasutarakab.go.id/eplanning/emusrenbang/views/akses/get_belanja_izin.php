<?php
use yii\helpers\Url;

?>
<?php
foreach ($data as $key => $value):
  // ubah kegiatan
  $url_ubah_belanja = Url::to(['akses/ubah-izin', 
                              'Tahun' => $value['Tahun'],
                              'Kd_Urusan' => $value['Kd_Urusan'],
                              'Kd_Bidang' => $value['Kd_Bidang'],
                              'Kd_Unit' => $value['Kd_Unit'],
                              'Kd_Sub' => $value['Kd_Sub'],
                              'Kd_Prog' => $value['Kd_Prog'],
                              'Kd_Keg' => $value['Kd_Keg'],
                              'Kd_Rek_1' => $value['Kd_Rek_1'],
                              'Kd_Rek_2' => $value['Kd_Rek_2'],
                              'Kd_Rek_3' => $value['Kd_Rek_3'],
                              'Kd_Rek_4' => $value['Kd_Rek_4'],
                              'Kd_Rek_5' => $value['Kd_Rek_5'],
                              'Nm_Izin' => 'Ubah_Belanja',
                            ]); 
  
  if ($i = $value->getIzin('Ubah_Belanja')) {
    $izin_ubah_belanja = $i->Izin;
  }
  else{
    $izin_ubah_belanja = '1';
  }

  $btn_ubah_belanja = $izin_ubah_belanja == '1'? 'btn-success':'btn-danger';
  $btn_text_ubah_belanja = $izin_ubah_belanja == '1'? 'Terbuka':'Tertutup';
  
  // hapus kegiatan
  $url_hapus_belanja = Url::to(['akses/ubah-izin', 
                              'Tahun' => $value['Tahun'],
                              'Kd_Urusan' => $value['Kd_Urusan'],
                              'Kd_Bidang' => $value['Kd_Bidang'],
                              'Kd_Unit' => $value['Kd_Unit'],
                              'Kd_Sub' => $value['Kd_Sub'],
                              'Kd_Prog' => $value['Kd_Prog'],
                              'Kd_Keg' => $value['Kd_Keg'],
                              'Kd_Rek_1' => $value['Kd_Rek_1'],
                              'Kd_Rek_2' => $value['Kd_Rek_2'],
                              'Kd_Rek_3' => $value['Kd_Rek_3'],
                              'Kd_Rek_4' => $value['Kd_Rek_4'],
                              'Kd_Rek_5' => $value['Kd_Rek_5'],
                              'Nm_Izin' => 'Hapus_Belanja',
                            ]); 
  
  if ($i = $value->getIzin('Hapus_Belanja')) {
    $izin_hapus_belanja = $i->Izin;
  }
  else{
    $izin_hapus_belanja = '1';
  }

  $btn_hapus_belanja = $izin_hapus_belanja == '1'? 'btn-success':'btn-danger';
  $btn_text_hapus_belanja = $izin_hapus_belanja == '1'? 'Terbuka':'Tertutup';
  
  // tambah belanja
  $url_tambah_belanja_rinc = Url::to(['akses/ubah-izin', 
                              'Tahun' => $value['Tahun'],
                              'Kd_Urusan' => $value['Kd_Urusan'],
                              'Kd_Bidang' => $value['Kd_Bidang'],
                              'Kd_Unit' => $value['Kd_Unit'],
                              'Kd_Sub' => $value['Kd_Sub'],
                              'Kd_Prog' => $value['Kd_Prog'],
                              'Kd_Keg' => $value['Kd_Keg'],
                              'Kd_Rek_1' => $value['Kd_Rek_1'],
                              'Kd_Rek_2' => $value['Kd_Rek_2'],
                              'Kd_Rek_3' => $value['Kd_Rek_3'],
                              'Kd_Rek_4' => $value['Kd_Rek_4'],
                              'Kd_Rek_5' => $value['Kd_Rek_5'],
                              'Nm_Izin' => 'Tambah_Belanja_Rinc',
                            ]); 
  
  if ($i = $value->getIzin('Tambah_Belanja_Rinc')) {
    $izin_tambah_belanja_rinc = $i->Izin;
  }
  else{
    $izin_tambah_belanja_rinc = '1';
  }

  $btn_tambah_belanja_rinc = $izin_tambah_belanja_rinc == '1'? 'btn-success':'btn-danger';
  $btn_text_tambah_belanja_rinc = $izin_tambah_belanja_rinc == '1'? 'Terbuka':'Tertutup';

  ?>
    <tr>
      <td class="dat-program">
        <?= $value->refRek5->Nm_Rek_5 ?>
      </td>
      <td>
        <button value="<?= $url_ubah_belanja ?>" class="btn btn-sm <?= $btn_ubah_belanja ?> btn_ubah_izin" ><?= $btn_text_ubah_belanja ?></button>
      </td>
      <td>
        <button value="<?= $url_hapus_belanja ?>" class="btn btn-sm <?= $btn_hapus_belanja ?> btn_ubah_izin" ><?= $btn_text_hapus_belanja ?></button>
      </td>
      <td>
        <button value="<?= $url_tambah_belanja_rinc ?>" class="btn btn-sm <?= $btn_tambah_belanja_rinc ?> btn_ubah_izin" ><?= $btn_text_tambah_belanja_rinc ?></button>
      </td>
    </tr>
  <?php
endforeach;
?>

<script type="text/javascript">

$('.btn_ubah_izin').on('click', function () {
  var alamat = $(this).attr('value');
  var $this = $(this);
  $this.html('proses...');
  // alert("localhost"+alamat);
  $.ajax({ 
    type: "POST",
    url:alamat,
    data:{},
    success: function(isi){
      // alert(isi);
      if (isi == '1') {
        // alert('buka');
        $this.html('Terbuka');
        $this.removeClass('btn-danger').addClass('btn-success');
      }
      else{
        // alert('tutup');
        $this.html('Tertutup');
        $this.removeClass('btn-success').addClass('btn-danger');
      }
    },
    error: function(){
      alert("Gagal Ubah Izin");
    }
  });
});

</script>