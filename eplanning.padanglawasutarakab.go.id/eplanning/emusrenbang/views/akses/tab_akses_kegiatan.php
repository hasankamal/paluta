<?php
use yii\helpers\Url;
use yii\helpers\Html;

// tambah belanja
$url_tutup_kegiatan = Url::to(['akses/tutup-semua-kegiatan', 
                                'Tahun' => $Tahun,
                                'Kd_Urusan' => $Kd_Urusan,
                                'Kd_Bidang' => $Kd_Bidang,
                                'Kd_Unit' => $Kd_Unit,
                                'Kd_Sub' => $Kd_Sub,
                              ]);
?>
<hr>
<div class="form-group">
  <label  class="col-sm-2 control-label">Program</label>
  <div class="col-sm-10">
    <?= Html::dropDownList('s_id', null, $TaProgramArr, ['id' => 'kegiatan_izin_program','class' => 'form-control']) ?>
  </div>
</div>
<div class="form-group">
  <label  class="col-sm-2 control-label"></label>
  <div class="col-sm-10">
    <button class="btn btn-primary load_ajax" 
        data-ajaxtarget='#kegiatan_izin_wrap' 
        data-ajaxload='<?= Url::to(['akses/get-kegiatan-izin']) ?>' 
        data-ajaxdata='#kegiatan_izin_program' >
        Lihat
    </button>
  </div>
</div>
<table class="table table-hover table-bordered">
  <thead>
    <tr>
      <th>Kegiatan</th>
      <th>Ubah</th>
      <th>Hapus</th>
      <th>Tambah Belanja</th>
    </tr>
  </thead>
  <tbody id="kegiatan_izin_wrap">
  </tbody>
</table>
