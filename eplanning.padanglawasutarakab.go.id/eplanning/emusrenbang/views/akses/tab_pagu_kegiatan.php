<?php
use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="form-group">
  <label  class="col-sm-2 control-label">Program</label>
  <div class="col-sm-10">
    <?= Html::dropDownList('s_id', null, $TaProgramArr, ['id' => 'kegiatan_pagu','class' => 'form-control']) ?>
  </div>
</div>
<div class="form-group">
  <label  class="col-sm-2 control-label"></label>
  <div class="col-sm-10">
    <button class="btn btn-primary load_ajax" 
        data-ajaxtarget='#kegiatan_pagu_wrap' 
        data-ajaxload='<?= Url::to(['akses/get-kegiatan-pagu']) ?>' 
        data-ajaxdata='#kegiatan_pagu' 
        id = 'lihat_pagu_kegiatan'
        >
        Lihat
    </button>
  </div>
</div>
<table class="table table-hover table-bordered">
  <thead>
    <tr>
      <th>Kegiatan</th>
      <th>Pagu Terpakai</th>
      <th>Pagu Kegiatan</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody id="kegiatan_pagu_wrap">
  </tbody>
</table>
