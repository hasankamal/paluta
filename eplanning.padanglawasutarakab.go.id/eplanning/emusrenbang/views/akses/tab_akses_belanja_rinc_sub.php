<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="form-group">
  <label  class="col-sm-2 control-label">Program</label>
  <div class="col-sm-10">
    <?= Html::dropDownList('s_id', null, $TaProgramArr, [
                                              'id' => 'kegiatan_izin_program',
                                              'class' => 'form-control load_ajax_change',
                                              'data-ajaxload' => Url::to(["akses/get-kegiatan-arr"]),
                                              'data-ajaxdata' => '#kegiatan_izin_program',
                                              'data-ajaxtarget' => '#belanja_rinc_sub_list_kegiatans',
                                              'prompt'=>'-Pilih Program-'
                                        ]) ?>
  </div>
</div>
<div class="form-group">
  <label  class="col-sm-2 control-label">Kegiatan</label>
  <div class="col-sm-10">
    <select id="belanja_rinc_sub_list_kegiatans" class="form-control load_ajax_change"
                                              data-ajaxload = <?= Url::to(["akses/get-belanja-arr"]) ?>
                                              data-ajaxdata = '#belanja_rinc_sub_list_kegiatans'
                                              data-ajaxtarget = '#belanja_rinc_sub_list_belanjas'
    ></select>
  </div>
</div>
<div class="form-group">
  <label  class="col-sm-2 control-label">Belanja</label>
  <div class="col-sm-10">
    <select id="belanja_rinc_sub_list_belanjas" class="form-control load_ajax_change"
                                              data-ajaxload = <?= Url::to(["akses/get-belanja-rinc-arr"]) ?>
                                              data-ajaxdata = '#belanja_rinc_sub_list_belanjas'
                                              data-ajaxtarget = '#belanja_rinc_sub_list_belanja_subs'
    ></select>
  </div>
</div>
<div class="form-group">
  <label  class="col-sm-2 control-label">Belanja Rinc</label>
  <div class="col-sm-10">
    <select id="belanja_rinc_sub_list_belanja_subs" class="form-control"></select>
  </div>
</div>
<div class="form-group">
  <label  class="col-sm-2 control-label"></label>
  <div class="col-sm-10">
    <button class="btn btn-primary load_ajax" 
        data-ajaxload='<?= Url::to(['akses/get-belanja-rinc-sub-izin']) ?>'
        data-ajaxdata='#belanja_rinc_sub_list_belanja_subs' 
        data-ajaxtarget='#belanja_rinc_sub_izin_wrap'  >
        Lihat
    </button>
  </div>
</div>
<table class="table table-hover table-bordered">
  <thead>
    <tr>
      <th>Kegiatan</th>
      <th>Ubah</th>
      <th>Hapus</th>
      <th>Ubah Volume</th>
    </tr>
  </thead>
  <tbody id="belanja_rinc_sub_izin_wrap">
  </tbody>
</table>
