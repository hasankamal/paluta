<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$url = ['akses/akses-dokumen-selesai-proses',
				'Tahun' => $Tahun,
				'Kd_Urusan' => $Kd_Urusan,
				'Kd_Bidang' => $Kd_Bidang,
				'Kd_Unit' => $Kd_Unit,
				'Kd_Sub' => $Kd_Sub,
			];

?>
<div class="pagu-form">

	<?php $form = ActiveForm::begin(['action' =>$url, 'id'=>'form_pagu']); ?>
    <h3>Apakah anda yakin sudah selesai mengatur hak akses SKPD?</h3>
    <div class="form-group">
        <?= Html::submitButton('Selesai', ['class' => 'btn btn-danger', 'id' => 'btn_simpan']) ?>
    </div>
	<?php ActiveForm::end(); ?>

</div>