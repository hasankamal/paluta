<?php
use yii\helpers\Url;

?>
<?php
foreach ($data as $key => $value):
  // ubah kegiatan
  $url_ubah_kegiatan = Url::to(['akses/ubah-izin', 
                              'Tahun' => $value['Tahun'],
                              'Kd_Urusan' => $value['Kd_Urusan'],
                              'Kd_Bidang' => $value['Kd_Bidang'],
                              'Kd_Unit' => $value['Kd_Unit'],
                              'Kd_Sub' => $value['Kd_Sub'],
                              'Kd_Prog' => $value['Kd_Prog'],
                              'Kd_Keg' => $value['Kd_Keg'],
                              'Nm_Izin' => 'Ubah_Kegiatan',
                            ]); 
  
  if ($i = $value->getIzin('Ubah_Kegiatan')) {
    $izin_ubah_kegiatan = $i->Izin;
  }
  else{
    $izin_ubah_kegiatan = '1';
  }

  $btn_ubah_kegiatan = $izin_ubah_kegiatan == '1'? 'btn-success':'btn-danger';
  $btn_text_ubah_kegiatan = $izin_ubah_kegiatan == '1'? 'Terbuka':'Tertutup';
  
  // ubah kegiatan
  $url_hapus_kegiatan = Url::to(['akses/ubah-izin', 
                              'Tahun' => $value['Tahun'],
                              'Kd_Urusan' => $value['Kd_Urusan'],
                              'Kd_Bidang' => $value['Kd_Bidang'],
                              'Kd_Unit' => $value['Kd_Unit'],
                              'Kd_Sub' => $value['Kd_Sub'],
                              'Kd_Prog' => $value['Kd_Prog'],
                              'Kd_Keg' => $value['Kd_Keg'],
                              'Nm_Izin' => 'Hapus_Kegiatan',
                            ]); 
  
  if ($i = $value->getIzin('Hapus_Kegiatan')) {
    $izin_hapus_kegiatan = $i->Izin;
  }
  else{
    $izin_hapus_kegiatan = '1';
  }

  $btn_hapus_kegiatan = $izin_hapus_kegiatan == '1'? 'btn-success':'btn-danger';
  $btn_text_hapus_kegiatan = $izin_hapus_kegiatan == '1'? 'Terbuka':'Tertutup';

  // tambah belanja
  $url_tambah_belanja = Url::to(['akses/ubah-izin', 
                              'Tahun' => $value['Tahun'],
                              'Kd_Urusan' => $value['Kd_Urusan'],
                              'Kd_Bidang' => $value['Kd_Bidang'],
                              'Kd_Unit' => $value['Kd_Unit'],
                              'Kd_Sub' => $value['Kd_Sub'],
                              'Kd_Prog' => $value['Kd_Prog'],
                              'Kd_Keg' => $value['Kd_Keg'],
                              'Nm_Izin' => 'Tambah_Belanja',
                            ]); 
  
  if ($i = $value->getIzin('Tambah_Belanja')) {
    $izin_tambah_belanja = $i->Izin;
  }
  else{
    $izin_tambah_belanja = '1';
  }

  $btn_tambah_belanja = $izin_tambah_belanja == '1'? 'btn-success':'btn-danger';
  $btn_text_tambah_belanja = $izin_tambah_belanja == '1'? 'Terbuka':'Tertutup';

  ?>
    <tr>
      <td class="dat-program">
        <?= $value->Ket_Kegiatan ?>
      </td>
      <td>
        <button value="<?= $url_ubah_kegiatan ?>" class="btn btn-sm <?= $btn_ubah_kegiatan ?> btn_ubah_izin" ><?= $btn_text_ubah_kegiatan ?></button>
      </td>
      <td>
        <button value="<?= $url_hapus_kegiatan ?>" class="btn btn-sm <?= $btn_hapus_kegiatan ?> btn_ubah_izin" ><?= $btn_text_hapus_kegiatan ?></button>
      </td>
      <td>
        <button value="<?= $url_tambah_belanja ?>" class="btn btn-sm <?= $btn_tambah_belanja ?> btn_ubah_izin" ><?= $btn_text_tambah_belanja ?></button>
      </td>
    </tr>
  <?php
endforeach;
?>

<script type="text/javascript">

$('.btn_ubah_izin').on('click', function () {
  var alamat = $(this).attr('value');
  var $this = $(this);
  $this.html('proses...');
  // alert(alamat);
  $.ajax({ 
    type: "POST",
    url:alamat,
    data:{},
    success: function(isi){
      // alert(isi);
      if (isi == '1') {
        // alert('buka');
        $this.html('Terbuka');
        $this.removeClass('btn-danger').addClass('btn-success');
      }
      else{
        // alert('tutup');
        $this.html('Tertutup');
        $this.removeClass('btn-success').addClass('btn-danger');
      }
    },
    error: function(){
      alert("Gagal Ubah Izin");
    }
  });
});

</script>