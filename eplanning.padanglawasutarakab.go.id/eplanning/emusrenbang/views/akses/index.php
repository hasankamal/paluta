<?php
use yii\helpers\Html;
?>
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Responsive Hover Table</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>Nama SKPD</th>
            <th>Pagu</th>
            <th>Status</th>
          </tr>
          <?php
          	foreach ($skpd as $data) :
          		$pagu = 0;
          		if($nilai = $data->paguSkpd){
          			$pagu = $nilai->pagu;
          		}
          		?>
          			<tr>
			            <td><?= $data->Nm_Sub_Unit ?></td>
			            <td><?= number_format($pagu,2,',','.') ?></td>
			            <td>
			            		<?= Html::a('Lihat', ['akses/detail', 
                                              'Tahun'=>2017,
			            														'Kd_Urusan'=>$data->Kd_Urusan,
                                              'Kd_Bidang'=>$data->Kd_Bidang,
                                              'Kd_Unit'=>$data->Kd_Unit,
                                              'Kd_Sub'=>$data->Kd_Sub,
			            														], ['class' => 'btn btn-success btn-sm']) ?>
			            </td>
			          </tr>
          		<?php
          	endforeach;
          ?>
          
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>