<?php

use yii\helpers\Url;
use yii\helpers\Html;


?>

<table class="table table-hover table-bordered">
  <thead>
    <tr>
      <th>Kode</th>
      <th>Program</th>
      <th>Tambah Kegiatan</th>
    </tr>
  </thead>
  <tbody id="program_izin_wrap">
  <?php
    foreach ($TaProgram as $key => $prog) :
      // tambah belanja
      $url_tambah_kegiatan = Url::to(['akses/ubah-izin', 
                                  'Nm_Izin' => 'Tambah_Kegiatan',
                                  'Tahun' => $prog['Tahun'],
                                  'Kd_Urusan' => $prog['Kd_Urusan'],
                                  'Kd_Bidang' => $prog['Kd_Bidang'],
                                  'Kd_Unit' => $prog['Kd_Unit'],
                                  'Kd_Sub' => $prog['Kd_Sub'],
                                  'Kd_Prog' => $prog['Kd_Prog'],
                                ]); 

      if ($i = $prog->getIzin('Tambah_Kegiatan')) {
        $izin_tambah_kegiatan = $i->Izin;
      }
      else{
        $izin_tambah_kegiatan = '1';
      }

      $btn_tambah_kegiatan = $izin_tambah_kegiatan == '1'? 'btn-success':'btn-danger';
      $btn_text_tambah_kegiatan = $izin_tambah_kegiatan == '1'? 'Terbuka':'Tertutup';
      ?>
        <tr>
          <td><?= $prog->Kd_Urusan.".".$prog->Kd_Bidang.".".$prog->Kd_Unit.".".$prog->Kd_Sub.".".$prog->Kd_Prog ?></td>
          <td><?= $prog->Ket_Prog ?></td>
          <td>
            <button value="<?= $url_tambah_kegiatan ?>" class="btn btn-sm <?= $btn_tambah_kegiatan ?> btn_ubah_izin" ><?= $btn_text_tambah_kegiatan ?></button>
          </td>
        </tr>
      <?php
    endforeach;
  ?>
  </tbody>
</table>
