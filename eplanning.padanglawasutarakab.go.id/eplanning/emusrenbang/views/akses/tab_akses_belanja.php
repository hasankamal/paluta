<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="form-group">
  <label  class="col-sm-2 control-label">Program</label>
  <div class="col-sm-10">
    <?= Html::dropDownList('s_id', null, $TaProgramArr, [
                                              'id' => 'kegiatan_izin_program',
                                              'class' => 'form-control load_ajax_change',
                                              'data-ajaxload' => Url::to(["akses/get-kegiatan-arr"]),
                                              'data-ajaxdata' => '#kegiatan_izin_program',
                                              'data-ajaxtarget' => '#belanja_list_kegiatans',
                                              'prompt'=>'-Pilih Program-'
                                        ]) ?>
  </div>
</div>
<div class="form-group">
  <label  class="col-sm-2 control-label">Kegiatan</label>
  <div class="col-sm-10">
    <select id="belanja_list_kegiatans" class="form-control"></select>
  </div>
</div>
<div class="form-group">
  <label  class="col-sm-2 control-label"></label>
  <div class="col-sm-10">
    <button class="btn btn-primary load_ajax" 
        data-ajaxload='<?= Url::to(['akses/get-belanja-izin']) ?>'
        data-ajaxdata='#belanja_list_kegiatans' 
        data-ajaxtarget='#belanja_izin_wrap'  >
        Lihat
    </button>
  </div>
</div>
<table class="table table-hover table-bordered">
  <thead>
    <tr>
      <th>Kegiatan</th>
      <th>Ubah</th>
      <th>Hapus</th>
      <th>Tambah Belanja Rinc</th>
    </tr>
  </thead>
  <tbody id="belanja_izin_wrap">
  </tbody>
</table>
