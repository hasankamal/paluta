<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\Helper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TaPaguSubUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pagu Sub Unit';
$this->params['breadcrumbs'][] = "Program Kegiatan";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title ?></h3>
                <!-- <?php
                if (Helper::checkRoute('list')) {
                    echo Html::a('Pagu Indikatif Sub Unit', ['list'], ['class' => 'btn btn-warning pull-right']);
                }
                ?> -->

                <!-- Tombol Peraturan-->
                <?php $form = \yii\bootstrap\ActiveForm::begin([
                            'id' => 'search-usulan',
                            'action' => ['ta-pagu-sub-unit/peraturan']

                ]) ?>         
                       
                <?= Html::a('Pagu Indikatif Sub Unit', ['ta-pagu-sub-unit/peraturan', 'No_ID'=>$modelId], ['class' => 'btn btn-warning pull-right']) ?>      
                <?php \yii\bootstrap\ActiveForm::end() ?>

                <!-- End -->


                <?php
                // if (Helper::checkRoute('create')) {
                //     echo Html::a('Tambah Pagu Sub Unit', ['create'], ['class' => 'btn btn-success pull-right']);
                // }
                ?>

                <?php $form = \yii\bootstrap\ActiveForm::begin([
                            'id' => 'search-usulan',
                            'action' => ['ta-pagu-sub-unit/cetak'], 
                            'options' => ['target' => '_blank']
                ]) ?>
 

                <div class="form-group">
                    <div class="col-sm-2">
                        <?= Html::submitButton('&nbsp;Cetak&nbsp;', ['id' => 'cari-submit', 'class' => 'btn btn-primary btn-md']); ?>
                    </div>            
                <?php \yii\bootstrap\ActiveForm::end() ?>
                </div>

              
            </div>
            <!-- /.box-header -->
            <div class="table-responsive">
                <div class="box-body">
                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'options' =>[
                            'class' => 'table table-bordered',
                        ],       
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'Tahun',
                            [
                                'attribute' => 'Kd_Urusan',
                                'value' => 'urusan.Nm_Urusan',
                                'filter'=> $urusan,
                            ],
                            [
                                'attribute' => 'Kd_Bidang',
                                'value' => 'bidang.Nm_Bidang',
                                'filter'=> $bidang,
                            ],
                            [
                                'attribute' => 'Kd_Unit',
                                'value' => 'unit.Nm_Unit',
                                'filter'=> $unit,
                            ],
                            [
                                'attribute' => 'Kd_Sub',
                                'value' => 'sub_unit.Nm_Sub_Unit',
                                'filter'=> $subunit,
                            ],
                            [
                                'label' => 'Pagu',
                                'value' => 'pagu',
                                'format' => ['decimal', 2],
                                'contentOptions' => ['class' => 'text-right']
                            ],
                            [
                                'label' => 'Pagu Serapan',
                                'format' => ['decimal', 2],
                                'contentOptions' => ['class' => 'text-right'],
                                'value' => function($model) {return $model->getTaBelanjaRincSubs()->sum('Total');},

                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => Helper::filterActionColumn('{view}{update}{delete}'),
                            ],
                        ],
                    ]);
                    ?>
                    
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>

<!-- modal musrenbang kelurahan -->
<div class="modal fade" id="modal_peraturan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'action' => ['ta-pagu-sub-unit/peraturan']]) ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span  ></button>
                <h4 class="modal-title" id="myModalLabel">Silahkan Upload Peraturan Untuk Mengubah Pagu</h4>
            </div>
            <div class="modal-body">
         
            <!-- <?php echo $form->field($peraturan, 'Tahun')->textInput(['maxlength' => true]) ?>  -->

            <?= $form->field($peraturan, 'No_Peraturan')->textInput(['maxlength' => true]) ?>

            <?= $form->field($peraturan, 'Tgl_Peraturan')->textInput() ?>

            <?= $form->field($peraturan, 'Uraian')->textInput(['maxlength' => true]) ?>

            <!-- <?php echo $form->field($media, 'Jenis_Dokumen')->textInput(['maxlength' => true]) ?>
 -->
          
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Tutup</button>
                <?= Html::submitButton('Ubah Pagu', ['class' => 'btn btn-danger', 'id' => 'kirim_btn']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
