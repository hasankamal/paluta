<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\Helper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;


/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TaPaguSubUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pagu Sub Unit';
$this->params['breadcrumbs'][] = "Program Kegiatan";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="table-responsive">
                <div class="box-body">         
                           

            <?php $form = ActiveForm::begin([
                'options'=> [ 'id' => 'modalContent','enctype' => 'multipart/form-data']]);
                ?>
                   <?= $form->field($upload, 'pdfFile[]')->widget(FileInput::className(), ['options' => [
                                    'multiple' => true], 'pluginOptions' => ['maxFileCount' => 2]]) ?>

                <?php ActiveForm::end(); ?>          
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>