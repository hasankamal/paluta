<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\Helper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

$this->registerJsFile(
        '@web/js/modalperaturan.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TaPaguSubUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pagu Sub Unit';
$this->params['breadcrumbs'][] = "Pagu Indikatif";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <section class="content-header">
                <h4>
                Peraturan Untuk Mengubah Pagu Infikatif
                </h4>
            </section>
            <hr>
           <!-- /.box-header -->
            <div class="table-responsive">
                <div class="box-body"> 

                 <?= Html::a('Upload File Peraturan', FALSE, ['value' => Url::to(['ta-pagu-sub-unit/upload-peraturan','No_ID'=>$No_ID]), 'class' => 'loadMainContent btn btn-success', 'id'=>'modalButton']); ?>        
               

                <?php Pjax::begin(); ?>
                    <?=
                    GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['attribute' => 'Nama Berkas',
                            'format' => 'raw',
                            'value' => function($model, $key, $index) {
                                return '<h5>' . $model->kdMedia->Judul_Media . '</h5>';
                            },
                        ],
                            ['attribute' => 'type',
                                'format' => 'raw',
                                'value' => function($model, $key, $index) {
                                    return '<p>' . $model->Jenis_Dokumen . '</p>';
                                },
                            ],
                      
                        ],
                        ]);
                        ?>
                    <?php Pjax::end(); ?>    

                <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'action' => ['ta-pagu-sub-unit/peraturan']]) ?>

                <?= $form->field($peraturan, 'No_ID')->hiddenInput(['value' => $No_ID])->label(false) ?>
             
                <?= $form->field($peraturan, 'No_Peraturan')->textInput(['maxlength' => true]) ?>
<!-- 
                <?php echo $form->field($peraturan, 'Tgl_Peraturan')->textInput(['class'=>'form-control datepicker']) ?> -->


                <?= $form->field($peraturan, 'Tgl_Peraturan')->widget(\yii\jui\DatePicker::classname(), [
                        'dateFormat' => 'dd-MM-y',
                        'options'=>[
                        'size'=> "80%"
                        ]
                ])  ?>

                <?= $form->field($peraturan, 'Uraian')->textInput(['maxlength' => true]) ?>

                
               

               <?php if ($kunciMedia) : ?>
               <?= Html::submitButton('Ubah Pagu', ['class' => 'btn btn-lg btn-danger pull-right']) ?>   
            
                <?php else : ?>
                
               <?= Html::submitButton('Ubah Pagu', ['class' => 'btn btn-lg btn-danger pull-right disabled']) ?>   
               <?php endif; ?>
             
                
                <?php ActiveForm::end(); ?>    
                
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h4> Upload File Peraturan<h/4>',
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
]);
echo "<div id='modalContent'><div style='text-align:center'><img src='web\images\load.gif'></div></div>";

Modal::end();
?>

<script type="text/javascript">
    $('.datepicker').datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      language: "id"
    });
</script>








