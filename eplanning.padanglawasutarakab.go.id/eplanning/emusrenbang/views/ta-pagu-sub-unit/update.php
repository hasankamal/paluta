<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaPaguSubUnit */

$this->title = 'Ubah Pagu Sub Unit: ' . $model->Tahun;
$this->params['breadcrumbs'][] = ['label' => 'Pagu Sub Unit', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tahun, 'url' => ['view', 'Tahun' => $model->Tahun, 'Kd_Urusan' => $model->Kd_Urusan, 'Kd_Bidang' => $model->Kd_Bidang, 'Kd_Unit' => $model->Kd_Unit, 'Kd_Sub' => $model->Kd_Sub]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="ta-pagu-sub-unit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
