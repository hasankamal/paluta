<?php
use yii\helpers\Url;

foreach($user as $u):
  $url = Url::to(['log-user/logout-user', 'id' => $u->id]);
  ?>
    <li class="item">
      <div class="product-img">
        <button class="btn btn-danger btn_logout" data-tujuan="<?= $url ?>">Logout</button>
        &nbsp
      </div>
      <div class="product-info">
        <a href="javascript:void(0)" class="product-title"> <?= $u->username ?>
          <span class="label label-warning pull-right"><?= $u->lastLog->created_at ?></span></a>
            <span class="product-description">
              <?= $u->lastLog->kegiatan ?>
            </span>
      </div>
    </li>
  <?php
endforeach;
?>

<script type="text/javascript">
$(".btn_logout").click(function(){
  var tujuan = $(this).data('tujuan');
  // alert(tujuan);
  $.ajax({ 
    type: "GET",
    url:tujuan,
    data:{},
    success: function(isi){
       if (isi == 1) {
         alert("Logout User Berhasil");
         $(tujuan).html("");
       }
       else{
         alert("Logout User Gagal");
       }
    },
    error: function(){
      alert("Logout Proses Gagal");
    }
  });
});

// window.setInterval(function(){
//   ambildata();
// }, 5000);
</script>