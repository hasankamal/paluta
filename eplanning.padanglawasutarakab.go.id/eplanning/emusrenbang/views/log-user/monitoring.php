<?php
use yii\helpers\Url;

$this->registerJsFile(
  '@web/js/monitoring.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<div class="row">
  <section class="col-lg-5">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">User Login</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <ul class="products-list product-list-in-box" id="user_login_wrap">
          mengambil data...
        </ul>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer -->
    </div>
  </section>
  <section class="col-lg-7">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">User Log</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div style="height: 400px; overflow-y: auto">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
              <tr>
                <th>Username</th>
                <th>Ip</th>
                <th>Kegiatan</th>
                <th>Browser / device</th>
              </tr>
              </thead>
              <tbody id="log_wrap">
              <?php
              foreach($log as $l):
                ?>
                  <tr>
                    <td><?= $l->username."<br/>".$l->created_at  ?></td>
                    <td><?= $l->ip ?></td>
                    <td><?= $l->kegiatan ?></td>
                    <td><?= $l->browser."<br/>".$l->os ?></td>
                  </tr>
                <?php
              endforeach;
              ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer -->
    </div>
  </section>
</div>