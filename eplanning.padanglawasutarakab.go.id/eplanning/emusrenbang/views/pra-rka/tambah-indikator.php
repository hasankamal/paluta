<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Misi */

$this->title = 'Indikator';
$this->params['breadcrumbs'][] = $this->title;
	
$url = ['pra-rka/indikator-proses',
				'Tahun' => $Tahun,
				'Kd_Urusan' => $Kd_Urusan,
				'Kd_Bidang' => $Kd_Bidang,
				'Kd_Unit' => $Kd_Unit,
				'Kd_Sub' => $Kd_Sub,
				'Kd_Prog' => $Kd_Prog,
				'Kd_Keg' => $Kd_Keg,
];
?>
<div>
	<?php
  $form = ActiveForm::begin(['action' =>$url,'id' => 'tambah_indikator_form']);
		?>
		<div class="col-md-12">
			<div class="row box box-primary">
				<div class="col-md-2">
					<h4>Pagu Anggaran N+1</h4>
				</div>
				<div class="col-md-3">
					<div class="form-group">
				    <label >&nbsp;</label>
				    <input type="text" class="form-control" name="Pagu_Anggaran_Nt1" value="<?= $model_kegiatan->Pagu_Anggaran_Nt1 ?>">
				  </div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row box box-primary">
				<div class="col-md-2">
					<h4><?= $model_keluaran->refIndikator->Nm_Indikator?></h4>
				</div>
				<div class="col-md-4">
					<div class="form-group">
				    <label >Tolak Ukur</label>
				    <input type="text" class="form-control" name="keluaran[tolak_ukur]" value="<?= $model_keluaran->Tolak_Ukur ?>">
				  </div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
				    <label >Target</label>
				    <input type="text" class="form-control angka" name="keluaran[target]" value="<?= $model_keluaran->Target_Angka ?>">
				  </div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
				    <label >&nbsp</label>
				    <select  class="form-control input-sm selects" name="keluaran[satuan]">
				    	<?php
				    		foreach ($satuan as $key => $sat):
				    			$selected = '';
				    			if(isset($model_keluaran->Target_Uraian)){
				    				$nilai = $model_keluaran->Target_Uraian;

				    				if ($nilai == $sat) {
				    					$selected = 'selected';
				    				}
				    			}

				    		?>
				    				<option value="<?= $sat ?>" <?= $selected ?> ><?= $sat ?></option>
				    		<?php
				    		endforeach;
				    	?>
				    </select>
				  </div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row box box-primary">
				<div class="col-md-2">
					<h4><?= $model_hasil->refIndikator->Nm_Indikator?></h4>
				</div>
				<div class="col-md-4">
					<div class="form-group">
				    <label >Tolak Ukur</label>
				    <input type="text" class="form-control" name="hasil[tolak_ukur]" value="<?= $model_hasil->Tolak_Ukur ?>">
				  </div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
				    <label >Target</label>
				    <input type="text" class="form-control angka" name="hasil[target]" value="<?= $model_hasil->Target_Angka ?>">
				  </div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
				    <label >&nbsp</label>
				    <select  class="form-control input-sm selects" name="hasil[satuan]">
				    	<?php
				    		foreach ($satuan as $key => $sat):
				    			$selected = '';
				    			if(isset($model_hasil->Target_Uraian)){
				    				$nilai = $model_hasil->Target_Uraian;

				    				if ($nilai == $sat) {
				    					$selected = 'selected';
				    				}
				    			}

				    		?>
				    				<option value="<?= $sat ?>" <?= $selected ?> ><?= $sat ?></option>
				    		<?php
				    		endforeach;
				    	?>
				    </select>
				  </div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row box box-primary">
				<div class="col-md-2">
					<h4><?= $model_n1->refIndikator->Nm_Indikator?></h4>
				</div>
				<div class="col-md-4">
					<div class="form-group">
				    <label >Tolak Ukur</label>
				    <input type="text" class="form-control" name="n1[tolak_ukur]" value="<?= $model_n1->Tolak_Ukur ?>">
				  </div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
				    <label >Target</label>
				    <input type="text" class="form-control angka" name="n1[target]" value="<?= $model_n1->Target_Angka ?>">
				  </div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
				    <label >&nbsp</label>
				    <select  class="form-control input-sm selects" name="n1[satuan]">
				    	<?php
				    		foreach ($satuan as $key => $sat):
				    			$selected = '';
				    			if(isset($model_n1->Target_Uraian)){
				    				$nilai = $model_n1->Target_Uraian;

				    				if ($nilai == $sat) {
				    					$selected = 'selected';
				    				}
				    			}

				    		?>
				    				<option value="<?= $sat ?>" <?= $selected ?> ><?= $sat ?></option>
				    		<?php
				    		endforeach;
				    	?>
				    </select>
				  </div>
				</div>
			</div>
		</div>
		<?php
		
    // echo Html::submitButton('Daftar', ['class' => 'btn btn-primary', 'name' => 'signup-button']);

	ActiveForm::end();
	?>
</div>

