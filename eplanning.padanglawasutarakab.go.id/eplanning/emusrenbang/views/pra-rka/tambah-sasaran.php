<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Misi */

$this->title = 'Sasaran';
$this->params['breadcrumbs'][] = $this->title;




// $this->registerCssFile(
//         '@web/plugins/select2/select2.css', ['depends' => [\yii\web\JqueryAsset::className()]]
// );

// $this->registerJsFile(
//         '@web/plugins/select2/select2.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
// );
if($status==1){
	$url = ['pra-rka/ubah-sasaran-proses',
				'Tahun' => $Tahun,
				'Kd_Urusan' => $Kd_Urusan,
				'Kd_Bidang' => $Kd_Bidang,
				'Kd_Unit' => $Kd_Unit,
				'Kd_Sub' => $Kd_Sub,
				'Kd_Prog' => $Kd_Prog,
				'Kd_Keg' => $Kd_Keg,
	];
}
else{
	$url = ['pra-rka/tambah-sasaran-proses'];
}
// $disable = false;  //pemilihan kegiatan dari kamus kegiatan tetap terbuka
//$url = ['pra-rka/tambah-sasaran-proses'];
?>
<div class="sasaran-form">
  <?php $form = ActiveForm::begin(['action' =>$url,'id' => 'tambah_sasaran_form']); ?>

  	<?php echo $form->field($model, 'Tahun')->hiddenInput(['value'=> $Tahun])->label(false); ?>
  	<?php echo $form->field($model, 'Kd_Urusan')->hiddenInput(['value'=> $Kd_Urusan])->label(false); ?>
  	<?php echo $form->field($model, 'Kd_Bidang')->hiddenInput(['value'=> $Kd_Bidang])->label(false); ?>
  	<?php echo $form->field($model, 'Kd_Unit')->hiddenInput(['value'=> $Kd_Unit])->label(false); ?>
  	<?php echo $form->field($model, 'Kd_Sub')->hiddenInput(['value'=> $Kd_Sub])->label(false); ?>
  	<?php echo $form->field($model, 'Kd_Prog')->hiddenInput(['value'=> $Kd_Prog])->label(false); ?>
  	<?php echo $form->field($model, 'Kd_Keg')->hiddenInput(['value'=> $Kd_Keg])->label(false); ?>
  	
	
	<?= $form->field($model, 'Sasaran')->textInput(['maxlength' => true, 'class'=>'form-control input-sm']) ?>
	<!-- 	<?php Html::submitButton('Tambah' , ['class' => 'btn btn-success' ])?> -->
</div>
	
  <?php ActiveForm::end(); ?>

</div>


