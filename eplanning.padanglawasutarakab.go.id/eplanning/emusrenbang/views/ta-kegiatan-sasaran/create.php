<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaKegiatanSasaran */

?>
<div class="ta-kegiatan-sasaran-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
