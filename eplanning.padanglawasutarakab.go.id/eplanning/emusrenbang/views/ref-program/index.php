<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RefProgramSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Program';
$this->params['breadcrumbs'][] = "Program Kegiatan";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title ?></h3>
                <?php
                    if (Helper::checkRoute('create')) {
                        //echo Html::a('Tambah Program Wajib', ['createwajib'], ['class' => 'btn btn-success pull-right']);
                        echo Html::a('Tambah Program', ['create'], ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 5px;']);
                    }
                ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <table class="table-hover">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'header' => 'No.'
                        ],
                        // [
                        //     'attribute' => 'Nm_Urusan',
                        //     'format' => 'text',
                        //     'value' => function($model){ return $model->Kd_Urusan.":".$model->Nm_Urusan; }
                        // ],
                        // [
                        //     'attribute' => 'Nm_Bidang',
                        //     'format' => 'text',
                        //     'value' => function($model){ return $model->Kd_Bidang.":".$model->Nm_Bidang; }
                        // ],
                        [
                            'attribute' => 'Kd_Urusan',
                            'format' => 'text',
                            'value' => 'kdUrusan.Nm_Urusan',
                            'filter' => $data_urusan
                        ],
                        [
                            'attribute' => 'Kd_Bidang',
                            'format' => 'text',
                            'value' => 'kdBidang.Nm_Bidang',
                            'filter' => $data_bidang
                        ],
                        [
                            'attribute' => 'Kd_Prog',
                            'format' => 'text',
                            'value' => 'Kd_Prog'
                        ],
                        [
                            'attribute' => 'Ket_Program',
                            'format' => 'text',
                            'value' => function($model){ return $model->Kd_Prog.":".$model->Ket_Program; }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => Helper::filterActionColumn('{view}{update}{delete}'),
                        ],
                        [
                            'attribute' => 'Sasaran',
                            'format' => 'raw',
                            'value' => function($model){ 
                                $Kd_Urusan = $model->Kd_Urusan;
                                $Kd_Bidang = $model->Kd_Bidang;
                                $Kd_Prog = $model->Kd_Prog;

                                $url = Url::to(['ref-program/sasaran', 
                                                'Kd_Urusan' => $Kd_Urusan, 
                                                'Kd_Bidang' => $Kd_Bidang, 
                                                'Kd_Prog' => $Kd_Prog, 
                                                ]);
                                return "<a href=".$url." class='btn btn-sm btn-primary'>Sasaran</a>"; 
                            }
                        ],
                    ],
                ]);
                ?>
                </table>
            </div>
        </div>            
    </div>
</div>
