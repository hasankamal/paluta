<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = ($model->isNewRecord ? 'Tambah' : 'Edit').' Program Pilihan';
$this->params['breadcrumbs'][] = "Program Kegiatan";
$this->params['breadcrumbs'][] = ['label' => 'Program Pilihan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-program-create">
	<div class="ref-program-form">
    <div class="box box-success"> 
        <div class="box-body">
			<?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'Sasaran')->textInput(['maxlength' => true, 'id'=>'prog']) ?>

    			<div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
			<?php ActiveForm::end(); ?>
        </div>
    </div>
	</div>
</div>
