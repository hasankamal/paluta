
$(".load_ajax").click(function(){
  var target= $(this).data('ajaxtarget');
  var data= $(this).data('ajaxdata');
  var load= $(this).data('ajaxload');
  var key = $(data).val();
  // alert(load);
  // alert(target+" "+load+" "+data);
  // alert("localhost"+load+"&key="+key);

  $(target).html("mengambil data ...");

  $.ajax({ 
    type: "GET",
    url:load,
    data:{key:key},
    success: function(isi){
      $(target).html(isi);
    },
    error: function(){
      alert("Ambil data gagal");
     $(target).html("");
    }
  });
});

$(".load_ajax_change").change(function(){
  var target= $(this).data('ajaxtarget');
  var data= $(this).data('ajaxdata');
  var load= $(this).data('ajaxload');
  var key = $(this).val();
  // alert(load);
  // alert("localhost"+load+"&key="+key);
  // $("#belanja_list_kegiatans").html('<option>Medan</option>');
  $(target).html('<option>loading...</option>');

  // $(target).html("mengambil data ...");

  $.ajax({ 
    type: "GET",
    url:load,
    data:{key:key},
    success: function(isi){
      $(target).html(isi);
    },
    error: function(){
      alert("Ambil data gagal");
     $(target).html("");
    }
  });
});

$('.btn_ubah_izin').on('click', function () {
  var alamat = $(this).attr('value');
  var $this = $(this);
  $this.html('proses...');
  // alert('localhost'+alamat);
  
  $.ajax({ 
    type: "POST",
    url:alamat,
    data:{},
    success: function(isi){
      // alert(isi);
      if (isi == '1') {
        // alert('buka');
        $this.html('Terbuka');
        $this.removeClass('btn-danger').addClass('btn-success');
      }
      else{
        // alert('tutup');
        $this.html('Tertutup');
        $this.removeClass('btn-success').addClass('btn-danger');
      }
    },
    error: function(){
      alert("Gagal Ubah Izin");
    }
  });
});

$( ".btn_modal" ).click(function() {
  var alamat = $(this).attr('value');
  var judul = $(this).attr('title');
  // alert(alamat);
  $('#modal').find('#modalContent').html('Mengambil Data..');

  if ($('#modal').data('bs.modal').isShown) {
    $('#modal').find('#modalContent')
                .load(alamat);
    $('#modalHeader').html('<h4>'+judul +'</h4>'); 
  }
  else{
     $('#modal').modal('show')
              .find('#modalContent')
              .load(alamat);
    $('#modalHeader').html('<h4>'+judul +'</h4>'); 
  }
});