<?php
use yii\helpers\Html;

if (Yii::$app->controller->action->id === 'login') {
    /**
     * Do not use this code in your template. Remove it. 
     * Instead, use the code  $this->layout = '//main-login'; in your controller.
     */
    echo $this->render(
            'main-front', ['content' => $content]
    );
} 
else if(Yii::$app->levelcomponent->getAktif() == 0){
    $cookies = Yii::$app->response->cookies;
    // $log = new Savelog();
    // $log->save('logout berhasil', 'logout', '', ''); //pesan, kegiatan, tabel, id dari tabel
    Yii::$app->user->logout();
    unset($cookies['limit']);
    Yii::$app->session->destroy();
    Yii::$app->response->redirect(['site/login']);
}
else {
    if (class_exists('emusrenbang\assets\AppAsset')) {
        emusrenbang\assets\AppAsset::register($this);
    } else {
        emusrenbang\assets\AppAsset::register($this);
    }
    dmstr\web\AdminLteAsset::register($this);
    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body class="<?= \dmstr\helpers\AdminLteHelper::skinClass() ?>">
            <?php $this->beginBody() ?>
            <div class="wrapper">

                <?=
                $this->render(
                        'header.php', ['directoryAsset' => $directoryAsset]
                )
                ?>
                <?=
                $this->render(
                        'left.php', ['directoryAsset' => $directoryAsset]
                )
                ?>

                <?=
                $this->render(
                        'content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
                )
                ?>

            </div>
            <?php $this->endBody() ?>
        </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
