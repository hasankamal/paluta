<?php

namespace emusrenbang\controllers;

use Yii;
use common\models\TaMisi;
use common\models\search\TaMisiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use emusrenbang\models\Savelog;

/**
 * TaMisiController implements the CRUD actions for TaMisi model.
 */
class TaMisiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaMisi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $log = new Savelog();
        $log->save('Akses Menu Misi SKPD Berhasil', 'Menu Misi SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $searchModel = new TaMisiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaMisi model.
     * @param integer $Tahun
     * @param integer $Kd_Urusan
     * @param integer $Kd_Bidang
     * @param integer $Kd_Unit
     * @param integer $Kd_Sub
     * @param integer $No_Misi
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $No_Misi)
    {
        $log = new Savelog();
        $log->save('Akses View Misi SKPD Berhasil', 'View Misi SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        return $this->render('view', [
            'model' => $this->findModel($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $No_Misi),
        ]);
    }

    /**
     * Creates a new TaMisi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $log = new Savelog();
        $log->save('Akses Create Misi SKPD Berhasil', 'Create Misi SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $model = new TaMisi();

        // $model->Tahun=( date('Y')+1 );
        // $identity=Yii::$app->user->identity;
        // $model->Kd_Urusan=$identity->id_urusan;
        // $model->Kd_Bidang=$identity->id_bidang;
        // $model->Kd_Unit=$identity->id_skpd;
        // $model->Kd_Sub=$identity->id_subunit;

        $unit = Yii::$app->levelcomponent->getUnit();
        $model->Tahun=( date('Y'));
        $model->Kd_Urusan=$unit->Kd_Urusan;
        $model->Kd_Bidang=$unit->Kd_Bidang;
        $model->Kd_Unit=$unit->Kd_Unit;
        $model->Kd_Sub=$unit->Kd_Sub_Unit;



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tahun' => $model->Tahun, 'Kd_Urusan' => $model->Kd_Urusan, 'Kd_Bidang' => $model->Kd_Bidang, 'Kd_Unit' => $model->Kd_Unit, 'Kd_Sub' => $model->Kd_Sub, 'No_Misi' => $model->No_Misi]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaMisi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $Tahun
     * @param integer $Kd_Urusan
     * @param integer $Kd_Bidang
     * @param integer $Kd_Unit
     * @param integer $Kd_Sub
     * @param integer $No_Misi
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $No_Misi)
    {
        $log = new Savelog();
        $log->save('Akses Update Misi SKPD Berhasil', 'Update Misi SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $model = $this->findModel($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $No_Misi);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tahun' => $model->Tahun, 'Kd_Urusan' => $model->Kd_Urusan, 'Kd_Bidang' => $model->Kd_Bidang, 'Kd_Unit' => $model->Kd_Unit, 'Kd_Sub' => $model->Kd_Sub, 'No_Misi' => $model->No_Misi]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaMisi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $Tahun
     * @param integer $Kd_Urusan
     * @param integer $Kd_Bidang
     * @param integer $Kd_Unit
     * @param integer $Kd_Sub
     * @param integer $No_Misi
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $No_Misi)
    {
        $log = new Savelog();
        $log->save('Akses Delete Misi SKPD Berhasil', 'Delete Misi SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $this->findModel($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $No_Misi)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaMisi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $Tahun
     * @param integer $Kd_Urusan
     * @param integer $Kd_Bidang
     * @param integer $Kd_Unit
     * @param integer $Kd_Sub
     * @param integer $No_Misi
     * @return TaMisi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $No_Misi)
    {
        if (($model = TaMisi::findOne(['Tahun' => $Tahun, 'Kd_Urusan' => $Kd_Urusan, 'Kd_Bidang' => $Kd_Bidang, 'Kd_Unit' => $Kd_Unit, 'Kd_Sub' => $Kd_Sub, 'No_Misi' => $No_Misi])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
