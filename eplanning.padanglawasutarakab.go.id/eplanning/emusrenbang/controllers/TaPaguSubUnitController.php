<?php

namespace emusrenbang\controllers;

use Yii;
use common\models\TaPaguSubUnit;
use common\models\search\TaPaguSubUnitSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\TaSubUnit;
use common\models\TaPaguUnit;
use common\models\RefUrusan;
use common\models\RefBidang;
use common\models\RefUnit;
use common\models\RefSubUnit;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;
use common\models\TaPaguSubUnitRiwayat;
use yii\web\UploadedFile;
use common\models\RefMedia;
use emusrenbang\models\TaPeraturanPagu;
use common\models\TaPaguMedia;
use common\models\search\TaPaguMediaSearch;
use emusrenbang\models\Savelog;


/**
 * TaPaguSubUnitController implements the CRUD actions for TaPaguSubUnit model.
 */
class TaPaguSubUnitController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaPaguSubUnit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $log = new Savelog();
        $log->save('Akses Pagu Sub Unit Berhasil', 'Menu Pagu Sub Unit', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $model = TaPaguSubUnit::find()->all();
        $searchModel = new TaPaguSubUnitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $urusan = ArrayHelper::map(RefUrusan::find()->all(),
                          'Kd_Urusan',
                          'Nm_Urusan'
                        );

        $bidang = ArrayHelper::map(RefBidang::find()
                    ->where(['Kd_Urusan' => $searchModel->Kd_Urusan])
                    ->all(),
                    'Kd_Bidang',
                    'Nm_Bidang'
                );

        $unit = ArrayHelper::map(RefUnit::find()
                ->where(['Kd_Urusan' => $searchModel->Kd_Urusan])
                ->andwhere(['Kd_Bidang' => $searchModel->Kd_Bidang])
                ->all(),
                'Kd_Unit',
                'Nm_Unit'
            );

        $subunit = ArrayHelper::map(RefSubUnit::find()
                        ->where(['Kd_Urusan' => $searchModel->Kd_Urusan])
                        ->andwhere(['Kd_Bidang' => $searchModel->Kd_Bidang])
                        ->andwhere(['Kd_Unit' => $searchModel->Kd_Unit])
                        ->all(),
                          'Kd_Sub',
                          'Nm_Sub_Unit'
                        );

        $model = TaPeraturanPagu::find()
         ->max('No_ID');
        $modelId= $model+1;

      // $RefTahapan = ArrayHelper::map(RefTahapan::find()->orderBy('No_Urut')->all(), 'Kd_Tahapan', 'Uraian');
      // $RefPeraturan = ArrayHelper::map(RefPeraturan::find()->all(), 'Kd_Peraturan', 'Nm_Peraturan');


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'urusan' => $urusan,
            'bidang' => $bidang,
            'unit' => $unit,
            'subunit' => $subunit,
            'peraturan' => new \emusrenbang\models\TaPeraturanPagu(),
            'media' => new \common\models\TaPaguMedia(),
            'upload' => new \eperencanaan\models\UploadForm(),
            'modelId' => $modelId,
        ]);
    }

    /**
     * Displays a single TaPaguSubUnit model.
     * @param string $Tahun
     * @param integer $Kd_Urusan
     * @param integer $Kd_Bidang
     * @param integer $Kd_Unit
     * @param integer $Kd_Sub
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)
    {
        return $this->render('view', [
            'model' => $this->findModel($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub),
        ]);
    }

    /**
     * Creates a new TaPaguSubUnit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaPaguSubUnit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tahun' => $model->Tahun, 'Kd_Urusan' => $model->Kd_Urusan, 'Kd_Bidang' => $model->Kd_Bidang, 'Kd_Unit' => $model->Kd_Unit, 'Kd_Sub' => $model->Kd_Sub]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaPaguSubUnit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Tahun
     * @param integer $Kd_Urusan
     * @param integer $Kd_Bidang
     * @param integer $Kd_Unit
     * @param integer $Kd_Sub
     * @return mixed
     */
    public function actionUpdate($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)
    {
        $model = $this->findModel($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tahun' => $model->Tahun, 'Kd_Urusan' => $model->Kd_Urusan, 'Kd_Bidang' => $model->Kd_Bidang, 'Kd_Unit' => $model->Kd_Unit, 'Kd_Sub' => $model->Kd_Sub]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaPaguSubUnit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Tahun
     * @param integer $Kd_Urusan
     * @param integer $Kd_Bidang
     * @param integer $Kd_Unit
     * @param integer $Kd_Sub
     * @return mixed
     */
    public function actionDelete($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)
    {
        $this->findModel($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaPaguSubUnit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Tahun
     * @param integer $Kd_Urusan
     * @param integer $Kd_Bidang
     * @param integer $Kd_Unit
     * @param integer $Kd_Sub
     * @return TaPaguSubUnit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)
    {
        if (($model = TaPaguSubUnit::findOne(['Tahun' => $Tahun, 'Kd_Urusan' => $Kd_Urusan, 'Kd_Bidang' => $Kd_Bidang, 'Kd_Unit' => $Kd_Unit, 'Kd_Sub' => $Kd_Sub])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionList() {
        $log = new Savelog();
        $log->save('Akses Ubah Pagu Sub Unit', 'Ubah Pagu Sub Unit', '', ''); //pesan, kegiatan, tabel, id dari tabel
       

        $modelPeraturan = TaPeraturanPagu::find()
         ->max('No_ID');

        $date = date('Y');
        
        $unit = TaSubUnit::find()->all();
        foreach ($unit as $data) {
            $cek = TaPaguSubUnit::findOne([
                        'Tahun' => $date,
                        'Kd_Urusan' => $data->Kd_Urusan,
                        'Kd_Bidang' => $data->Kd_Bidang,
                        'Kd_Unit' => $data->Kd_Unit,
                        'Kd_Sub' => $data->Kd_Sub,
            ]);
            if (!($cek)) {
                $new = new TaPaguSubUnit();
                $new->Kd_Urusan = $data->Kd_Urusan;
                $new->Kd_Bidang = $data->Kd_Bidang;
                $new->Kd_Unit = $data->Kd_Unit;
                $new->Kd_Sub =$data->Kd_Sub;
                $new->Tahun = date('Y');
                $new->pagu = 0;
                $new->save();
            }

            $cekUnit = TaPaguUnit::findOne([    
                    'Tahun' => $date,
                    'Kd_Urusan' => $data->Kd_Urusan,
                    'Kd_Bidang' => $data->Kd_Bidang,
                    'Kd_Unit' => $data->Kd_Unit,
            ]);
            if (!($cekUnit)) {
                $new = new TaPaguUnit();
                $new->Kd_Urusan = $data->Kd_Urusan;
                $new->Kd_Bidang = $data->Kd_Bidang;
                $new->Kd_Unit = $data->Kd_Unit;
                $new->Tahun = date('Y');
                $new->pagu = 0;
                $new->save();
            } 
        }

        if ($post = Yii::$app->request->post()) {
            foreach ($post['pagu'] as $Kd_Urusan => $a) {
                foreach ($a as $Kd_Bidang => $b) {
                    foreach ($b as $Kd_Unit => $c) {
                        $paguUnit = 0;
                        foreach ($c as $Kd_Sub => $pagu) {
                            $updatePaguSubUnit = TaPaguSubUnit::findOne([
                                        'Kd_Urusan' => $Kd_Urusan,
                                        'Kd_Bidang' => $Kd_Bidang,
                                        'Kd_Unit' => $Kd_Unit,
                                        'Kd_Sub' => $Kd_Sub,
                                        'Tahun' => $date,
                            ]);
                            $pagu = str_replace(".", "", $pagu);
                            $updatePaguSubUnit->pagu = $pagu;
                            // $updatePaguSubUnit->update();
                           
                            if($updatePaguSubUnit->update()){
                              $model_riwayat = new TaPaguSubUnitRiwayat();
                              $model_riwayat->attributes = $updatePaguSubUnit->attributes;
                              $model_riwayat->Tanggal_Riwayat=date('Y-m-d');
                              $model_riwayat->Keterangan_Riwayat="Ubah Pagu";
                              $model_riwayat->Id_Peraturan = $modelPeraturan;
                              $model_riwayat->Waktu_Riwayat = time();
                              $model_riwayat->save();
                            }
                             $paguUnit += $pagu;
                        }
                        $updatePaguUnit = TaPaguUnit::findOne([
                                    'Kd_Urusan' => $Kd_Urusan,
                                    'Kd_Bidang' => $Kd_Bidang,
                                    'Kd_Unit' => $Kd_Unit,
                                    'Tahun' => $date,
                        ]);
                        $updatePaguUnit->pagu = $paguUnit;
                        $updatePaguUnit->update();
                    }
                }
            }
        }
        $model = TaPaguSubUnit::find()
                ->where(['Tahun' => $date])
                ->all();
        return $this->render('list', [
                    'model' => $model,
        ]);
    }

    public function getKota() {
        return Yii::$app->pengaturan->Kolom('Nm_Pemda');
    }

    public function actionCetak() 
    {
        $model = TaPaguSubUnit::find()->all();

        $paguUnit = (new \yii\db\Query())->from('Ta_Pagu_Sub_Unit');
        $paguIndikatif = $paguUnit->sum('pagu');


        $paguPakai = (new \yii\db\Query())->from('Ta_Belanja_Rinc_Sub');
        $pemakaian = $paguPakai->sum('Total');


        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'format' => Pdf::FORMAT_FOLIO,
            'content' => $this->renderPartial('cetak', [
               'model' => $model,
               'paguIndikatif' => $paguIndikatif,
               'pemakaian' =>$pemakaian,
            ]),
            'options' => [
                'title' => 'Pagu Sub Unit',
            //'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'methods' => [
                'SetHeader' => ['Dicetak dari: Sistem E-Planning '.$this->getKota().'||Dicetak tanggal: ' . 
                    Yii::$app->zultanggal->ZULgethari(date('N')) .', '.(date('j')).' '.
                    Yii::$app->zultanggal->ZULgetbulan(date('n')) .' '.(date('Y')).'/'.
            (date('H:i:s'))
                    ],
                'SetFooter' => ['Bappeda Kabupaten Paluta|Halaman {PAGENO}|Tvic10'],
            ]
        ]);
        return $pdf->render();
    } 


  public function actionPeraturan(){


    $peraturan = new \emusrenbang\models\TaPeraturanPagu();
    $No_ID = Yii::$app->getRequest()->getQueryParam('No_ID');

    $kunciPeraturan = TaPeraturanPagu::find()
      ->where(['No_ID'=> $No_ID])
      ->one();


    $kunciMedia = TaPaguMedia::find()
      ->where(['Id_Peraturan'=>$No_ID])
      ->exists();
    
    // $model = TaPeraturanPagu::find()
    // ->max('No_ID');
    // $modelId= $model+1;
    // $userInput = Yii::$app->user->identity->id;

    $searchModel = new TaPaguMediaSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($peraturan->load(Yii::$app->request->post())) {
            $peraturan->Tahun = date('Y');
            $peraturan->Tgl_Peraturan = date("Y-m-d");
            if($peraturan->save(false)){

              return $this->redirect(['ta-pagu-sub-unit/list','No_ID'=> $No_ID]);
            }       
         }   

      return $this->render('peraturan', [
          'No_ID' => $No_ID,
          'peraturan' => $peraturan,
          'dataProvider' => $dataProvider,
          'searchModel' => $searchModel,
          'kunciMedia' => $kunciMedia,
          'kunciPeraturan' => $kunciPeraturan,
    
        ]);
  }


  public function actionUploadPeraturan(){

  // $model = TaPeraturanPagu::find()
  //  ->max('No_ID');
  // $modelId= $model+1;

  $No_ID = Yii::$app->getRequest()->getQueryParam('No_ID');

  $userInput = Yii::$app->user->identity->id;

  $upload = new \emusrenbang\models\UploadPeraturan();

      if ($upload->load(Yii::$app->request->post())) {   
          $upload->pdfFile = UploadedFile::getInstances($upload, 'pdfFile');
          $upload->imageFile = UploadedFile::getInstances($upload, 'imageFile');


          if ($upload->upload()) {
              $id = 0;
              foreach ($upload->pdfFile as $file) {
              $user = RefMedia::findOne(['Nm_Media' => $upload->namePdf[$id]]);
              if ($user == null)
                  continue;
              $kd_media = $user->Kd_Media;
              $modelMedia = new TaPaguMedia();
              $modelMedia->Kd_Media = $kd_media;
              $modelMedia->Jenis_Dokumen = "Peraturan";
              // $modelMedia->Id_Peraturan = $modelId;
              $modelMedia->Id_Peraturan = $No_ID;
              $modelMedia->Tahun = date('Y');
              $modelMedia->Kd_User = $userInput;
              if($modelMedia->save(false)){

                return $this->redirect(['ta-pagu-sub-unit/peraturan', 'No_ID'=> $No_ID]);
              }

              $id++;
              } 
            }
                  
          }

     return $this->renderAjax('tes-upload', [
        'upload' => $upload,
  
      ]);
      }
}
