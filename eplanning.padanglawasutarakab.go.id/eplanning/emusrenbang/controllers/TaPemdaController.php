<?php

namespace emusrenbang\controllers;

use Yii;
use common\models\TaPemda;
use common\models\search\TaPemdaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use emusrenbang\models\Savelog;

/**
 * TaPemdaController implements the CRUD actions for TaPemda model.
 */
class TaPemdaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaPemda models.
     * @return mixed
     */
    public function actionIndex()
    {
        $log = new Savelog();
        $log->save('Akses menu Pemda berhasil', 'Menu Pemda', '', ''); //pesan, kegiatan, tabel, id dari tabel
        
        $searchModel = new TaPemdaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaPemda model.
     * @param string $id
     * @return mixed
     */
    public function actionView($Tahun, $Kd_Prov, $Kd_Kab)
    {
        $log = new Savelog();
        $log->save('Akses view Pemda berhasil', 'View Pemda', '', ''); //pesan, kegiatan, tabel, id dari tabel
        
        return $this->render('view', [
            'model' => $this->findModel($Tahun, $Kd_Prov, $Kd_Kab),
        ]);
    }

    /**
     * Creates a new TaPemda model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $log = new Savelog();
        $log->save('Akses Crate Pemda berhasil', 'Create Pemda', '', ''); //pesan, kegiatan, tabel, id dari tabel
        
        $model = new TaPemda();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Tahun]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaPemda model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $log = new Savelog();
        $log->save('Akses update Pemda berhasil', 'Update Pemda', '', ''); //pesan, kegiatan, tabel, id dari tabel
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Tahun]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaPemda model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $log = new Savelog();
        $log->save('Akses delete Pemda berhasil', 'Delete Pemda', '', ''); //pesan, kegiatan, tabel, id dari tabel
        
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaPemda model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TaPemda the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaPemda::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
