<?php

namespace emusrenbang\controllers;
use Yii;
use yii\helpers\ArrayHelper;

use common\models\RefSubUnit;
use common\models\TaPaguSubUnit;
use common\models\TaProgram;
use common\models\TaKegiatan;
use emusrenbang\models\Savelog;
use emusrenbang\models\TaPaguKegiatan;
use emusrenbang\models\TaBelanja;
use emusrenbang\models\TaBelanjaRinc;
use emusrenbang\models\TaBelanjaRincSub;
use emusrenbang\models\TaAksesDokumen;
use common\models\TaPaguSubUnitRiwayat;

use common\models\TaAksesIzin;

class AksesController extends \yii\web\Controller
{
  public function actionIndex()
  {
    $log = new Savelog();
    $log->save('Buka Akses SKPD Berhasil', 'Buka Akses SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
    
  	$skpd = RefSubUnit::find()->where(["!=", "Nm_Sub_Unit", ""])->orderBy('Nm_Sub_Unit')->all();
  	// echo $skpd->paguSkpd->pagu;
  	// die();
   	return $this->render('index',[
   		'skpd' => $skpd,
   	]);
  }

  public function actionDetail($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)
  {
    $log = new Savelog();
    $log->save('Lihat Detail SKPD Berhasil', 'Lihat Detail SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel

    $posisi = ['Kd_Urusan'=>$Kd_Urusan,
                      'Kd_Bidang'=>$Kd_Bidang,
                      'Kd_Unit'=>$Kd_Unit,
                      'Kd_Sub'=>$Kd_Sub];

    $TaAksesDokumen = TaAksesDokumen::find()
                      ->where($posisi)
                      ->andWhere(['Status' => '1']);
    if($TaAksesDokumen->one()){
      $status_terbuka = true;
    }
    else{
      $status_terbuka = false;
    }

    $RiwayatAkses_q = TaAksesDokumen::find()
                      ->where($posisi)
                      ->andWhere(['Status' => '0'])
                      ->orderBy(['Kd_Akses' => SORT_DESC]);

    date_default_timezone_set('Asia/Jakarta');
    if ($riwayatAkses = $RiwayatAkses_q->one()) {
      $riwayat['username'] = $riwayatAkses->user->username;
      $riwayat['nama_lengkap'] = $riwayatAkses->user->nama_lengkap;
      $riwayat['waktu_buka'] = date('d-m-Y H:m:s', $riwayatAkses->Waktu_Buka);
      $riwayat['waktu_tutup'] = date('d-m-Y H:m:s', $riwayatAkses->Waktu_Tutup);
      // $riwayat_user = $riwayat->Kd_User;
    }
    else{
      $riwayat['username'] = '';
      $riwayat['nama_lengkap'] = '';
      $riwayat['waktu_buka'] = '';
      $riwayat['waktu_tutup'] = '';
    }

  	$skpd = RefSubUnit::find()
  						->where($posisi)
  						->one();

    $model_pagu = TaPaguSubUnit::find()
              ->where($posisi)
              ->one();

    $TaProgramArr  = ArrayHelper::map(TaProgram::find()->where($posisi)->all(),
                  function($model){
                    return $model->Tahun."|".$model->Kd_Urusan."|".$model->Kd_Bidang."|".$model->Kd_Unit."|".$model->Kd_Sub."|".$model->Kd_Prog ;
                  }, 
                  function($model){
                    return $model->Kd_Urusan.".".$model->Kd_Bidang.".".$model->Kd_Unit.".".$model->Kd_Sub.".".$model->Kd_Prog." ".$model->Ket_Prog;
                  }
                );

    $TaProgram  = TaProgram::find()->where($posisi)->all();

   	return $this->render('detail',[
      'Tahun' => $Tahun,
      'Kd_Urusan' => $Kd_Urusan,
      'Kd_Bidang' => $Kd_Bidang,
      'Kd_Unit' => $Kd_Unit,
      'Kd_Sub' => $Kd_Sub,
      'skpd' => $skpd,
      'model_pagu' => $model_pagu,
      'TaProgramArr' => $TaProgramArr,
   		'TaProgram' => $TaProgram,
      'status_terbuka' => $status_terbuka,
      'riwayat' => $riwayat
   	]);
  }

  public function actionPaguProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)
  {
    $log = new Savelog();
    $log->save('Ubah Pagu Sub Unit Berhasil', 'Ubah Pagu Sub Unit', '', ''); //pesan, kegiatan, tabel, id dari tabel
    
    $posisi = [
      'Tahun' => $Tahun, 
      'Kd_Urusan' => $Kd_Urusan, 
      'Kd_Bidang' => $Kd_Bidang,
      'Kd_Unit' => $Kd_Unit,
      'Kd_Sub' => $Kd_Sub,
    ];

    $model_pagu = TaPaguSubUnit::find()
            ->where($posisi)
            ->one();

    if($model_pagu->load(Yii::$app->request->post()) && $model_pagu->save(false)){
      $model_riwayat = new TaPaguSubUnitRiwayat();
      $model_riwayat->attributes = $model_pagu->attributes;
      $model_riwayat->Tanggal_Riwayat=date('Y-m-d');
      $model_riwayat->Keterangan_Riwayat="Ubah Pagu";
      // $model_riwayat->Id_Peraturan = $modelPeraturan;
      $model_riwayat->Waktu_Riwayat = time();
      $model_riwayat->Kd_User = Yii::$app->user->identity->id;
      
      if($model_riwayat->save(false)){
        return $this->redirect(['detail', 
                              'Tahun' => $Tahun, 
                              'Kd_Urusan' => $Kd_Urusan, 
                              'Kd_Bidang' => $Kd_Bidang,
                              'Kd_Unit' => $Kd_Unit,
                              'Kd_Sub' => $Kd_Sub,
                            ]);
      }
    }
  }

  public function actionGetKegiatanIzin($key)
  {
    $log = new Savelog();
    $log->save('Mengambil Data Kegiatan Berhasil', 'Mengambil Data Kegiatan', '', ''); //pesan, kegiatan, tabel, id dari tabel

    $var = explode("|", $key);
    $Tahun = $var[0];
    $Kd_Urusan = $var[1];
    $Kd_Bidang = $var[2];
    $Kd_Unit = $var[3];
    $Kd_Sub = $var[4];
    $Kd_Prog = $var[5];

    $option = [
                'Tahun' => $Tahun,
                'Kd_Urusan' => $Kd_Urusan,
                'Kd_Bidang' => $Kd_Bidang,
                'Kd_Unit' => $Kd_Unit,
                'Kd_Sub' => $Kd_Sub,
                'Kd_Prog' => $Kd_Prog,
              ];

    $data = TaKegiatan::find()->where($option)->all();
    return $this->renderAjax('get_kegiatan_izin',[
      'data' => $data
    ]);
  }

  public function actionUbahIzin($Nm_Izin, $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg = 0, $Kd_Rek_1 = 0, $Kd_Rek_2 = 0, $Kd_Rek_3 = 0, $Kd_Rek_4 = 0, $Kd_Rek_5 = 0 , $No_Rinc = 0 , $No_ID = 0 )
  {
    $log = new Savelog();
    $log->save('Ubah Izin '.$Nm_Izin.'Berhasil', 'Ubah Izin', '', ''); //pesan, kegiatan, tabel, id dari tabel

    $query = TaAksesIzin::find();

    $query->andFilterWhere([
      'Tahun' => $Tahun, 
      'Kd_Urusan' => $Kd_Urusan, 
      'Kd_Bidang' => $Kd_Bidang,
      'Kd_Unit' => $Kd_Unit,
      'Kd_Sub' => $Kd_Sub,
      'Kd_Prog' => $Kd_Prog,
      'Nm_Izin' => $Nm_Izin,
    ]);

    if ($Kd_Keg != 0) {
      $query->andFilterWhere(['Kd_Keg' => $Kd_Keg]);
    }

    if ($Kd_Rek_1 != 0) {
      $query->andFilterWhere(['Kd_Rek_1' => $Kd_Rek_1]);
    }

    if ($Kd_Rek_2 != 0) {
      $query->andFilterWhere(['Kd_Rek_2' => $Kd_Rek_2]);
    }

    if ($Kd_Rek_3 != 0) {
      $query->andFilterWhere(['Kd_Rek_3' => $Kd_Rek_3]);
    }

    if ($Kd_Rek_4 != 0) {
      $query->andFilterWhere(['Kd_Rek_4' => $Kd_Rek_4]);
    }

    if ($Kd_Rek_5 != 0) {
      $query->andFilterWhere(['Kd_Rek_5' => $Kd_Rek_5]);
    }

    if ($No_Rinc != 0) {
      $query->andFilterWhere(['No_Rinc' => $No_Rinc]);
    }

    if ($No_ID != 0) {
      $query->andFilterWhere(['No_ID' => $No_ID]);
    }

    if ($model = $query->one()) {
      if ($model->Izin == '1') {
        $model->Izin = '0';
      }
      else{
        $model->Izin = '1';
      }
      
      $model->save(false);

      $izin = $model->Izin;
    }
    else {
      $model_izin = new TaAksesIzin;
      $model_izin->Tahun = $Tahun;
      $model_izin->Kd_Urusan = $Kd_Urusan;
      $model_izin->Kd_Bidang = $Kd_Bidang;
      $model_izin->Kd_Unit = $Kd_Unit;
      $model_izin->Kd_Sub = $Kd_Sub;
      $model_izin->Kd_Prog = $Kd_Prog;
      $model_izin->Kd_Keg = $Kd_Keg;
      $model_izin->Kd_Rek_1 = $Kd_Rek_1;
      $model_izin->Kd_Rek_2 = $Kd_Rek_2;
      $model_izin->Kd_Rek_3 = $Kd_Rek_3;
      $model_izin->Kd_Rek_4 = $Kd_Rek_4;
      $model_izin->Kd_Rek_5 = $Kd_Rek_5;
      $model_izin->No_Rinc = $No_Rinc;
      $model_izin->No_ID = $No_ID;
      $model_izin->Izin = '0';
      $model_izin->Nm_Izin = $Nm_Izin;
      $model_izin->save(false);
      // echo "kosong";
      $izin = $model_izin->Izin;

    }

    return $izin;

  }

  public function actionGetKegiatanPagu($key)
  {
    $log = new Savelog();
    $log->save('Melihat Data Pagu Kegiatan Berhasil', 'Melihat Data Pagu Kegiatan', '', ''); //pesan, kegiatan, tabel, id dari tabel

    $var = explode("|", $key);
    $Tahun = $var[0];
    $Kd_Urusan = $var[1];
    $Kd_Bidang = $var[2];
    $Kd_Unit = $var[3];
    $Kd_Sub = $var[4];
    $Kd_Prog = $var[5];

    $option = [
                'Tahun' => $Tahun,
                'Kd_Urusan' => $Kd_Urusan,
                'Kd_Bidang' => $Kd_Bidang,
                'Kd_Unit' => $Kd_Unit,
                'Kd_Sub' => $Kd_Sub,
                'Kd_Prog' => $Kd_Prog,
              ];

    $data = TaKegiatan::find()->where($option)->all();
    return $this->renderAjax('get_kegiatan_pagu',[
      'data' => $data
    ]);
  }

  public function actionUbahPagu($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg) 
  {
    $log = new Savelog();
    $log->save('Membuka Pagu Form SKPD', 'Mengubah Pagu SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel

      // $model = new TaPaguKegiatan;

      $query = TaPaguKegiatan::find();

      $query->andFilterWhere([
        'Tahun' => $Tahun, 
        'Kd_Urusan' => $Kd_Urusan, 
        'Kd_Bidang' => $Kd_Bidang,
        'Kd_Unit' => $Kd_Unit,
        'Kd_Sub' => $Kd_Sub,
        'Kd_Prog' => $Kd_Prog,
        'Kd_Keg' => $Kd_Keg,
      ]);

      if ($model = $query->one()) {
        $status = 1;
      }
      else{
        $model = new TaPaguKegiatan;
        $status = 0;
      }
      
      if (Yii::$app->request->isAjax) {
          return $this->renderAjax('form_pagu', [
              'model' => $model,
              'Tahun' => $Tahun, 
              'Kd_Urusan' => $Kd_Urusan, 
              'Kd_Bidang' => $Kd_Bidang,
              'Kd_Unit' => $Kd_Unit,
              'Kd_Sub' => $Kd_Sub,
              'Kd_Prog' => $Kd_Prog,
              'Kd_Keg' => $Kd_Keg,
          ]);
      } 
      else {
          return $this->render('form_pagu', [
              'model' => $model,
              'Tahun' => $Tahun, 
              'Kd_Urusan' => $Kd_Urusan, 
              'Kd_Bidang' => $Kd_Bidang,
              'Kd_Unit' => $Kd_Unit,
              'Kd_Sub' => $Kd_Sub,
              'Kd_Prog' => $Kd_Prog,
              'Kd_Keg' => $Kd_Keg,
          ]);
      }
  }

  public function actionUbahPaguProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg) 
  {
    $log = new Savelog();
    $log->save('Mengubah Pagu SKPD Berhasil', 'Mengubah Pagu SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel

    $request = Yii::$app->request;

    $query = TaPaguKegiatan::find();

    $query->andFilterWhere([
      'Tahun' => $Tahun, 
      'Kd_Urusan' => $Kd_Urusan, 
      'Kd_Bidang' => $Kd_Bidang,
      'Kd_Unit' => $Kd_Unit,
      'Kd_Sub' => $Kd_Sub,
      'Kd_Prog' => $Kd_Prog,
      'Kd_Keg' => $Kd_Keg,
    ]);

    if ($model = $query->one()) {
      $status = 1;
    }
    else{
      $model = new TaPaguKegiatan;
      $status = 0;
    }

    if ($request->isAjax) {
      if ($model->load($request->post()) && $model->save()){
        return "Ubah Berhasil";
      }
    }
    else{
      if ($model->load($request->post()) && $model->save()) {
        return $this->redirect([
                          'detail',
                          'Tahun' => $Tahun, 
                          'Kd_Urusan' => $Kd_Urusan, 
                          'Kd_Bidang' => $Kd_Bidang,
                          'Kd_Unit' => $Kd_Unit,
                          'Kd_Sub' => $Kd_Sub,
                          'Kd_Prog' => $Kd_Prog,
                          'Kd_Keg' => $Kd_Keg,
                      ]);
      }
    } 
  }

  public function actionGetKegiatanArr($key)
  {
    $var = explode("|", $key);
    $Tahun = $var[0];
    $Kd_Urusan = $var[1];
    $Kd_Bidang = $var[2];
    $Kd_Unit = $var[3];
    $Kd_Sub = $var[4];
    $Kd_Prog = $var[5];

    $option = [
                'Tahun' => $Tahun,
                'Kd_Urusan' => $Kd_Urusan,
                'Kd_Bidang' => $Kd_Bidang,
                'Kd_Unit' => $Kd_Unit,
                'Kd_Sub' => $Kd_Sub,
                'Kd_Prog' => $Kd_Prog,
              ];

    echo "<option>-Pilih Kegiatan-</option>";
    $data = TaKegiatan::find()->where($option)->all();
    foreach ($data as $key => $value) {
      $key = $value->Tahun."|".$value->Kd_Urusan."|".$value->Kd_Bidang."|".$value->Kd_Unit."|".$value->Kd_Sub."|".$value->Kd_Prog."|".$value->Kd_Keg ;
      echo '<option value='.$key.'>'.$value->Ket_Kegiatan.'</option>';
    }

  }

  public function actionGetBelanjaIzin($key)
  {
    $log = new Savelog();
    $log->save('Melihat Data Belanja Berhasil', 'Melihat Data Belanja', '', ''); //pesan, kegiatan, tabel, id dari tabel

    $var = explode("|", $key);
    $Tahun = $var[0];
    $Kd_Urusan = $var[1];
    $Kd_Bidang = $var[2];
    $Kd_Unit = $var[3];
    $Kd_Sub = $var[4];
    $Kd_Prog = $var[5];
    $Kd_Keg = $var[6];

    $option = [
                'Tahun' => $Tahun,
                'Kd_Urusan' => $Kd_Urusan,
                'Kd_Bidang' => $Kd_Bidang,
                'Kd_Unit' => $Kd_Unit,
                'Kd_Sub' => $Kd_Sub,
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg,
              ];

    $data = TaBelanja::find()->where($option)->all();
    return $this->renderAjax('get_belanja_izin',[
      'data' => $data
    ]);
  }

  public function actionGetBelanjaArr($key)
  {
    $var = explode("|", $key);
    $Tahun = $var[0];
    $Kd_Urusan = $var[1];
    $Kd_Bidang = $var[2];
    $Kd_Unit = $var[3];
    $Kd_Sub = $var[4];
    $Kd_Prog = $var[5];
    $Kd_Keg = $var[6];

    $option = [
                'Tahun' => $Tahun,
                'Kd_Urusan' => $Kd_Urusan,
                'Kd_Bidang' => $Kd_Bidang,
                'Kd_Unit' => $Kd_Unit,
                'Kd_Sub' => $Kd_Sub,
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg,
              ];

    echo "<option>-Pilih Belanja-</option>";
    $data = TaBelanja::find()->where($option)->all();
    foreach ($data as $key => $value) {
      $key = $value->Tahun."|".$value->Kd_Urusan."|".$value->Kd_Bidang."|".$value->Kd_Unit."|".$value->Kd_Sub."|".$value->Kd_Prog."|".$value->Kd_Keg."|".$value->Kd_Rek_1."|".$value->Kd_Rek_2."|".$value->Kd_Rek_3."|".$value->Kd_Rek_4."|".$value->Kd_Rek_5 ;
      echo '<option value='.$key.'>'.$value->refRek5->Nm_Rek_5.'</option>';
    }

  }

  public function actionGetBelanjaRincIzin($key)
  {
    $var = explode("|", $key);
    $Tahun = $var[0];
    $Kd_Urusan = $var[1];
    $Kd_Bidang = $var[2];
    $Kd_Unit = $var[3];
    $Kd_Sub = $var[4];
    $Kd_Prog = $var[5];
    $Kd_Keg = $var[6];
    $Kd_Rek_1 = $var[7];
    $Kd_Rek_2 = $var[8];
    $Kd_Rek_3 = $var[9];
    $Kd_Rek_4 = $var[10];
    $Kd_Rek_5 = $var[11];

    $option = [
                'Tahun' => $Tahun,
                'Kd_Urusan' => $Kd_Urusan,
                'Kd_Bidang' => $Kd_Bidang,
                'Kd_Unit' => $Kd_Unit,
                'Kd_Sub' => $Kd_Sub,
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg,
                'Kd_Rek_1' => $Kd_Rek_1,
                'Kd_Rek_2' => $Kd_Rek_2,
                'Kd_Rek_3' => $Kd_Rek_3,
                'Kd_Rek_4' => $Kd_Rek_4,
                'Kd_Rek_5' => $Kd_Rek_5,
              ];

    $data = TaBelanjaRinc::find()->where($option)->all();
    return $this->renderAjax('get_belanja_rinc_izin',[
      'data' => $data
    ]);
  }

  public function actionGetBelanjaRincArr($key)
  {
    $var = explode("|", $key);
    $Tahun = $var[0];
    $Kd_Urusan = $var[1];
    $Kd_Bidang = $var[2];
    $Kd_Unit = $var[3];
    $Kd_Sub = $var[4];
    $Kd_Prog = $var[5];
    $Kd_Keg = $var[6];
    $Kd_Rek_1 = $var[7];
    $Kd_Rek_2 = $var[8];
    $Kd_Rek_3 = $var[9];
    $Kd_Rek_4 = $var[10];
    $Kd_Rek_5 = $var[11];

    $option = [
                'Tahun' => $Tahun,
                'Kd_Urusan' => $Kd_Urusan,
                'Kd_Bidang' => $Kd_Bidang,
                'Kd_Unit' => $Kd_Unit,
                'Kd_Sub' => $Kd_Sub,
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg,
                'Kd_Rek_1' => $Kd_Rek_1,
                'Kd_Rek_2' => $Kd_Rek_2,
                'Kd_Rek_3' => $Kd_Rek_3,
                'Kd_Rek_4' => $Kd_Rek_4,
                'Kd_Rek_5' => $Kd_Rek_5,
              ];

    echo "<option>-Pilih Belanja Rinc-</option>";
    $data = TaBelanjaRinc::find()->where($option)->all();
    foreach ($data as $key => $value) {
      $key = $value->Tahun."|".$value->Kd_Urusan."|".$value->Kd_Bidang."|".$value->Kd_Unit."|".$value->Kd_Sub."|".$value->Kd_Prog."|".$value->Kd_Keg."|".$value->Kd_Rek_1."|".$value->Kd_Rek_2."|".$value->Kd_Rek_3."|".$value->Kd_Rek_4."|".$value->Kd_Rek_5."|".$value->No_Rinc ;
      echo '<option value='.$key.'>'.$value->Keterangan.'</option>';
    }

  }

  public function actionGetBelanjaRincSubIzin($key)
  {

    $log = new Savelog();
    $log->save('Melihat Data Belanja Rinci Berhasil', 'Melihat Data Belanja Rinci', '', ''); //pesan, kegiatan, tabel, id dari tabel

    $var = explode("|", $key);
    $Tahun = $var[0];
    $Kd_Urusan = $var[1];
    $Kd_Bidang = $var[2];
    $Kd_Unit = $var[3];
    $Kd_Sub = $var[4];
    $Kd_Prog = $var[5];
    $Kd_Keg = $var[6];
    $Kd_Rek_1 = $var[7];
    $Kd_Rek_2 = $var[8];
    $Kd_Rek_3 = $var[9];
    $Kd_Rek_4 = $var[10];
    $Kd_Rek_5 = $var[11];
    $No_Rinc = $var[12];

    $option = [
                'Tahun' => $Tahun,
                'Kd_Urusan' => $Kd_Urusan,
                'Kd_Bidang' => $Kd_Bidang,
                'Kd_Unit' => $Kd_Unit,
                'Kd_Sub' => $Kd_Sub,
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg,
                'Kd_Rek_1' => $Kd_Rek_1,
                'Kd_Rek_2' => $Kd_Rek_2,
                'Kd_Rek_3' => $Kd_Rek_3,
                'Kd_Rek_4' => $Kd_Rek_4,
                'Kd_Rek_5' => $Kd_Rek_5,
                'No_Rinc' => $No_Rinc,
              ];

    $data = TaBelanjaRincSub::find()->where($option)->all();
    return $this->renderAjax('get_belanja_rinc_sub_izin',[
      'data' => $data
    ]);
  }

  public function actionAksesDokumen($Tahun, $Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub)
  {

    $log = new Savelog();
    $log->save('Membuka Form Dokumen Akses Berhasil', 'Membuka Form Dokumen Akses', '', ''); //pesan, kegiatan, tabel, id dari tabel

    $model = new TaAksesDokumen;
    return $this->renderAjax('akses_dokumen',[
      'model' => $model,
      'Tahun' => $Tahun,
      'Kd_Urusan' => $Kd_Urusan,
      'Kd_Bidang' => $Kd_Bidang,
      'Kd_Unit' => $Kd_Unit,
      'Kd_Sub' => $Kd_Sub,
    ]);
  }

  public function actionAksesDokumenProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)
  {
    $model = new TaAksesDokumen;
    $model->Kd_Urusan = $Kd_Urusan;
    $model->Kd_Bidang = $Kd_Bidang;
    $model->Kd_Unit = $Kd_Unit;
    $model->Kd_Sub = $Kd_Sub;
    $model->Waktu_Buka = time();
    $model->Kd_User = Yii::$app->user->identity->id;
    $model->Status = '1';
    if ($model->save()) {
      $log = new Savelog();
      $log->save('Upload Dokumen Akses Berhasil', 'Upload Dokumen Akses', '', ''); //pesan, kegiatan, tabel, id dari tabel

      return $this->redirect(['detail', 
                              'Tahun' => $Tahun, 
                              'Kd_Urusan' => $Kd_Urusan, 
                              'Kd_Bidang' => $Kd_Bidang,
                              'Kd_Unit' => $Kd_Unit,
                              'Kd_Sub' => $Kd_Sub,
                            ]);
    }
  }

  public function actionAksesDokumenSelesai($Tahun, $Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub)
  {
    $log = new Savelog();
    $log->save('Menutup Pengaturan Akses SKPD Berhasil', 'Menutup Pengaturan Akses SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel

    $model = new TaAksesDokumen;
    return $this->renderAjax('akses_dokumen_selesai',[
      'model' => $model,
      'Tahun' => $Tahun,
      'Kd_Urusan' => $Kd_Urusan,
      'Kd_Bidang' => $Kd_Bidang,
      'Kd_Unit' => $Kd_Unit,
      'Kd_Sub' => $Kd_Sub,
    ]);
  }

  public function actionAksesDokumenSelesaiProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)
  {
    $model = TaAksesDokumen::find()->where([
              'Kd_Urusan' => $Kd_Urusan,
              'Kd_Bidang' => $Kd_Bidang,
              'Kd_Unit' => $Kd_Unit,
              'Kd_Sub' => $Kd_Sub,
              'Status' => '1',
            ])->one();
    $model->Waktu_Tutup = time();
    $model->Status = '0';

    $query_tutup = Yii::$app->db->createCommand("UPDATE Ta_Akses_Izin SET Izin='0' WHERE (Tahun=:Tahun AND Kd_Urusan=:Kd_Urusan AND Kd_Bidang=:Kd_Bidang AND Kd_Unit=:Kd_Unit AND Kd_Sub=:Kd_Sub) ")
    ->bindValue(':Tahun', $Tahun)
    ->bindValue(':Kd_Urusan', $Kd_Urusan)
    ->bindValue(':Kd_Bidang', $Kd_Bidang)
    ->bindValue(':Kd_Unit', $Kd_Unit)
    ->bindValue(':Kd_Sub', $Kd_Sub);

    if ($model->save(false) && $query_tutup->execute()) {
      $log = new Savelog();
      $log->save('Menutup Pengaturan Akses SKPD Berhasil', 'Menutup Pengaturan Akses SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel

      return $this->redirect(['detail', 
                              'Tahun' => $Tahun, 
                              'Kd_Urusan' => $Kd_Urusan, 
                              'Kd_Bidang' => $Kd_Bidang,
                              'Kd_Unit' => $Kd_Unit,
                              'Kd_Sub' => $Kd_Sub,
                            ]);
    }
  }

  public function actionTutupSemuaKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub)
  {
    Yii::$app->db->createCommand("UPDATE Ta_Akses_Izin SET Izin='0' WHERE (Tahun=:Tahun AND Kd_Urusan=:Kd_Urusan AND Kd_Bidang=:Kd_Bidang AND Kd_Unit=:Kd_Unit AND Kd_Sub=:Kd_Sub) ")
    ->bindValue(':Tahun', $Tahun)
    ->bindValue(':Kd_Urusan', $Kd_Urusan)
    ->bindValue(':Kd_Bidang', $Kd_Bidang)
    ->bindValue(':Kd_Unit', $Kd_Unit)
    ->bindValue(':Kd_Sub', $Kd_Sub)
    ->execute();

                                          
  }
}
