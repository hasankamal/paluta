<?php

namespace emusrenbang\controllers;
use Yii;
use common\models\TaPaguSubUnit;
use common\models\TaSubUnit;
use common\models\TaProgram;
use common\models\search\TaProgramSearch;
use common\models\TaPaguProgram;
use common\models\TaKegiatan;
use common\models\TaKegiatanSearch;
use common\models\RefProgram;
use common\models\RefKegiatan;
use common\models\RefIndikator;
use common\models\Satuan;
use common\models\RefRek1;
use common\models\RefRek2;
use common\models\RefRek3;
use common\models\RefRek4;
use common\models\RefRek5;
use common\models\RefApPub;
use common\models\RefSumberDana;
use common\models\RefStandardSatuan;
use emusrenbang\models\TaIndikator;
use emusrenbang\models\TaBelanja;
use emusrenbang\models\TaBelanjaRinc;
use emusrenbang\models\TaBelanjaRincSub;
use emusrenbang\models\TaPaguKegiatan;

//========riwayat=========//
use emusrenbang\models\TaKegiatanRiwayat;
use emusrenbang\models\TaBelanjaRiwayat;
use emusrenbang\models\TaBelanjaRincRiwayat;
use emusrenbang\models\TaBelanjaRincSubRiwayat;

use emusrenbang\models\Savelog;
use common\models\TaKegiatanSasaran;

use yii\helpers\ArrayHelper;


class PraRkaController extends \yii\web\Controller
{

    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionTambahKamus($Kd_Urusan, $Kd_Bidang, $Kd_Prog)
    {
        $Posisi_ref = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Prog' => $Kd_Prog,
        ];

        $model = new RefKegiatan();

        $max_kd=RefKegiatan::find()
                ->where($Posisi_ref)
                ->max('Kd_Keg');
        $Kd_Keg = $max_kd + 1;

        //$model2 = new TaPaguKegiatan();
        return $this->renderajax('tambah_kamus', [
                'model' => $model,
                'Kd_Keg' => $Kd_Keg, 
                'Kd_Urusan' => $Kd_Urusan, 
                'Kd_Bidang' => $Kd_Bidang,
                'Kd_Prog' => $Kd_Prog,
        ]);
    }

    public function actionTambahKamusProses()
    {
      $request = Yii::$app->request;
      $model = new RefKegiatan();
      if($model->load($request->post())){
        if($model->save(false)){
          echo "Tambah Kamus Berhasil";

          $log = new Savelog();
          $log->save('tambah kamus berhasil', 'tambah kamus', 'Ref_Kegiatan', ''); //pesan, kegiatan, tabel, id dari tabel
        }
      }

    }

    //====================//
    public function actionBelanjaRincSub($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
    {
        $log = new Savelog();
        $log->save('Akses Belanja Rincian Sub SKPD Berhasil', 'Belanja Rincian Sub SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $PosisiKegiatan = [
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            'No_Rinc' => $No_Rinc,
            ];
        $data = TaBelanjaRinc::findOne($PosisiKegiatan);

        $data_belanja=TaBelanjaRincSub::find()
                ->where($PosisiKegiatan)
                ->all();

        return $this->render('belanja_rinc_sub', [
                'data' => $data,
                'data_belanja' => $data_belanja,
                'PosisiKegiatan' => $PosisiKegiatan,
        ]);
    }

    public function actionTambahRincianObyek($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
    {

      $log = new Savelog();
      $log->save('Akses Tambah Rincian Obyek SKPD Berhasil', 'Tambah Rincian Obyek SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $PosisiKegiatan = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            'No_Rinc' => $No_Rinc,
            ];

        $model = new TaBelanjaRincSub();

        $max_id=TaBelanjaRincSub::find()
                ->where($PosisiKegiatan)
                ->max('No_ID');
        $No_ID = $max_id + 1;


        $Ref_Usulan_Rincian = 4;
        $Standard_Satuan=ArrayHelper::map(RefStandardSatuan::find()->orderby('Uraian')->all(),'Uraian','Uraian');

        return $this->renderajax('tambah_rincian_obyek', [
                'Tahun' => $Tahun, 
                'Kd_Urusan' => $Kd_Urusan, 
                'Kd_Bidang' => $Kd_Bidang, 
                'Kd_Unit' => $Kd_Unit, 
                'Kd_Sub' => $Kd_Sub, 
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg, 
                'Kd_Rek_1' => $Kd_Rek_1,
                'Kd_Rek_2' => $Kd_Rek_2,
                'Kd_Rek_3' => $Kd_Rek_3,
                'Kd_Rek_4' => $Kd_Rek_4,
                'Kd_Rek_5' => $Kd_Rek_5,
                'model' => $model,
                'No_Rinc' => $No_Rinc,
                'No_ID' => $No_ID,
                'Standard_Satuan' => $Standard_Satuan,
                'Ref_Usulan_Rincian' => $Ref_Usulan_Rincian,
        ]);
    }

    public function actionTambahRincianObyekProses()
    {
      $log = new Savelog();
      $log->save('Akses Proses Tambah Rincian Obyek SKPD Berhasil', 'Proses Tambah Rincian Obyek SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $request = Yii::$app->request;
      $model = new TaBelanjaRincSub();
      if($model->load($request->post())){
        //$model->Ket_Kegiatan;

        $PosisiKegiatan = [
              'Kd_Urusan' => $model->Kd_Urusan, 
              'Kd_Bidang' => $model->Kd_Bidang,
              'Kd_Unit' => $model->Kd_Unit,
              'Kd_Sub' => $model->Kd_Sub,
              'Kd_Prog' => $model->Kd_Prog,
              'Kd_Keg' => $model->Kd_Keg,
              'Kd_Rek_1' => $model->Kd_Rek_1,
              'Kd_Rek_2' => $model->Kd_Rek_2,
              'Kd_Rek_3' => $model->Kd_Rek_3,
              'Kd_Rek_4' => $model->Kd_Rek_4,
              'Kd_Rek_5' => $model->Kd_Rek_5,
              'No_Rinc' => $model->No_Rinc,
              ];
        $data = TaBelanjaRinc::findOne($PosisiKegiatan);

        $pagu_skpd = $data->taPaguSubUnit->pagu;
        $pemakaian_skpd = $data->getJlhBelanjaRincSubUnits()->sum('Total');
        $total_biaya = $model->Total;
        $total_pakai = $pemakaian_skpd + $total_biaya;

        if ($total_pakai > $pagu_skpd) {
          echo "Pagu Tidak Mencukupi";
        }
        else{
          $connection = \Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {
              $model_riwayat = new TaBelanjaRincSubRiwayat();
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
              $model_riwayat->ID_Prog = 0;
              $model_riwayat->Keterangan_Riwayat="Tambah";
             // $model_riwayat->Waktu_Riwayat = time();
              $model_riwayat->save(false);

              $model->save(false);
              
              echo "Tambah Rincian Obyek Kegiatan Berhasil";
              $transaction->commit();
          } catch (\Exception $e) {
              $transaction->rollBack();
              throw $e;
          } catch (\Throwable $e) {
              $transaction->rollBack();
              throw $e;
          }
        }
      }
    }

    public function actionHapusRincianObyek($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc, $No_ID)
    {

      $log = new Savelog();
      $log->save('Akses Hapus Rincian Obyek SKPD Berhasil', 'Hapus Rincian Obyek SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $Posisi = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            'No_Rinc' => $No_Rinc,
            'No_ID' => $No_ID,
            ];
      $model = TaBelanjaRincSub::findOne($Posisi);
      
      // if ($model->delete()) {
      //   return "Hapus Berhasil";
      // }

      $connection = \Yii::$app->db;
      $transaction = $connection->beginTransaction();
      try {
          $model_riwayat = new TaBelanjaRincSubRiwayat();
          $model_riwayat->attributes = $model->attributes;
          $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
          $model_riwayat->ID_Prog = 0;
          $model_riwayat->Keterangan_Riwayat="Hapus";
         // $model_riwayat->Waktu_Riwayat = time();
          $model_riwayat->save(false);


          $model->delete(false);
          
          echo "Hapus Berhasil";
          $transaction->commit();
      } catch (\Exception $e) {
          $transaction->rollBack();
          throw $e;
      } catch (\Throwable $e) {
          $transaction->rollBack();
          throw $e;
      }
    }

    public function actionUbahRincianObyek($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc, $No_ID)
    {
      $log = new Savelog();
      $log->save('Akses Ubah Rincian Obyek SKPD Berhasil', 'Ubah Rincian Obyek SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $Posisi = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            'No_Rinc' => $No_Rinc,
            'No_ID' => $No_ID,
            ];
      $model = TaBelanjaRincSub::findOne($Posisi);
      $No_ID = $model->No_ID;
      $Ref_Usulan_Rincian = $model->Ref_Usulan_Rincian;
      $Standard_Satuan=ArrayHelper::map(RefStandardSatuan::find()->orderby('Uraian')->all(),'Uraian','Uraian');

      return $this->renderajax('tambah_rincian_obyek', [
              'Tahun' => $Tahun, 
              'Kd_Urusan' => $Kd_Urusan, 
              'Kd_Bidang' => $Kd_Bidang, 
              'Kd_Unit' => $Kd_Unit, 
              'Kd_Sub' => $Kd_Sub, 
              'Kd_Prog' => $Kd_Prog,
              'Kd_Keg' => $Kd_Keg, 
              'Kd_Rek_1' => $Kd_Rek_1,
              'Kd_Rek_2' => $Kd_Rek_2,
              'Kd_Rek_3' => $Kd_Rek_3,
              'Kd_Rek_4' => $Kd_Rek_4,
              'Kd_Rek_5' => $Kd_Rek_5,
              'model' => $model,
              'No_Rinc' => $No_Rinc,
              'No_ID' => $No_ID,
              'Standard_Satuan' => $Standard_Satuan,
              'Ref_Usulan_Rincian' => $Ref_Usulan_Rincian,
              'ubah' => 1,
      ]);
    }

    public function actionUbahRincianObyekProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc, $No_ID)
    {
      $log = new Savelog();
      $log->save('Akses Proses Ubah Rincian Obyek SKPD Berhasil', 'Proses Ubah Rincian Obyek SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $Posisi = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            'No_Rinc' => $No_Rinc,
            'No_ID' => $No_ID,
            ];
      $model = TaBelanjaRincSub::findOne($Posisi);

      $total_pra = $model->Total;
      $request = Yii::$app->request;
      if($model->load($request->post())){
        $PosisiKegiatan = [
              'Kd_Urusan' => $model->Kd_Urusan, 
              'Kd_Bidang' => $model->Kd_Bidang,
              'Kd_Unit' => $model->Kd_Unit,
              'Kd_Sub' => $model->Kd_Sub,
              'Kd_Prog' => $model->Kd_Prog,
              'Kd_Keg' => $model->Kd_Keg,
              'Kd_Rek_1' => $model->Kd_Rek_1,
              'Kd_Rek_2' => $model->Kd_Rek_2,
              'Kd_Rek_3' => $model->Kd_Rek_3,
              'Kd_Rek_4' => $model->Kd_Rek_4,
              'Kd_Rek_5' => $model->Kd_Rek_5,
              'No_Rinc' => $model->No_Rinc,
              ];
        $data = TaBelanjaRinc::findOne($PosisiKegiatan);

        $pagu_skpd = $data->taPaguSubUnit->pagu;
        $pemakaian_skpd = $data->getJlhBelanjaRincSubUnits()->sum('Total')-$total_pra;
        $total_biaya = $model->Total;
        $total_pakai = $pemakaian_skpd + $total_biaya;

        if ($total_pakai > $pagu_skpd) {
          echo "Pagu Tidak Mencukupi";
        }
        else{


          $connection = \Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {
              $model_riwayat = new TaBelanjaRincSubRiwayat();
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
              $model_riwayat->ID_Prog = 0;
              $model_riwayat->Keterangan_Riwayat="Ubah";
             // $model_riwayat->Waktu_Riwayat = time();
              $model_riwayat->save(false);

              $model->save(false);
              
              echo "Ubah Rincian Obyek Kegiatan Berhasil";
              $transaction->commit();
          } catch (\Exception $e) {
              $transaction->rollBack();
              throw $e;
          } catch (\Throwable $e) {
              $transaction->rollBack();
              throw $e;
          }
        }
      }
    }


    //=====================//
    public function actionBelanjaRinc($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
    {

      $log = new Savelog();
      $log->save('Akses Belanja Rincian SKPD Berhasil', 'Belanja Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
        $PosisiKegiatan = [
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            ];
        $data = TaBelanja::findOne($PosisiKegiatan);

        $data_belanja = TaBelanjaRinc::find()->where($PosisiKegiatan)->all();
        $jlh_pagu_terpakai = TaBelanjaRincSub::find()
                            ->where($PosisiKegiatan)
                            ->sum('Total');
        $jlh_objek_rincian = TaBelanjaRincSub::find()
                            ->where($PosisiKegiatan)
                            ->count();

        return $this->render('belanja_rinc', [
                'data' => $data,
                'data_belanja' => $data_belanja,
                'jlh_pagu_terpakai' => $jlh_pagu_terpakai,
                'jlh_objek_rincian' => $jlh_objek_rincian,
                'PosisiKegiatan' => $PosisiKegiatan,
                //'data_belanja' => $data_belanja,
        ]);
    }

    public function actionTambahRincianSub($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
    {
      
      $log = new Savelog();
      $log->save('Akses Tambah Sub Rincian SKPD Berhasil', 'Tambah Sub Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
        $PosisiKegiatan = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            ];

        $model = new TaBelanjaRinc();

        $max_id=TaBelanjaRinc::find()
                ->where($PosisiKegiatan)
                ->max('No_Rinc');
        $No_Rinc = $max_id + 1;

        return $this->renderajax('tambah_rincian_sub', [
                'Tahun' => $Tahun, 
                'Kd_Urusan' => $Kd_Urusan, 
                'Kd_Bidang' => $Kd_Bidang, 
                'Kd_Unit' => $Kd_Unit, 
                'Kd_Sub' => $Kd_Sub, 
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg, 
                'Kd_Rek_1' => $Kd_Rek_1,
                'Kd_Rek_2' => $Kd_Rek_2,
                'Kd_Rek_3' => $Kd_Rek_3,
                'Kd_Rek_4' => $Kd_Rek_4,
                'Kd_Rek_5' => $Kd_Rek_5,
                'model' => $model,
                'No_Rinc' => $No_Rinc,
        ]);
    }

    public function actionTambahRincianSubProses()
    {

      $log = new Savelog();
      $log->save('Akses Proses Tambah Sub Rincian SKPD Berhasil', 'Proses Tambah Sub Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $request = Yii::$app->request;
      $model = new TaBelanjaRinc();
      if($model->load($request->post())){

          //$model->Ket_Kegiatan;
          $connection = \Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {
              if($model->save(false)){
              $model_riwayat = new TaBelanjaRincRiwayat();
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
              $model_riwayat->ID_Prog = 0;
              $model_riwayat->Keterangan_Riwayat="Tambah";
             // $model_riwayat->Waktu_Riwayat = time();
              $model_riwayat->save(false);
                                        
              echo "Masukkan Rincian Sub Kegiatan Berhasil";
              $transaction->commit();
            }
          } catch (\Exception $e) {
              $transaction->rollBack();
              throw $e;
          } catch (\Throwable $e) {
              $transaction->rollBack();
              throw $e;
          }
      }
    }

    public function actionHapusRincianSub($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
    {

      $log = new Savelog();
      $log->save('Akses Hapus Sub Rincian SKPD Berhasil', 'Hapus Sub Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $Posisi = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            'No_Rinc' => $No_Rinc,
            ];
      $model = TaBelanjaRinc::findOne($Posisi);
      
      // if ($model->delete()) {
      //   return "Hapus Berhasil";
      // }

      $connection = \Yii::$app->db;
      $transaction = $connection->beginTransaction();
      try {
          $model_riwayat = new TaBelanjaRincRiwayat();
          $model_riwayat->attributes = $model->attributes;
          $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
          $model_riwayat->ID_Prog = 0;
          $model_riwayat->Keterangan_Riwayat="Hapus";
         // $model_riwayat->Waktu_Riwayat = time();
          $model_riwayat->save(false);


          $model->delete(false);
          
          echo "Hapus Berhasil";
          $transaction->commit();
      } catch (\Exception $e) {
          $transaction->rollBack();
          throw $e;
      } catch (\Throwable $e) {
          $transaction->rollBack();
          throw $e;
      }
    }

    public function actionUbahRincianSub($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
    {

      $log = new Savelog();
      $log->save('Akses Ubah Sub Rincian SKPD Berhasil', 'Ubah Sub Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $Posisi = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            'No_Rinc' => $No_Rinc,
            ];
      $model = TaBelanjaRinc::findOne($Posisi);
      
      return $this->renderajax('tambah_rincian_sub', [
              'Tahun' => $Tahun, 
              'Kd_Urusan' => $Kd_Urusan, 
              'Kd_Bidang' => $Kd_Bidang, 
              'Kd_Unit' => $Kd_Unit, 
              'Kd_Sub' => $Kd_Sub, 
              'Kd_Prog' => $Kd_Prog,
              'Kd_Keg' => $Kd_Keg, 
              'Kd_Rek_1' => $Kd_Rek_1,
              'Kd_Rek_2' => $Kd_Rek_2,
              'Kd_Rek_3' => $Kd_Rek_3,
              'Kd_Rek_4' => $Kd_Rek_4,
              'Kd_Rek_5' => $Kd_Rek_5,
              'No_Rinc' => $No_Rinc,
              'model' => $model,
              'ubah' => 1,
      ]);
    }

    public function actionUbahRincianSubProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc)
    {
      $log = new Savelog();
      $log->save('Akses Proses Ubah Sub Rincian SKPD Berhasil', 'Proses Ubah Sub Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $Posisi = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            'No_Rinc' => $No_Rinc,
            ];
      $model = TaBelanjaRinc::findOne($Posisi);
      $request = Yii::$app->request;
      if($model->load($request->post())){
          //$model->Ket_Kegiatan;
          $connection = \Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {
              $model_riwayat = new TaBelanjaRincRiwayat();
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
              $model_riwayat->ID_Prog = 0;
              $model_riwayat->Keterangan_Riwayat="Ubah";
             // $model_riwayat->Waktu_Riwayat = time();
              $model_riwayat->save(false);

              
              $model_riwayat->save();
              $model->save(false);

              
              echo "Ubah Rincian Sub Kegiatan Berhasil";
              $transaction->commit();
          } catch (\Exception $e) {
              $transaction->rollBack();
              throw $e;
          } catch (\Throwable $e) {
              $transaction->rollBack();
              throw $e;
          }
      }
    }

    //====================//

    public function actionBelanja($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {
      $log = new Savelog();
      $log->save('Akses Menu Belanja SKPD Berhasil', 'Menu Belanja SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
        $PosisiKegiatan = [
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            ];
        $data = TaKegiatan::findOne($PosisiKegiatan);

        $data_belanja = TaBelanja::find()->where($PosisiKegiatan)->all();
        $jlh_pagu_terpakai = TaBelanjaRincSub::find()
                            ->where($PosisiKegiatan)
                            ->sum('Total');

        return $this->render('belanja', [
                'data' => $data,
                'data_belanja' => $data_belanja,
                'jlh_pagu_terpakai' => $jlh_pagu_terpakai,
                'PosisiKegiatan' => $PosisiKegiatan
        ]);
    }

    public function actionTambahRincian($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {
      $log = new Savelog();
      $log->save('Akses Tambah Rincian SKPD Berhasil', 'Tambah Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
        $PosisiKegiatan = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            ];

        $model = new TaBelanja();

        $Data_Rek_1 = RefRek1::findOne(['Kd_Rek_1'=>5]);
        $Data_Rek_2 = RefRek2::findOne(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2]);
        $Data_Rek_3 = ArrayHelper::map(RefRek3::find()->where(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2])->all(), 
                                        'Kd_Rek_3',
                                        function($model, $defaultValue) {
                                          return $model->Kd_Rek_3.". ".$model->Nm_Rek_3;
                                        }
        );  
        $Data_Rek_4 = [];
        $Data_Rek_5 = [];


        $Kd_Rek_1 = $Data_Rek_1->Kd_Rek_1;
        $Nm_Rek_1 = $Data_Rek_1->Nm_Rek_1;
        $Kd_Rek_2 = $Data_Rek_2->Kd_Rek_2;
        $Nm_Rek_2 = $Data_Rek_2->Nm_Rek_2;
        //$model->addRule(['Nm_Rek_1'], 'string', ['max' => 128]);


        $RefApPub = ArrayHelper::map(RefApPub::find()->all(), 'Kd_Ap_Pub', 'Nm_Ap_Pub');
        $RefSumberDana = ArrayHelper::map(RefSumberDana::find()->all(), 'Kd_Sumber', 'Nm_Sumber');

        return $this->renderajax('tambah_rincian', [
                'Tahun' => $Tahun, 
                'Kd_Urusan' => $Kd_Urusan, 
                'Kd_Bidang' => $Kd_Bidang, 
                'Kd_Unit' => $Kd_Unit, 
                'Kd_Sub' => $Kd_Sub, 
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg, 
                'model' => $model, 
                'Kd_Rek_1' => $Kd_Rek_1,
                'Nm_Rek_1' => $Nm_Rek_1, 
                'Kd_Rek_2' => $Kd_Rek_2,
                'Nm_Rek_2' => $Nm_Rek_2, 
                'Data_Rek_2' => $Data_Rek_2,
                'Data_Rek_3' => $Data_Rek_3,
                'Data_Rek_4' => $Data_Rek_4,
                'Data_Rek_5' => $Data_Rek_5,
                'RefApPub' => $RefApPub,
                'RefSumberDana' => $RefSumberDana,
        ]);
    }

    public function actionTambahRincianProses()
    {

      $log = new Savelog();
      $log->save('Akses Proses Tambah Rincian SKPD Berhasil', 'Proses Tambah Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $request = Yii::$app->request;
      $model = new TaBelanja();
      if($model->load($request->post())){
          //$model->Ket_Kegiatan;
          $connection = \Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {
              $model->save(false);
              $model_riwayat = new TaBelanjaRiwayat();
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
              $model_riwayat->ID_Prog = 0;
              $model_riwayat->Keterangan_Riwayat="Tambah";
             // $model_riwayat->Waktu_Riwayat = time();
              $model_riwayat->save(false);
              
              echo "Masukkan Rincian Kegiatan Berhasil";
              $transaction->commit();
          } catch (\Exception $e) {
              $transaction->rollBack();
              throw $e;
          } catch (\Throwable $e) {
              $transaction->rollBack();
              throw $e;
          }
      }
    }

    public function actionHapusRincian($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
    {
      $log = new Savelog();
      $log->save('Akses Hapus Rincian SKPD Berhasil', 'Hapus Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $Posisi = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5
            ];
      $model = TaBelanja::findOne($Posisi);
      
      // if ($model->delete()) {
      //   return "Hapus Berhasil";
      // }

      $connection = \Yii::$app->db;
      $transaction = $connection->beginTransaction();
      try {
          $model_riwayat = new TaBelanjaRiwayat();
          $model_riwayat->attributes = $model->attributes;
          $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
          $model_riwayat->ID_Prog = 0;
          $model_riwayat->Keterangan_Riwayat="Hapus";
         // $model_riwayat->Waktu_Riwayat = time();
          $model_riwayat->save(false);


          $model->delete(false);
          
          echo "Hapus Berhasil";
          $transaction->commit();
      } catch (\Exception $e) {
          $transaction->rollBack();
          throw $e;
      } catch (\Throwable $e) {
          $transaction->rollBack();
          throw $e;
      }

    }

    public function actionUbahRincian($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
    {

      $log = new Savelog();
      $log->save('Akses Ubah Rincian SKPD Berhasil', 'Ubah Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
        $Posisi = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5
            ];

        $model = TaBelanja::findOne($Posisi);

        $Data_Rek_1 = RefRek1::findOne(['Kd_Rek_1'=>5]);
        $Data_Rek_2 = RefRek2::findOne(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2]);
        $Data_Rek_3 = ArrayHelper::map(RefRek3::find()->where(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2])->all(), 'Kd_Rek_3','Nm_Rek_3');
        $Data_Rek_4 = ArrayHelper::map(RefRek4::find()->where(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2, 'Kd_Rek_3'=>$Kd_Rek_3])->all(), 'Kd_Rek_4','Nm_Rek_4');
        $Data_Rek_5 = ArrayHelper::map(RefRek5::find()->where(['Kd_Rek_1'=>5, 'Kd_Rek_2'=>2, 'Kd_Rek_3'=>$Kd_Rek_3, 'Kd_Rek_4'=>$Kd_Rek_4])->all(), 'Kd_Rek_5','Nm_Rek_5');


        $Kd_Rek_1 = $Data_Rek_1->Kd_Rek_1;
        $Nm_Rek_1 = $Data_Rek_1->Nm_Rek_1;
        $Kd_Rek_2 = $Data_Rek_2->Kd_Rek_2;
        $Nm_Rek_2 = $Data_Rek_2->Nm_Rek_2;
        //$model->addRule(['Nm_Rek_1'], 'string', ['max' => 128]);


        $RefApPub = ArrayHelper::map(RefApPub::find()->all(), 'Kd_Ap_Pub', 'Nm_Ap_Pub');
        $RefSumberDana = ArrayHelper::map(RefSumberDana::find()->all(), 'Kd_Sumber', 'Nm_Sumber');

        return $this->renderajax('tambah_rincian', [
                'Tahun' => $Tahun, 
                'Kd_Urusan' => $Kd_Urusan, 
                'Kd_Bidang' => $Kd_Bidang, 
                'Kd_Unit' => $Kd_Unit, 
                'Kd_Sub' => $Kd_Sub, 
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg, 
                'model' => $model, 
                'Kd_Rek_1' => $Kd_Rek_1,
                'Nm_Rek_1' => $Nm_Rek_1, 
                'Kd_Rek_2' => $Kd_Rek_2,
                'Nm_Rek_2' => $Nm_Rek_2, 
                'Data_Rek_2' => $Data_Rek_2,
                'Data_Rek_3' => $Data_Rek_3,
                'Data_Rek_4' => $Data_Rek_4,
                'Data_Rek_5' => $Data_Rek_5,
                'RefApPub' => $RefApPub,
                'RefSumberDana' => $RefSumberDana,
                'Kd_Rek_3' => $Kd_Rek_3,
                'Kd_Rek_4' => $Kd_Rek_4,
                'Kd_Rek_5' => $Kd_Rek_5,
                'ubah' => 1,
        ]);
    }

    public function actionUbahRincianProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5)
    {

      $log = new Savelog();
      $log->save('Akses Proses Ubah Rincian SKPD Berhasil', 'Proses Ubah Rincian SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $request = Yii::$app->request;
      $Posisi = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5
            ];

      $model = TaBelanja::findOne($Posisi);
      //$model = new TaBelanja();
      if($model->load($request->post())){
          //$model->Ket_Kegiatan;
          $connection = \Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {
              $model_riwayat = new TaBelanjaRiwayat();
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
              $model_riwayat->ID_Prog = 0;
              $model_riwayat->Keterangan_Riwayat="Ubah";
             // $model_riwayat->Waktu_Riwayat = time();
              $model_riwayat->save(false);


              $model->save(false);
              
              echo "Ubah Rincian Kegiatan Berhasil";
              $transaction->commit();
          } catch (\Exception $e) {
              $transaction->rollBack();
              throw $e;
          } catch (\Throwable $e) {
              $transaction->rollBack();
              throw $e;
          }
      }
    }

    //======================//

    public function actionKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog)
    {
        $log = new Savelog();
        $log->save('Akses Menu Kegiatan SKPD Berhasil', 'Kegiatan SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $PosisiKegiatan = [
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            ];

        $searchModel = new TaKegiatanSearch();
        $dataProvider = $searchModel->searchKegiatan($PosisiKegiatan);
        $modelUnit = TaProgram::findOne($PosisiKegiatan);

        $jlh_pagu_terpakai = TaBelanjaRincSub::find()
                            ->where($PosisiKegiatan)
                            ->sum('Total');

        return $this->render('kegiatan', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'data' => $modelUnit,
                    'jlh_pagu_terpakai' => $jlh_pagu_terpakai,
                    'PosisiKegiatan' => $PosisiKegiatan
        ]);
    }

    public function actionTambahKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog)
    {
        $log = new Savelog();
        $log->save('Akses Tambah Kegiatan SKPD Berhasil', 'Tambah Kegiatan SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $PosisiKegiatan = [
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
        ];

        $Posisi_ref = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Prog' => $Kd_Prog,
        ];

        $model = new TaKegiatan();

        $max_id=TaKegiatan::find()
                ->where($PosisiKegiatan)
                ->max('ID_Prog');
        $ID_Prog = $max_id + 1;

        $max_kd=TaKegiatan::find()
                ->where($PosisiKegiatan)
                ->max('Kd_Keg');
        $Kd_Keg = $max_kd + 1;

        //============//
        $indikator=RefIndikator::find()
                      ->where(['In', 'Kd_Indikator', [2,3,4,7]])
                      ->all();

        //============//
        //$satuan=satuan::find()->all();
        $satuan =  ArrayHelper::map(Satuan::find()->all(),'id','satuan');

        //============//
        $sumber_dana = ArrayHelper::map(RefSumberdana::find()->all(), 'Kd_Sumber', 'Nm_Sumber');

        //============//
        //$ref_kegiatan =  ArrayHelper::map(RefKegiatan::find()->where($Posisi_ref)->all(),'Ket_Kegiatan','Ket_Kegiatan');
        $ref_kegiatan = [];
        $dat_kegiatan = RefKegiatan::find()->where($Posisi_ref)->all();
        foreach ($dat_kegiatan as $key => $value) {
          $ref_kegiatan[$value->Kd_Keg."|".$value->Ket_Kegiatan] = $value->Ket_Kegiatan;
        }


        //$model2 = new TaPaguKegiatan();
        return $this->renderajax('tambah-kegiatan', [
                'model' => $model,
                'ID_Prog' => $ID_Prog, 
                'Kd_Keg' => $Kd_Keg, 
                'Tahun' => $Tahun, 
                'Kd_Urusan' => $Kd_Urusan, 
                'Kd_Bidang' => $Kd_Bidang, 
                'Kd_Unit' => $Kd_Unit, 
                'Kd_Sub' => $Kd_Sub, 
                'Kd_Prog' => $Kd_Prog,
                'indikator' => $indikator,
                'satuan' => $satuan,
                'sumber_dana' => $sumber_dana,
                'ref_kegiatan' => $ref_kegiatan,
                //'models' => $model2,
        ]);
    }

    public function actionTambahKegiatanProses()
    {

      $log = new Savelog();
      $log->save('Akses Proses Tambah Kegiatan SKPD Berhasil', 'Proses Tambah Kegiatan SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $request = Yii::$app->request;
      $model = new TaKegiatan();
      if($model->load($request->post())){
          //$model->Ket_Kegiatan;
          $connection = \Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {
              $waktu_pelaksanaan_nilai = $request->post('waktu_pelaksanaan_nilai');
              $waktu_pelaksanaan_satuan = $request->post('waktu_pelaksanaan_satuan');
              $model->Waktu_Pelaksanaan = $waktu_pelaksanaan_nilai." ".$waktu_pelaksanaan_satuan;
              $model->Status = 0;
              $dat_kegiatan = explode("|", $model->Ket_Kegiatan);
              $model->Kd_Keg = $dat_kegiatan[0];
              $model->Ket_Kegiatan = $dat_kegiatan[1];
              $model->save(false);

              //simpan ke riwayat
              $model_riwayat = new TaKegiatanRiwayat();
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
              $model_riwayat->ID_Prog = 0;
              $model_riwayat->Keterangan_Riwayat="Tambah";
             // $model_riwayat->Waktu_Riwayat = time();
              $model_riwayat->save(false);

              $model_pagu = new TaPaguKegiatan();
              $model_pagu->Tahun=date('Y');
              $model_pagu->Kd_Keg=$model->Kd_Keg;
              $model_pagu->Kd_Urusan=$model->Kd_Urusan;
              $model_pagu->Kd_Bidang=$model->Kd_Bidang;
              $model_pagu->Kd_Unit=$model->Kd_Unit;
              $model_pagu->Kd_Sub=$model->Kd_Sub;
              $model_pagu->Kd_Prog=$model->Kd_Prog;
              $model_pagu->pagu=$model->Pagu_Anggaran;

              $model_pagu->save(false);

              $tolak_ukur = $request->post('tolak_ukur');
              $target = $request->post('target');
              $target_satuan = $request->post('target_satuan');

              // $max_indi=TaIndikator::find()
              //           ->where([
              //               'Kd_Urusan' => $model->Kd_Urusan, 
              //               'Kd_Bidang' => $model->Kd_Bidang,
              //               'Kd_Unit' => $model->Kd_Unit,
              //               'Kd_Sub' => $model->Kd_Sub,
              //               'Kd_Prog' => $model->Kd_Prog,
              //               'Kd_Keg' => $model->Kd_Keg,
              //             ])
              //           ->max('Kd_Indikator');
              // $Kd_Indikator = $max_indi + 1;

              foreach ($tolak_ukur as $key => $toluk) {
                $model_indikator = new TaIndikator();

                $model_indikator->Tahun = $model->Tahun;
                $model_indikator->Kd_Urusan = $model->Kd_Urusan;
                $model_indikator->Kd_Bidang = $model->Kd_Bidang;
                $model_indikator->Kd_Unit = $model->Kd_Unit;
                $model_indikator->Kd_Sub = $model->Kd_Sub;
                $model_indikator->Kd_Prog = $model->Kd_Prog;
                $model_indikator->ID_Prog = $model->ID_Prog;
                $model_indikator->Kd_Keg = $model->Kd_Keg;
                $model_indikator->Kd_Indikator = $key;
                $model_indikator->No_ID = 0;
                $model_indikator->Tolak_Ukur= $toluk;
                $model_indikator->Target_Angka=str_replace('.', '', $target[$key]);
                $model_indikator->Target_Uraian=str_replace('.', '', $target_satuan[$key]);

                $model_indikator->save(false);
                //$Kd_Indikator++;
              }

              echo "Masukkan Kegiatan Berhasil";
              $transaction->commit();
          } catch (\Exception $e) {
              $transaction->rollBack();
              throw $e;
          } catch (\Throwable $e) {
              $transaction->rollBack();
              throw $e;
          }
      }
    }

    public function actionUbahKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {
        $log = new Savelog();
        $log->save('Ubah Kegiatan SKPD Berhasil', 'Ubah Kegiatan SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $PosisiKegiatan = [
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
        ];

        $Posisi_ref = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Prog' => $Kd_Prog,
        ];

        $model = TaKegiatan::findOne($PosisiKegiatan);

        // $max_id=TaKegiatan::find()
        //         ->where($PosisiKegiatan)
        //         ->max('ID_Prog');
        // $ID_Prog = $max_id + 1;

        // $max_kd=TaKegiatan::find()
        //         ->where($PosisiKegiatan)
        //         ->max('Kd_Keg');
        // $Kd_Keg = $max_kd + 1;

        //============//
        // $indikator=RefIndikator::find()
        //               ->where(['In', 'Kd_Indikator', [2,3,4,7]])
        //               ->all();

        $indikator_data=$model->getIndikator()->all();
        $indikator=RefIndikator::find()
                    ->where(['In', 'Kd_Indikator', [2,3,4,7]])
                    ->all();
        //============//
        //$satuan=satuan::find()->all();
        $satuan =  ArrayHelper::map(Satuan::find()->all(),'id','satuan');

        //============//
        $sumber_dana = ArrayHelper::map(RefSumberdana::find()->all(), 'Kd_Sumber', 'Nm_Sumber');

        //============//
        //$ref_kegiatan =  ArrayHelper::map(RefKegiatan::find()->where($Posisi_ref)->all(),'Ket_Kegiatan','Ket_Kegiatan');
        $ref_kegiatan = [];
        $dat_kegiatan = RefKegiatan::find()->where($Posisi_ref)->all();
        foreach ($dat_kegiatan as $key => $value) {
          $ref_kegiatan[$value->Kd_Keg."|".$value->Ket_Kegiatan] = $value->Ket_Kegiatan;
        }

        if (isset($model->Waktu_Pelaksanaan)) {
          $waktu = explode(" ", $model->Waktu_Pelaksanaan);
          $waktu_pelaksanaan_nilai = $waktu[0];
          $waktu_pelaksanaan_satuan = $waktu[1];
        }
        else{
          $waktu_pelaksanaan_nilai = '';
          $waktu_pelaksanaan_satuan = '';
        }
        $a = $model->Kd_Keg;
        $b = $model->Ket_Kegiatan;
        $model->Ket_Kegiatan = $a."|".$b;

        $model_indikator = TaIndikator::findall($PosisiKegiatan);
        foreach ($model_indikator as $key => $indi) {
          $Kd_Indikator = $indi->Kd_Indikator;

          $Tolak_Ukur[$Kd_Indikator]=$indi->Tolak_Ukur;
          $Target_Angka[$Kd_Indikator]=$indi->Target_Angka;
          $Target_Uraian[$Kd_Indikator]=$indi->Target_Uraian;
        }

        if (!isset($Tolak_Ukur)) {
          $Tolak_Ukur=[];
          $Target_Angka=[];
          $Target_Uraian=[];
        }

        

        //$model2 = new TaPaguKegiatan();
        return $this->renderajax('tambah-kegiatan', [
                'model' => $model,
                // 'ID_Prog' => $ID_Prog, 
                // 'Kd_Keg' => $Kd_Keg, 
                'Tahun' => $Tahun, 
                'Kd_Urusan' => $Kd_Urusan, 
                'Kd_Bidang' => $Kd_Bidang, 
                'Kd_Unit' => $Kd_Unit, 
                'Kd_Sub' => $Kd_Sub, 
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg,
                'indikator' => $indikator,
                'satuan' => $satuan,
                'sumber_dana' => $sumber_dana,
                'ref_kegiatan' => $ref_kegiatan,
                'waktu_pelaksanaan_nilai' => $waktu_pelaksanaan_nilai,
                'waktu_pelaksanaan_satuan' => $waktu_pelaksanaan_satuan,
                'ubah' => 1,
                'Tolak_Ukur' => $Tolak_Ukur,
                'Target_Angka' => $Target_Angka,
                'Target_Uraian' => $Target_Uraian,
                //'models' => $model2,
        ]);
    }

    public function actionUbahKegiatanProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {

      $log = new Savelog();
      $log->save('Akses Proses Ubah Kegiatan SKPD Berhasil', 'Proses Ubah Kegiatan SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $request = Yii::$app->request;
      $Posisi = [
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            ];

      $model = TaKegiatan::findone($Posisi);

      if($model->load($request->post())){
          $connection = \Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {
              $waktu_pelaksanaan_nilai = $request->post('waktu_pelaksanaan_nilai');
              $waktu_pelaksanaan_satuan = $request->post('waktu_pelaksanaan_satuan');
              $model->Waktu_Pelaksanaan = $waktu_pelaksanaan_nilai." ".$waktu_pelaksanaan_satuan;
              $model->Status = 0;
              $dat_kegiatan = explode("|", $model->Ket_Kegiatan);
              $model->Kd_Keg = $dat_kegiatan[0];
              $model->Ket_Kegiatan = $dat_kegiatan[1];
              $model->save(false);

              //simpan ke riwayat
              $model_riwayat = new TaKegiatanRiwayat();
              $model_riwayat->attributes = $model->attributes;
              $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
              $model_riwayat->ID_Prog = 0;
              $model_riwayat->Keterangan_Riwayat="Ubah";
             // $model_riwayat->Waktu_Riwayat = time();
              $model_riwayat->save(false);

              $model_pagu = TaPaguKegiatan::findOne($Posisi);
              $model_pagu->pagu=$model->Pagu_Anggaran;
              $model_pagu->save(false);

              $tolak_ukur = $request->post('tolak_ukur');
              $target = $request->post('target');
              $target_satuan = $request->post('target_satuan');

              
              TaIndikator::deleteAll($Posisi);
              foreach ($tolak_ukur as $key => $toluk) {
                $model_indikator = new TaIndikator();

                $model_indikator->Tahun = $model->Tahun;
                $model_indikator->Kd_Urusan = $model->Kd_Urusan;
                $model_indikator->Kd_Bidang = $model->Kd_Bidang;
                $model_indikator->Kd_Unit = $model->Kd_Unit;
                $model_indikator->Kd_Sub = $model->Kd_Sub;
                $model_indikator->Kd_Prog = $model->Kd_Prog;
                $model_indikator->ID_Prog = $model->ID_Prog;
                $model_indikator->Kd_Keg = $model->Kd_Keg;
                $model_indikator->Kd_Indikator = $key;
                $model_indikator->No_ID = 0;
                $model_indikator->Tolak_Ukur=$nilai = str_replace('.', '', $toluk);
                $model_indikator->Target_Angka=str_replace('.', '', $target[$key]);
                $model_indikator->Target_Uraian=str_replace('.', '', $target_satuan[$key]);

                $model_indikator->save(false);
                //$Kd_Indikator++;
              }
              echo 'Ubah Kegiatan Berhasil';
              $transaction->commit();
          } catch (\Exception $e) {
              $transaction->rollBack();
              throw $e;
          } catch (\Throwable $e) {
              $transaction->rollBack();
              throw $e;
          }
      }
    }

    public function actionViewKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {

      $log = new Savelog();
      $log->save('Akses View Kegiatan SKPD Berhasil', 'View Kegiatan SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
        return $this->render('view-kegiatan');
    }

    public function actionHapusKegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {
        $log = new Savelog();
        $log->save('Akses Hapus Kegiatan SKPD Berhasil', 'Hapus Kegiatan SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
      $Posisi = [
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            ];
      $model = TaKegiatan::findOne($Posisi);
      //$model2 = TaPaguKegiatan::findOne($Posisi);
      //$model3 = TaIndikator::findOne($Posisi);
      
      $connection = \Yii::$app->db;
      $transaction = $connection->beginTransaction();
      try {
          $model_riwayat = new TaKegiatanRiwayat();
          $model_riwayat->attributes = $model->attributes;
          $model_riwayat->Tanggal_Riwayat=date("Y-m-d H:i:s");
          $model_riwayat->ID_Prog = 0;
          $model_riwayat->Keterangan_Riwayat="Hapus";
         // $model_riwayat->Waktu_Riwayat = time();
          $model_riwayat->save(false);


          TaIndikator::deleteall($Posisi);
          $model->delete(false);
          
          echo "Hapus Berhasil";
          $transaction->commit();
      } catch (\Exception $e) {
          $transaction->rollBack();
          throw $e;
      } catch (\Throwable $e) {
          $transaction->rollBack();
          throw $e;
      }
    }

    //===========================//
    public function actionProgram()
    {

        $log = new Savelog();
        $log->save('Akses Menu Porgram SKPD Berhasil', 'Program SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
        $unit = Yii::$app->levelcomponent->getUnit();
        $PosisiUnit = Yii::$app->levelcomponent->PosisiUnit();

        $searchModel = new TaProgramSearch();
        $dataProvider = $searchModel->searchProgram2(Yii::$app->request->queryParams);
        $modelUnit = TaSubUnit::find()->where($PosisiUnit)->all();
        $data = TaSubUnit::findone($PosisiUnit);

        $tabelanjanrincsub = TaBelanjaRincSub::find()
                          ->where($PosisiUnit)
                          ->sum('Total');

        $modelLevel = $PosisiUnit;

        return $this->render('program', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelLevel' => $modelLevel,
                    'modelUnit' => $modelUnit,
                    'data' => $data,
                    'tabelanjanrincsub' => $tabelanjanrincsub,
        ]);
    }

    public function actionPaguProgram()
    {

        $unitskpd = Yii::$app->levelcomponent->getUnit();
        $data = TaPaguSubUnit::find()
                        ->where([
                            'Tahun' => date("Y"),
                            'Kd_Urusan' => $unitskpd->Kd_Urusan,
                            'Kd_Bidang' => $unitskpd->Kd_Bidang,
                            'Kd_Unit' => $unitskpd->Kd_Unit, 
                            'Kd_Sub' => $unitskpd->Kd_Sub_Unit,    
                            ])
                        ->all();

        return $this->render('pagu-program', [
            'data' => $data,
        ]);
    }

    public function actionRincian()
    {
        return $this->render('rincian', ['cinta' => []]);
    }


    public function actionSasaran($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {

      $log = new Savelog();
      $log->save('Akses Menu Sasaran Kegiatan Berhasil', 'Sasaran Kegiatan', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
      $PosisiKegiatan = [
      'Tahun' => $Tahun, 
      'Kd_Urusan' => $Kd_Urusan, 
      'Kd_Bidang' => $Kd_Bidang,
      'Kd_Unit' => $Kd_Unit,
      'Kd_Sub' => $Kd_Sub,
      'Kd_Prog' => $Kd_Prog,
      'Kd_Keg' => $Kd_Keg,
      ];
      
      // echo $Tahun;
      // echo $Kd_Urusan;
      // echo $Kd_Bidang;
      // echo $Kd_Unit;
      // echo $Kd_Sub;
      // echo $Kd_Prog;
      // echo $Kd_Keg;
      // die();
     // $model = new TaKegiatanSasaran();
      if ($model = TaKegiatanSasaran::findOne($PosisiKegiatan)) {
        $status = 1;
      } else{
        $model = new TaKegiatanSasaran();
        // $model->Tahun= $Tahun;
        // $model->Kd_Urusan = $Kd_Urusan;
        // $model->Kd_Bidang = $Kd_Bidang;
        // $model->Kd_Unit = $Kd_Unit;
        // $model->Kd_Sub = $Kd_Sub;
        // $model->Kd_Prog = $Kd_Prog;
        // $model->Kd_Keg = $Kd_Keg;
        $status = 0;
        // $model->save();
      }
    
      //$model = TaKegiatanSasaran::findOne($PosisiKegiatan);
      // echo $model->Sasaran;
      // die();
      // if (isset($modelCari)) {

      //   $ubah = 1;
      // }else {

      //   $ubah = "";
      // }

      return $this->renderajax('tambah-sasaran',[
          'model' => $model, 
          'Tahun' => $Tahun, 
          'Kd_Urusan' => $Kd_Urusan, 
          'Kd_Bidang' => $Kd_Bidang, 
          'Kd_Unit' => $Kd_Unit, 
          'Kd_Sub' => $Kd_Sub, 
          'Kd_Prog' => $Kd_Prog,
          'Kd_Keg' => $Kd_Keg,
          'status' => $status,
           // 'modelCari'=> $modelCari,

          ]);
    }


    public function actionTambahSasaranProses()
    {

      $log = new Savelog();
      $log->save('Akses Tambah Sasaran Kegiatan Berhasil', 'Tambah Sasaran Kegiatan', '', ''); //pesan, kegiatan, tabel, id dari tabel
       

      $request = Yii::$app->request;
      $model = new TaKegiatanSasaran();
      if($model->load($request->post())){

          $model->save(false);
      }
          echo "Masukkan Sasaran Berhasil";
    }

    public function actionUbahSasaranProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {

    $log = new Savelog();
    $log->save('Ubah Sasaran Kegiatan Berhasil', 'Ubah Sasaran Kegiatan', '', ''); //pesan, kegiatan, tabel, id dari tabel
       

    $PosisiSasaran = [
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            ];
      $model = TaKegiatanSasaran::findOne($PosisiSasaran);

      if($model->load(Yii::$app->request->post())){

          $model->save(false);
      }
          echo "Ubah Sasaran Berhasil";
    }

    public function actionIndikator($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {
      $log = new Savelog();
      $log->save('Akses Indikator Kegiatan Berhasil', 'Indikator Kegiatan', '', ''); //pesan, kegiatan, tabel, id dari tabel
       
      $PosisiKegiatan = [
        'Tahun' => $Tahun, 
        'Kd_Urusan' => $Kd_Urusan, 
        'Kd_Bidang' => $Kd_Bidang,
        'Kd_Unit' => $Kd_Unit,
        'Kd_Sub' => $Kd_Sub,
        'Kd_Prog' => $Kd_Prog,
        'Kd_Keg' => $Kd_Keg,
      ];
      // print_r($PosisiKegiatan);
      // die;
      $model_kegiatan = TaKegiatan::findOne($PosisiKegiatan);

      $model_keluaran = TaIndikator::find()
                      ->where($PosisiKegiatan)
                      ->andWhere(['Kd_Indikator'=>3])
                      ->one();

      $model_hasil = TaIndikator::find()
                      ->where($PosisiKegiatan)
                      ->andWhere(['Kd_Indikator'=>4])
                      ->one();

      $model_n1 = TaIndikator::find()
                      ->where($PosisiKegiatan)
                      ->andWhere(['Kd_Indikator'=>7])
                      ->one();

      if ($model_keluaran == NULL ) {
        $mas_mod = new TaIndikator();
        $mas_mod->Tahun = $Tahun;
        $mas_mod->Kd_Urusan = $Kd_Urusan;
        $mas_mod->Kd_Bidang = $Kd_Bidang;
        $mas_mod->Kd_Unit = $Kd_Unit;
        $mas_mod->Kd_Sub = $Kd_Sub;
        $mas_mod->Kd_Prog = $Kd_Prog;
        // $mas_mod->ID_Prog = $ID_Prog;
        $mas_mod->Kd_Keg = $Kd_Keg;
        $mas_mod->Kd_Indikator = 2;
        $mas_mod->No_ID = 0;
        $mas_mod->save(false);
        //=========================================//

        $kel_mod = new TaIndikator();
        $kel_mod->Tahun = $Tahun;
        $kel_mod->Kd_Urusan = $Kd_Urusan;
        $kel_mod->Kd_Bidang = $Kd_Bidang;
        $kel_mod->Kd_Unit = $Kd_Unit;
        $kel_mod->Kd_Sub = $Kd_Sub;
        $kel_mod->Kd_Prog = $Kd_Prog;
        // $kel_mod->ID_Prog = $ID_Prog;
        $kel_mod->Kd_Keg = $Kd_Keg;
        $kel_mod->Kd_Indikator = 3;
        $kel_mod->No_ID = 0;
        $kel_mod->save(false);

        $model_keluaran = TaIndikator::find()
                      ->where($PosisiKegiatan)
                      ->andWhere(['Kd_Indikator'=>3])
                      ->one();
      }

      if ($model_hasil == NULL ) {
        $has_mod = new TaIndikator();
        $has_mod->Tahun = $Tahun;
        $has_mod->Kd_Urusan = $Kd_Urusan;
        $has_mod->Kd_Bidang = $Kd_Bidang;
        $has_mod->Kd_Unit = $Kd_Unit;
        $has_mod->Kd_Sub = $Kd_Sub;
        $has_mod->Kd_Prog = $Kd_Prog;
        // $has_mod->ID_Prog = $ID_Prog;
        $has_mod->Kd_Keg = $Kd_Keg;
        $has_mod->Kd_Indikator = 4;
        $has_mod->No_ID = 0;
        $has_mod->save(false);

        $model_hasil = TaIndikator::find()
                      ->where($PosisiKegiatan)
                      ->andWhere(['Kd_Indikator'=>4])
                      ->one();
      }

      if ($model_n1 == NULL ) {
        $n_mod = new TaIndikator();
        $n_mod->Tahun = $Tahun;
        $n_mod->Kd_Urusan = $Kd_Urusan;
        $n_mod->Kd_Bidang = $Kd_Bidang;
        $n_mod->Kd_Unit = $Kd_Unit;
        $n_mod->Kd_Sub = $Kd_Sub;
        $n_mod->Kd_Prog = $Kd_Prog;
        // $n_mod->ID_Prog = $ID_Prog;
        $n_mod->Kd_Keg = $Kd_Keg;
        $n_mod->Kd_Indikator = 7;
        $n_mod->No_ID = 0;
        $n_mod->save(false);

        $model_keluaran = TaIndikator::find()
                      ->where($PosisiKegiatan)
                      ->andWhere(['Kd_Indikator'=>7])
                      ->one();
      }

      $satuan =  ArrayHelper::map(Satuan::find()->orderBy('satuan')->all(),'satuan','satuan');

      return $this->renderajax('tambah-indikator',[
          'model_kegiatan' => $model_kegiatan, 
          'model_keluaran' => $model_keluaran, 
          'model_hasil' => $model_hasil, 
          'model_n1' => $model_n1, 
          'Tahun' => $Tahun, 
          'Kd_Urusan' => $Kd_Urusan, 
          'Kd_Bidang' => $Kd_Bidang, 
          'Kd_Unit' => $Kd_Unit, 
          'Kd_Sub' => $Kd_Sub, 
          'Kd_Prog' => $Kd_Prog,
          'Kd_Keg' => $Kd_Keg,
          'satuan' => $satuan,
          // 'status' => $status,

          ]);
    }

    public function actionIndikatorProses($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg)
    {
      $log = new Savelog();
      $log->save('Ubah Indikator Kegiatan Proses', 'Ubah Indikator Proses', '', ''); //pesan, kegiatan, tabel, id dari tabel
      
      $PosisiKegiatan = [
        'Tahun' => $Tahun, 
        'Kd_Urusan' => $Kd_Urusan, 
        'Kd_Bidang' => $Kd_Bidang,
        'Kd_Unit' => $Kd_Unit,
        'Kd_Sub' => $Kd_Sub,
        'Kd_Prog' => $Kd_Prog,
        'Kd_Keg' => $Kd_Keg,
      ];

      $model_kegiatan = TaKegiatan::findOne($PosisiKegiatan);

      $model_keluaran = TaIndikator::find()
                      ->where($PosisiKegiatan)
                      ->andWhere(['Kd_Indikator'=>3])
                      ->one();

      $model_hasil = TaIndikator::find()
                      ->where($PosisiKegiatan)
                      ->andWhere(['Kd_Indikator'=>4])
                      ->one();

      $model_n1 = TaIndikator::find()
                      ->where($PosisiKegiatan)
                      ->andWhere(['Kd_Indikator'=>7])
                      ->one();
      
      $request = Yii::$app->request;
      
      $Pagu_Anggaran_Nt1 = $request->post('Pagu_Anggaran_Nt1');
      $keluaran = $request->post('keluaran');
      $hasil = $request->post('hasil');
      $n1 = $request->post('n1');

      $model_kegiatan->Pagu_Anggaran_Nt1 = $Pagu_Anggaran_Nt1;

      $model_keluaran->Tolak_Ukur = $keluaran['tolak_ukur'];
      $model_keluaran->Target_Angka = $keluaran['target'];
      $model_keluaran->Target_Uraian = $keluaran['satuan'];

      $model_hasil->Tolak_Ukur = $hasil['tolak_ukur'];
      $model_hasil->Target_Angka = $hasil['target'];
      $model_hasil->Target_Uraian = $hasil['satuan'];

      $model_n1->Tolak_Ukur = $n1['tolak_ukur'];
      $model_n1->Target_Angka = $n1['target'];
      $model_n1->Target_Uraian = $n1['satuan'];

      if ($model_kegiatan->save(false) && $model_keluaran->save(false) && $model_hasil->save(false) && $model_n1->save(false)) {
        echo "Berhasil";
      }
    }


    //===================================
    public function actionUbahVolume($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_Prog, $Kd_Keg, $Kd_Rek_1, $Kd_Rek_2, $Kd_Rek_3, $Kd_Rek_4, $Kd_Rek_5, $No_Rinc, $No_ID)
    {
      $log = new Savelog();
      $log->save('Akses Ubah Volum Rincian Obyek SKPD', 'Ubah Rincian Obyek SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
      $request = Yii::$app->request;

      $Posisi = [
            'Tahun' => $Tahun, 
            'Kd_Urusan' => $Kd_Urusan, 
            'Kd_Bidang' => $Kd_Bidang,
            'Kd_Unit' => $Kd_Unit,
            'Kd_Sub' => $Kd_Sub,
            'Kd_Prog' => $Kd_Prog,
            'Kd_Keg' => $Kd_Keg,
            'Kd_Rek_1' => $Kd_Rek_1,
            'Kd_Rek_2' => $Kd_Rek_2,
            'Kd_Rek_3' => $Kd_Rek_3,
            'Kd_Rek_4' => $Kd_Rek_4,
            'Kd_Rek_5' => $Kd_Rek_5,
            'No_Rinc' => $No_Rinc,
            'No_ID' => $No_ID,
            ];
      $model = TaBelanjaRincSub::findOne($Posisi);
      
      if($model->load($request->post())){
        $model->Total = str_replace(".", "", $model->Total);
        $model->save(false);
        $log = new Savelog();
        $log->save('Akses Ubah Volum Rincian Obyek SKPD Berhasil', 'Ubah Rincian Obyek SKPD', '', ''); //pesan, kegiatan, tabel, id dari tabel
        return $this->redirect(['belanja-rinc-sub', 
                              'Tahun' => $Tahun, 
                              'Kd_Urusan' => $Kd_Urusan, 
                              'Kd_Bidang' => $Kd_Bidang,
                              'Kd_Unit' => $Kd_Unit,
                              'Kd_Sub' => $Kd_Sub,
                              'Kd_Prog' => $Kd_Prog,
                              'Kd_Keg' => $Kd_Keg,
                              'Kd_Rek_1' => $Kd_Rek_1,
                              'Kd_Rek_2' => $Kd_Rek_2,
                              'Kd_Rek_3' => $Kd_Rek_3,
                              'Kd_Rek_4' => $Kd_Rek_4,
                              'Kd_Rek_5' => $Kd_Rek_5,
                              'No_Rinc' => $No_Rinc,
                              'No_ID' => $No_ID,
                            ]);
      }
      else{
        $Standard_Satuan=ArrayHelper::map(RefStandardSatuan::find()->orderby('Uraian')->all(),'Uraian','Uraian');
      
        return $this->renderajax('ubah_volume', [
                'Tahun' => $Tahun, 
                'Kd_Urusan' => $Kd_Urusan, 
                'Kd_Bidang' => $Kd_Bidang, 
                'Kd_Unit' => $Kd_Unit, 
                'Kd_Sub' => $Kd_Sub, 
                'Kd_Prog' => $Kd_Prog,
                'Kd_Keg' => $Kd_Keg, 
                'Kd_Rek_1' => $Kd_Rek_1,
                'Kd_Rek_2' => $Kd_Rek_2,
                'Kd_Rek_3' => $Kd_Rek_3,
                'Kd_Rek_4' => $Kd_Rek_4,
                'Kd_Rek_5' => $Kd_Rek_5,
                'No_Rinc' => $No_Rinc,
                'No_ID' => $No_ID,
                'model' => $model,
                'Standard_Satuan' => $Standard_Satuan
        ]);
      }
    }
}


