<?php

namespace emusrenbang\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RefKamusProgram;

/**
 * RefKamusProgramSearch represents the model behind the search form of `common\models\RefKamusProgram`.
 */
class RefKamusProgramSearch extends RefKamusProgram
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kd_Program', 'Status'], 'integer'],
            [['Nm_Program', 'Sasaran'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefKamusProgram::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Kd_Program' => $this->Kd_Program,
            'Status' => $this->Status,
        ]);

        $query->andFilterWhere(['like', 'Nm_Program', $this->Nm_Program])
            ->andFilterWhere(['like', 'Sasaran', $this->Sasaran]);

        return $dataProvider;
    }
}
