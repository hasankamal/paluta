<?php

namespace emusrenbang\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emusrenbang\models\TaKegiatanRiwayat;

/**
 * TaKegiatanRiwayatSearch represents the model behind the search form about `emusrenbang\models\TaKegiatanRiwayat`.
 */
class TaKegiatanRiwayatSearch extends TaKegiatanRiwayat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Prog', 'Kd_Keg', 'Kd_Unit', 'Kd_Sub', 'ID_Prog', 'Kd_Sumber', 'Status', 'Verifikasi_Bappeda', 'Tanggal_Verifikasi_Bappeda'], 'integer'],
            [['Tahun', 'Ket_Kegiatan', 'Lokasi', 'Kelompok_Sasaran', 'Status_Kegiatan', 'Waktu_Pelaksanaan', 'Keterangan', 'Keterangan_Verifikasi_Bappeda', 'Tanggal_Riwayat', 'Keterangan_Riwayat'], 'safe'],
            [['Pagu_Anggaran', 'Pagu_Anggaran_Nt1'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaKegiatanRiwayat::find()
                ->OrderBy(['Tanggal_Riwayat'=> SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Id' => $this->Id,
            'Tahun' => $this->Tahun,
            'Kd_Urusan' => $this->Kd_Urusan,
            'Kd_Bidang' => $this->Kd_Bidang,
            'Kd_Prog' => $this->Kd_Prog,
            'Kd_Keg' => $this->Kd_Keg,
            'Kd_Unit' => $this->Kd_Unit,
            'Kd_Sub' => $this->Kd_Sub,
            'ID_Prog' => $this->ID_Prog,
            'Pagu_Anggaran' => $this->Pagu_Anggaran,
            'Kd_Sumber' => $this->Kd_Sumber,
            'Status' => $this->Status,
            'Pagu_Anggaran_Nt1' => $this->Pagu_Anggaran_Nt1,
            'Verifikasi_Bappeda' => $this->Verifikasi_Bappeda,
            'Tanggal_Verifikasi_Bappeda' => $this->Tanggal_Verifikasi_Bappeda,
            'Tanggal_Riwayat' => $this->Tanggal_Riwayat,
        ]);

        $query->andFilterWhere(['like', 'Ket_Kegiatan', $this->Ket_Kegiatan])
            ->andFilterWhere(['like', 'Lokasi', $this->Lokasi])
            ->andFilterWhere(['like', 'Kelompok_Sasaran', $this->Kelompok_Sasaran])
            ->andFilterWhere(['like', 'Status_Kegiatan', $this->Status_Kegiatan])
            ->andFilterWhere(['like', 'Waktu_Pelaksanaan', $this->Waktu_Pelaksanaan])
            ->andFilterWhere(['like', 'Keterangan', $this->Keterangan])
            ->andFilterWhere(['like', 'Keterangan_Verifikasi_Bappeda', $this->Keterangan_Verifikasi_Bappeda])
            ->andFilterWhere(['like', 'Keterangan_Riwayat', $this->Keterangan_Riwayat]);

        return $dataProvider;
    }
}
