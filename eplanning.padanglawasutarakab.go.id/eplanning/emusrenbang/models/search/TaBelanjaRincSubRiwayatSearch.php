<?php

namespace emusrenbang\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emusrenbang\models\TaBelanjaRincSubRiwayat;

/**
 * TaBelanjaRincSubRiwayatSearch represents the model behind the search form about `emusrenbang\models\TaBelanjaRincSubRiwayat`.
 */
class TaBelanjaRincSubRiwayatSearch extends TaBelanjaRincSubRiwayat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Kd_Prog', 'ID_Prog', 'Kd_Keg', 'Kd_Rek_1', 'Kd_Rek_2', 'Kd_Rek_3', 'Kd_Rek_4', 'Kd_Rek_5', 'No_Rinc', 'No_ID'], 'integer'],
            [['Tahun', 'Sat_1', 'Sat_2', 'Sat_3', 'Satuan123', 'Keterangan', 'Asal_Biaya', 'Uraian_Asal_Biaya', 'Ref_Usulan_Rincian', 'Uraian_Ref_Usulan', 'Tanggal_Riwayat', 'Keterangan_Riwayat'], 'safe'],
            [['Nilai_1', 'Nilai_2', 'Nilai_3', 'Jml_Satuan', 'Nilai_Rp', 'Total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaBelanjaRincSubRiwayat::find()
                ->OrderBy(['Tanggal_Riwayat'=> SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Id' => $this->Id,
            'Tahun' => $this->Tahun,
            'Kd_Urusan' => $this->Kd_Urusan,
            'Kd_Bidang' => $this->Kd_Bidang,
            'Kd_Unit' => $this->Kd_Unit,
            'Kd_Sub' => $this->Kd_Sub,
            'Kd_Prog' => $this->Kd_Prog,
            'ID_Prog' => $this->ID_Prog,
            'Kd_Keg' => $this->Kd_Keg,
            'Kd_Rek_1' => $this->Kd_Rek_1,
            'Kd_Rek_2' => $this->Kd_Rek_2,
            'Kd_Rek_3' => $this->Kd_Rek_3,
            'Kd_Rek_4' => $this->Kd_Rek_4,
            'Kd_Rek_5' => $this->Kd_Rek_5,
            'No_Rinc' => $this->No_Rinc,
            'No_ID' => $this->No_ID,
            'Nilai_1' => $this->Nilai_1,
            'Nilai_2' => $this->Nilai_2,
            'Nilai_3' => $this->Nilai_3,
            'Jml_Satuan' => $this->Jml_Satuan,
            'Nilai_Rp' => $this->Nilai_Rp,
            'Total' => $this->Total,
            'Tanggal_Riwayat' => $this->Tanggal_Riwayat,
        ]);

        $query->andFilterWhere(['like', 'Sat_1', $this->Sat_1])
            ->andFilterWhere(['like', 'Sat_2', $this->Sat_2])
            ->andFilterWhere(['like', 'Sat_3', $this->Sat_3])
            ->andFilterWhere(['like', 'Satuan123', $this->Satuan123])
            ->andFilterWhere(['like', 'Keterangan', $this->Keterangan])
            ->andFilterWhere(['like', 'Asal_Biaya', $this->Asal_Biaya])
            ->andFilterWhere(['like', 'Uraian_Asal_Biaya', $this->Uraian_Asal_Biaya])
            ->andFilterWhere(['like', 'Ref_Usulan_Rincian', $this->Ref_Usulan_Rincian])
            ->andFilterWhere(['like', 'Uraian_Ref_Usulan', $this->Uraian_Ref_Usulan])
            ->andFilterWhere(['like', 'Keterangan_Riwayat', $this->Keterangan_Riwayat]);

        return $dataProvider;
    }
}
