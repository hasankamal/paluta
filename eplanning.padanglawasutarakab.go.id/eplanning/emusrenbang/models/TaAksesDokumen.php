<?php

namespace emusrenbang\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "Ta_Akses_Dokumen".
 *
 * @property int $Kd_Akses
 * @property string $Tanggal
 * @property int $Kd_File
 * @property int $Kd_User
 * @property int $Kd_Urusan
 * @property int $Kd_Bidang
 * @property int $Kd_Unit
 * @property int $Kd_Sub
 * @property string $status
 */
class TaAksesDokumen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Akses_Dokumen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['File'], 'required'],
            [['Waktu_Buka', 'Waktu_Tutup'], 'safe'],
            [['Kd_File', 'Kd_User', 'Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub'], 'integer'],
            [['Status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kd_Akses' => 'Kd  Akses',
            'Tanggal' => 'Tanggal',
            'Kd_File' => 'Kd  File',
            'Kd_User' => 'Kd  User',
            'Kd_Urusan' => 'Kd  Urusan',
            'Kd_Bidang' => 'Kd  Bidang',
            'Kd_Unit' => 'Kd  Unit',
            'Kd_Sub' => 'Kd  Sub',
            'Status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     * @return TaAksesDokumenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaAksesDokumenQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'mdm\upload\UploadBehavior',
                'attribute' => 'File', // required, use to receive input file
                'savedAttribute' => 'Kd_File', // optional, use to link model with saved file.
                'uploadPath' => '@common/upload', // saved directory. default to '@runtime/upload'
                'autoSave' => true, // when true then uploaded file will be save before ActiveRecord::save()
                'autoDelete' => true, // when true then uploaded file will deleted before ActiveRecord::delete()
            ],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'Kd_User']);
    }
}
