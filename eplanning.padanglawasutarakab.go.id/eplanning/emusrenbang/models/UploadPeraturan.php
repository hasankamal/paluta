<?php

namespace emusrenbang\models;

use yii\base\Model;
use yii\web\UploadedFile;
use common\models\RefMedia;

class UploadPeraturan extends Model {

    /**
     * @var UploadedFile
     */
    public $pdfFile;
    public $imageFile;
    public $namePdf;

    public function rules() {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 50],
            [['pdfFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'maxFiles' => 2],
            
        ];
    }

    public function attributeLabels() {
        return [

            'pdfFile' => 'Berkas PDF Peraturan',
            'imageFile' => 'Berkas Gambar Peraturan',
        ];
    }

    public function upload() {
        //if ($this->validate()) {
        if ($this->imageFile !== null) {
            $i = 0;
            foreach ($this->imageFile as $file) {
                $date = time();
                $file->saveAs(realpath(dirname(dirname(__FILE__))) . '/web/data/' . preg_replace('/\s+/', '_',$file->baseName) . $date . '.' . $file->extension);
                $model1 = new RefMedia();
                $model1->Jenis_Media = $file->extension;
                $model1->Type_Media = $file->extension;
                $model1->Judul_Media = preg_replace('/\s+/', '_',$file->baseName);
                $model1->Nm_Media = preg_replace('/\s+/', '_',$file->baseName) . $date . '.' . $file->extension;
                $model1->Created_At = $date;
                $this->nameImage[$i] = preg_replace('/\s+/', '_',$file->baseName) . $date . '.' . $file->extension;
                $model1->save(false);
                $i++;
            }
        }
        if ($this->pdfFile !== null) {
            $i = 0;
            foreach ($this->pdfFile as $file) {
                $date = time();
                $file->saveAs(realpath(dirname(dirname(__FILE__))) . '/web/data/' . preg_replace('/\s+/', '_',$file->baseName) . $date . '.' . $file->extension);
                $model1 = new RefMedia();
                $model1->Jenis_Media = $file->extension;
                $model1->Type_Media = $file->extension;
                $model1->Judul_Media = preg_replace('/\s+/', '_',$file->baseName);
                $model1->Nm_Media = preg_replace('/\s+/', '_',$file->baseName) . $date . '.' . $file->extension;
                $model1->Created_At = $date;
                $this->namePdf[$i] = preg_replace('/\s+/', '_',$file->baseName) . $date . '.' . $file->extension;
                $model1->save(false);
                $i++;
            }
        }
        
	   return true;
    }

}

?>