<?php

namespace emusrenbang\models;

/**
 * This is the ActiveQuery class for [[TaAksesDokumen]].
 *
 * @see TaAksesDokumen
 */
class TaAksesDokumenQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TaAksesDokumen[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TaAksesDokumen|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
