<?php

namespace emusrenbang\models;

use Yii;

/**
 * This is the model class for table "Ta_Peraturan_Pagu".
 *
 * @property string $Tahun
 * @property int $No_ID
 * @property string $No_Peraturan
 * @property string $Tgl_Peraturan
 * @property string $Uraian
 *
 * @property TaPaguSubUnitRiwayat[] $taPaguSubUnitRiwayats
 */
class TaPeraturanPagu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ta_Peraturan_Pagu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tahun', 'No_ID', 'Tgl_Peraturan', 'No_Peraturan','Uraian'], 'required'],
            [['Tahun', 'Tgl_Peraturan'], 'safe'],
            [['No_ID'], 'integer'],
            [['No_Peraturan'], 'string', 'max' => 50],
            [['Uraian'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Tahun' => 'Tahun',
            'No_ID' => 'No  ID',
            'No_Peraturan' => 'No  Peraturan',
            'Tgl_Peraturan' => 'Tgl  Peraturan',
            'Uraian' => 'Uraian',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaPaguSubUnitRiwayats()
    {
        return $this->hasMany(TaPaguSubUnitRiwayat::className(), ['Id_Peraturan' => 'No_ID']);
    }

    /**
     * @inheritdoc
     * @return \emusrenbang\models\query\TaPaguSubUnitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \emusrenbang\models\query\TaPaguSubUnitQuery(get_called_class());
    }

     public function getTaPaguMedias()
    {
        return $this->hasMany(\common\models\TaPaguMedia::className(), ['Id_Peraturan' => 'No_ID']);
    }


}
