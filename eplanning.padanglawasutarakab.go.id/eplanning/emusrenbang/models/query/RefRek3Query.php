<?php

namespace emusrenbang\models\query;

/**
 * This is the ActiveQuery class for [[\emusrenbang\models\RefRek3]].
 *
 * @see \emusrenbang\models\RefRek3
 */
class RefRek3Query extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \emusrenbang\models\RefRek3[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \emusrenbang\models\RefRek3|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
