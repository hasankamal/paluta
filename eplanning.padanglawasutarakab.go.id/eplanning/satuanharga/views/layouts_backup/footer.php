<!--Footer Start Here -->
<footer class="footer-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="footer-left">
                    <span><a href="">E-Planning Pemkab Paluta</a></span>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="footer-right">
                    <span class="footer-meta">&copy; Bappeda Kabupaten Paluta</span>
                </div>
            </div>
        </div>
    </div>
</footer>