<?php

namespace userlevel\controllers;

use Yii;
use common\models\User;
use common\models\Log;

class LogUserController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new \userlevel\models\searchs\User();
        	
      	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
				
      	return $this->render('index', [
          	'searchModel' => $searchModel,
          	'dataProvider' => $dataProvider,
      	]);
    }

    public function actionDetail()
    {
      // $data = Yii::$app->request->queryParams;
      // echo $data['user_id'];
      // die();
    	$searchModel = new \userlevel\models\searchs\LogSearch();
      	
    	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			
    	return $this->render('log_detail', [
        	'searchModel' => $searchModel,
        	'dataProvider' => $dataProvider,
    	]);
    }

    public function actionMonitoring()
    {
      // $user = user::find()->where(['aktif'=>1])->all();
      $log = Log::find()->limit(20)->orderBy(['Kd_Log' => SORT_DESC])->all();
      
      return $this->render('monitoring', [
          // 'user' => $user,
          'log' => $log,
      ]);
    }

    public function actionLogoutUser($id)
    {
      $model_user = User::find()->where(['id' => $id])->one();
      $model_user->aktif = 0;
      if ($model_user->save(false)) {
        return 1;
      }
      else{
        return 0;
      }
    }

    public function actionUserLogin()
    {
      $user = user::find()->where(['aktif'=>1])->all();
      return $this->renderAjax('user_login', [
          'user' => $user,
      ]);
    }

}
