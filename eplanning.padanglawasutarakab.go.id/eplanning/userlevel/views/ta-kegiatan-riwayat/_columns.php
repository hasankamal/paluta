<?php
use yii\helpers\Url;
use common\components\Helper;
use yii\helpers\ArrayHelper;
use common\models\RefUrusan;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    //     [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'Id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Tahun',
    ],
        [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Tanggal_Riwayat',
        
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Keterangan_Riwayat',
    ],
    [
        // 'class'=>'\kartik\grid\DataColumn',
        'value' => 'urusan.Nm_Urusan',
        'attribute'=>'Kd_Urusan',
        'format'=>'raw',
        'filter'=> $data_urusan,
        'label'=>'Urusan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Bidang',
        'value' => 'bidang.Nm_Bidang',
        'filter'=> $data_bidang,
        'label'=>'Bidang',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Unit',
        'value' => 'unit.Nm_Unit',
        'filter' => $data_unit,
        'label'=>'Unit',

    ],
        [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Sub',
        'value' => 'sub.Nm_Sub_Unit',
        'filter' => $data_sub,
        'label'=>'Sub Unit',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Prog',
        'value' => 'program.Ket_Program',
        'filter' => $data_program,
        'label'=>'Program',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'Kd_Keg',
    // ],

    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'ID_Prog',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'value' => 'Ket_Kegiatan',
        'attribute'=>'Ket_Kegiatan',
        'label'=>'Kegiatan',

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Lokasi',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kelompok_Sasaran',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Status_Kegiatan',
        'value' => function ($model) {
        if($model->Status_Kegiatan==1)
              return 'Baru';   
          else
              return 'Cadangan';   
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Pagu_Anggaran',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Waktu_Pelaksanaan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Sumber',
        'value'=>'kdSumber.Nm_Sumber',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'Status',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Keterangan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Pagu_Anggaran_Nt1',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'Verifikasi_Bappeda',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'Tanggal_Verifikasi_Bappeda',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'Keterangan_Verifikasi_Bappeda',
    // ],
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to([$action,'Id, $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Prog, $Kd_Keg, $Kd_Unit, $Kd_Sub'=>$key]);
    //     },
    //     'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>'Are you sure?',
    //                       'data-confirm-message'=>'Are you sure want to delete this item'], 
    // ],

];   