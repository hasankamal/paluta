<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emusrenbang\models\TaKegiatanRiwayat */
?>
<div class="ta-kegiatan-riwayat-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'Tahun',
            'Kd_Urusan',
            'Kd_Bidang',
            'Kd_Prog',
            'Kd_Keg',
            'Kd_Unit',
            'Kd_Sub',
            'ID_Prog',
            'Ket_Kegiatan',
            'Lokasi',
            'Kelompok_Sasaran',
            'Status_Kegiatan',
            'Pagu_Anggaran',
            'Waktu_Pelaksanaan',
            'Kd_Sumber',
            'Status',
            'Keterangan:ntext',
            'Pagu_Anggaran_Nt1',
            'Verifikasi_Bappeda',
            'Tanggal_Verifikasi_Bappeda',
            'Keterangan_Verifikasi_Bappeda:ntext',
            'Tanggal_Riwayat',
            'Keterangan_Riwayat',
        ],
    ]) ?>

</div>
