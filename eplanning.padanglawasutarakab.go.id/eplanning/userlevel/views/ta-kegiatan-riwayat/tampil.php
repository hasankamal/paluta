
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\components\Helper;
use yii\helpers\ArrayHelper;
use common\models\RefUrusan;


/* @var $this yii\web\View */
/* @var $model eperencanaan\models\TaMusrenbang */

$this->title = 'Usulan Kecamatan';
$this->params['breadcrumbs'][] = ['label' => 'Ta Musrenbangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="ta-musrenbang-view col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    <?= $this->title; ?>
                </h3>
                <div class="control-wrap">
                    <?php $form = \yii\bootstrap\ActiveForm::begin([
                                          'id' => 'search-usulan',
                                'action' => ['ta-musrenbang/cetak-usulan-kecamatan'], 
                                'options' => ['target' => '_blank']
                    ]) ?>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <br>
                            <?= Html::submitButton('&nbsp;Cetak&nbsp;', ['id' => 'cari-submit', 'class' => 'btn btn-primary btn-lg']); ?>
                        </div>
                    </div>
                    <?php \yii\bootstrap\ActiveForm::end() ?>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table">
                     <?= 
                          GridView::widget([
                          'dataProvider' => $dataProvider,
                          'filterModel' => $searchModel,
                          'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'value' => 'urusan.Nm_Urusan',
                            'attribute'=>'Urusan',
                            'filter'=> ArrayHelper::map(RefUrusan::find()->asArray()->all(), 'Kd_Urusan', 'Nm_Urusan'),
                        ],
                        [
                            'attribute'=>'Bidang',
                            'value' => 'bidang.Nm_Bidang',
                            'filter'=> $data_bidang,
                        ],
                        [
                            'attribute'=>'Unit',
                            'value' => 'unit.Nm_Unit',
                            'filter' => $data_unit,

                        ],
                        [
                            'attribute'=>'Sub Unit',
                            'value' => 'sub.Nm_Sub_Unit',
                        ],
                        [
                            'attribute'=>'Program',
                            'value' => 'program.Ket_Program',
                        ],
                        // [
                        //     'class'=>'\kartik\grid\DataColumn',
                        //     'attribute'=>'Kd_Keg',
                        // ],

                        // [
                            // 'class'=>'\kartik\grid\DataColumn',
                            // 'attribute'=>'ID_Prog',
                        // ],
                        [
                            'value' => 'Ket_Kegiatan',
                            'attribute'=>'Kegiatan',
                        ],
                        [
                            'attribute'=>'Lokasi',
                        ],
                        [
                            'attribute'=>'Kelompok_Sasaran',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'Status_Kegiatan',
                            'value' => function ($model) {
                            if($model->Status_Kegiatan==1)
                                  return 'Baru';   
                              else
                                  return 'Cadangan';   
                            }
                        ],
                        [
                            'attribute'=>'Pagu_Anggaran',
                        ],
                        [
                            'attribute'=>'Waktu_Pelaksanaan',
                        ],
                        [
                            'attribute'=>'Kd_Sumber',
                            'value'=>'kdSumber.Nm_Sumber',
                        ],
                        // [
                        //     'class'=>'\kartik\grid\DataColumn',
                        //     'attribute'=>'Status',
                        // ],
                        [
                            'attribute'=>'Keterangan',
                        ],
                        [
                            'attribute'=>'Pagu_Anggaran_Nt1',
                        ],
                        // [
                            // 'class'=>'\kartik\grid\DataColumn',
                            // 'attribute'=>'Verifikasi_Bappeda',
                        // ],
                        // [
                            // 'class'=>'\kartik\grid\DataColumn',
                            // 'attribute'=>'Tanggal_Verifikasi_Bappeda',
                        // ],
                        // [
                            // 'class'=>'\kartik\grid\DataColumn',
                            // 'attribute'=>'Keterangan_Verifikasi_Bappeda',
                        // ],
                        [
                            'attribute'=>'Tanggal_Riwayat',
                        ],
                        [
                            'attribute'=>'Keterangan_Riwayat',
                        ],
                        [
                            'class' => 'kartik\grid\ActionColumn',
                            'dropdown' => false,
                            'vAlign'=>'middle',
                            'urlCreator' => function($action, $model, $key, $index) { 
                                    return Url::to([$action,'Id, $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Prog, $Kd_Keg, $Kd_Unit, $Kd_Sub'=>$key]);
                            },
                            'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
                            'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
                            'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                              'data-request-method'=>'post',
                                              'data-toggle'=>'tooltip',
                                              'data-confirm-title'=>'Are you sure?',
                                              'data-confirm-message'=>'Are you sure want to delete this item'], 
                        ],
                        ],
                    ]); 
                    ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>  