<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emusrenbang\models\TaBelanjaRincRiwayat */
?>
<div class="ta-belanja-rinc-riwayat-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
