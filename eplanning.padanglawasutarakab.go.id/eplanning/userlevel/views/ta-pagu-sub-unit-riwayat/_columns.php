<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
     [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Tanggal_Riwayat',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Waktu_Riwayat',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'value' => 'peraturan.Uraian',
        'attribute'=>'Peraturan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Tahun',
    ],
   [
        // 'class'=>'\kartik\grid\DataColumn',
        'value' => 'urusan.Nm_Urusan',
        'attribute'=>'Kd_Urusan',
        'format'=>'raw',
        'filter'=> $data_urusan,
        'label'=>'Urusan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Bidang',
        'value' => 'bidang.Nm_Bidang',
        'filter'=> $data_bidang,
        'label'=>'Bidang',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Unit',
        'value' => 'unit.Nm_Unit',
        'filter' => $data_unit,
        'label'=>'Unit',

    ],
        [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Kd_Sub',
        'value' => 'sub.Nm_Sub_Unit',
        'filter' => $data_sub,
        'label'=>'Sub Unit',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pagu',
    ],
   
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Keterangan_Riwayat',
    ],
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to([$action,'id'=>$key]);
    //     },
    //     'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>'Are you sure?',
    //                       'data-confirm-message'=>'Are you sure want to delete this item'], 
    // ],

];   