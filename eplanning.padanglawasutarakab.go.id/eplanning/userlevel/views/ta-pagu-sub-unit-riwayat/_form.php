<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaPaguSubUnitRiwayat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ta-pagu-sub-unit-riwayat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Id_Peraturan')->textInput() ?>

    <?= $form->field($model, 'Tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kd_Urusan')->textInput() ?>

    <?= $form->field($model, 'Kd_Bidang')->textInput() ?>

    <?= $form->field($model, 'Kd_Unit')->textInput() ?>

    <?= $form->field($model, 'Kd_Sub')->textInput() ?>

    <?= $form->field($model, 'pagu')->textInput() ?>

    <?= $form->field($model, 'Tanggal_Riwayat')->textInput() ?>

    <?= $form->field($model, 'Waktu_Riwayat')->textInput() ?>

    <?= $form->field($model, 'Keterangan_Riwayat')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
