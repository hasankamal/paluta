<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TaPaguSubUnitRiwayat */
?>
<div class="ta-pagu-sub-unit-riwayat-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'Id_Peraturan',
            'Tahun',
            'Kd_Urusan',
            'Kd_Bidang',
            'Kd_Unit',
            'Kd_Sub',
            'pagu',
            'Tanggal_Riwayat',
            'Waktu_Riwayat',
            'Keterangan_Riwayat',
        ],
    ]) ?>

</div>
