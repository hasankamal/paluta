<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_options extends CI_Model {

	/**
	 * Primary key
	 * @var string
	 */
	public static $pk = 'id';

	/**
	 * Table
	 * @var string
	 */
	public static $table = 'options';

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get data for pagination
	 * @param string
	 * @param int
	 * @param int
	 * @return Query
	 */
	public function get_options($group = 'student_status') {
		$query = $this->db
			->select('id, option')
			->where('group', $group)
			->where('is_deleted', 'false')
			->order_by('option', 'ASC')
			->get(self::$table);
		$data = [];
		foreach($query->result() as $row) {
			$data[$row->id] = $row->option;
		}
		return $data;
	}

	/**
	 * Get options id
	 * @param string 
	 * @param string 
	 * @return int
	 */
	public function get_options_id($group = '', $option = '') {
		$query = $this->db
			->select('id')
			->where('group', $group)
			->where('LOWER(`option`)', $option)
			->limit(1)
			->get('options')
			->row();
		return $query->id;
	}
}