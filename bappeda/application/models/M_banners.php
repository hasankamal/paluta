<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_banners extends CI_Model {

	/**
	 * Primary key
	 * @var string
	 */
	public static $pk = 'id';

	/**
	 * Table
	 * @var string
	 */
	public static $table = 'banners';

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get data for pagination
	 * @param string
	 * @param int
	 * @param int
	 * @return Query
	 */
	public function get_where($keyword, $limit = 0, $offset = 0, $sort_field = '', $sort_type = 'ASC') {
		$this->db->select('id, url, title, target, image, is_deleted');
		$this->db->like('url', $keyword);
		$this->db->or_like('title', $keyword);
		$this->db->or_like('target', $keyword);
		if ($sort_field != '') {
			$this->db->order_by($sort_field, $sort_type);
		}
		if ($limit > 0) {
			$this->db->limit($limit, $offset);
		}
		return $this->db->get(self::$table);
	}

	/**
	 * Get All Data
	 * @return Query
	 */
	public function get_all() {
		return $this->db
			->select('url, title, target, image')
			->get(self::$table);
	}

	/**
	 * Get Total row for pagination
	 * @param string
	 * @return int
	 */
	public function total_rows($keyword) {
		return $this->db
			->like('url', $keyword)
			->or_like('title', $keyword)
			->or_like('target', $keyword)
			->count_all_results(self::$table);
	}

	/**
	 * Get All Banners
	 * @access public
	 * @return Query
	 */
	public function get_banners() {
		return $this->db
			->select('id, title, url, target, image')
			->where('is_deleted', 'false')
			->get('banners');
	}
}