<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Backup_apps extends Admin_Controller {

	/**
	 * Constructor
	 */
   public function __construct() {
      parent::__construct();
      if ($this->session->userdata('user_type') !== 'super_user')
      	redirect(base_url());
   }

	/**
	 * Backup Apps
	 */
	public function index() {
		$this->load->library('zip');
		$this->zip->read_dir(FCPATH, false);
		$file_name = 'backup-apps-on-'. date("Y-m-d-H-i-s") .'.zip';
		$this->zip->download($filename);
	}
}