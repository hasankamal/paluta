<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Chart extends Admin_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model('m_students');
	}

	/**
	 * Index
	 */
	public function index() {
		$this->vars['title'] = 'GRAFIK';
		$this->vars['students'] = $this->vars['chart_students'] = true;
		$query = $this->m_students->student_by_student_status();
		$labels = [];
		$data = [];
		foreach($query->result() as $row) {
			array_push($labels, $row->labels);
			array_push($data, $row->data);
		}
		$this->vars['labels'] = json_encode($labels);
		$this->vars['data'] = json_encode($data);
		$this->vars['content'] = 'students/chart';
		$this->load->view('backend/index', $this->vars);
	}
}