<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Lost_password extends CI_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Index
	 */
	public function index() {
		$this->vars['title'] = 'Lost Password';
		$this->load->view('users/lost_password', $this->vars);
	}
}