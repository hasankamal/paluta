<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_videos extends Public_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model('m_videos');
	}
	
	/**
	 * video
	 */
	public function index() {
		$this->vars['query'] = $this->m_videos->get_videos();
		$this->vars['content'] = 'themes/'.theme_folder().'/loop-videos';
		$this->load->view('themes/'.theme_folder().'/index', $this->vars);
	}
}