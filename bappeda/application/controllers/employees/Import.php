<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends Admin_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model('m_employees');
	}

	/**
	 * Index
	 */
	public function index() {
		$this->vars['title'] = 'IMPORT GURU DAN TENAGA KEPENDIDIKAN';
		$this->vars['employees'] = $this->vars['import_employees'] = true;
		$this->vars['content'] = 'employees/import';
		$this->load->view('backend/index', $this->vars);
	}

	/**
	 * Save
	 */
	public function save() {
		$rows	= explode("\n", $this->input->post('employees'));
		$success = 0;
		$failed = 0;
		$exist = 0;
		foreach($rows as $row) {
			$exp = explode("\t", $row);
			if (count($exp) != 4) continue;
			$arr = [
				'nik' => trim($exp[0]),
				'full_name' => trim($exp[1]),
				'gender' => trim($exp[2]),
				'street_address' => trim($exp[3])
			];
			$query = $this->model->isValExist('nik', trim($exp[0]), 'employees');
			if (!$query) {
				$this->model->insert('employees', $arr) ? $success++ : $failed++;
			} else {
				$exist++;
			}
		}
		$response = [];
		$response['type'] = 'info';
		$response['message'] = 'Success : ' . $success. ' rows, Failed : '. $failed .', Exist : ' . $exist;
		$this->output
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
		exit;
	}
}