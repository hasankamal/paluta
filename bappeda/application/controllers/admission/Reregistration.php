<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reregistration extends Admin_Controller {
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('m_registrants');
	}

	/**
	 * Index
	 */
	public function index() {
		$this->vars['title'] = 'Verifikasi Calon ' . ($this->session->userdata('school_level') == 5 ? 'Mahasiswa Baru' : 'Peserta Didik Baru');
		$this->vars['admission'] = $this->vars['reregistration'] = true;
		$this->vars['content'] = 'admission/reregistration';
		$this->load->view('backend/index', $this->vars);
	}

	/**
	 * Autocomplete
	 */
	public function autocomplete() {
		$keyword = $this->input->get('term'); 
		$query = $this->m_registrants->autocomplete($keyword, 'reregistration');
		$data = [];
		foreach($query->result() as $row) {
			$data[] = $row->registration_number .' - '. $row->full_name;
		}

		$this->output
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($data, JSON_PRETTY_PRINT))
			->_display();
		exit;
	}

	/**
	 * save
	 */
	public function save() {
		$response = [];
		if ($this->validation()) {
			$registrants = explode(',', $this->input->post('registrants'));
			$registration_number = [];
			foreach($registrants as $registrant) {
				$registration_number[] = trim(explode('-', $registrant)[0]);
			}
			$fill_data['created_by'] = $this->session->userdata('id');
			$response['action'] = 'save';
			$response['type'] = $this->m_registrants->re_registration($registration_number) ? 'success' : 'error';
			$response['message'] = $response['type'] == 'success' ? 'created' : 'not_created';
		} else {
			$response['action'] = 'validation_errors';
			$response['type'] = 'error';
			$response['message'] = validation_errors();
		}

		$this->output
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
		exit;
	}

	/**
	 * Validations Form
	 */
	private function validation() {
		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('registrants', 'Registrants', 'trim|required');
		$val->set_error_delimiters('<div>&sdot; ', '</div>');
		return $val->run();
	}
}