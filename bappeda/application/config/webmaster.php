<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['apps']      = 'Dinas Kominfo Padang Lawas Utara';
$config['version']   = '1.0';
$config['webmaster'] = 'M. Sahidin';
$config['email']     = 'msahidin22@gmail.com';
$config['website']   = 'https://diskominfo.padanglawasutarakab.go.id/';
