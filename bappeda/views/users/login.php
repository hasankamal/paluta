<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$title;?></title>
		<?=link_tag('assets/css/bootstrap.css');?>
		<?=link_tag('assets/css/font-awesome.css');?>
		<?=link_tag('assets/css/toastr.css');?>
		<?=link_tag('assets/css/login-form-elements.css');?>
		<?=link_tag('assets/css/login-style.css');?>
		<link rel="icon" href="<?=base_url('assets/img/favicon.png');?>">
		<script type="text/javascript">
			const _BASE_URL = '<?=base_url();?>', _CURRENT_URL = '<?=current_url();?>';
		</script>
		<script src="<?=base_url('assets/js/jquery.js');?>"></script>
		<script src="<?=base_url('assets/js/bootstrap.js');?>"></script>
		<script src="<?=base_url('assets/js/toastr.js');?>"></script>
		<script src="<?=base_url('assets/js/global.js')?>" type="text/javascript"></script>
	</head>
	<script type="text/javascript">
		function login() {
			$('#submit, #username, #password').attr('disabled', 'disabled');
			var values = {
				username : $('#username').val(),
				password : $('#password').val()
			};
			$.post(_BASE_URL + 'login/process', values, function(response) {
				var res = H.stringToJSON(response);
				H.growl(res.type, H.message(res.message));
				if (res.type == 'success') {
					setInterval(function() {
						window.location = _BASE_URL + 'dashboard';
					}, 2000);						
				} else {
					$('#username, #password').val('');
					if (!res.can_logged_in) {
						$('#submit, #username, #password').attr('disabled', 'disabled');
						$('#login-info').text('The login page has been blocked for 30 minutes');
						$('#login-info').addClass('text-danger');
					} else {							
						$('#submit, #username, #password').removeAttr('disabled');	
					}
				}
			});
		}
	</script>
	<body>
		<div class="top-content">
			<div class="inner-bg">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3 form-box">
							<div class="form-top">
								<div class="form-top-left">
									<h2>Login to Bappeda</h2>
									<p id="login-info" <?=$can_logged_in ? '':'class="text-danger"';?>><?=$login_info;?></p>
								</div>
								<div class="form-top-right">
									<i class="fa fa-key"></i>
								</div>
							</div>
							<div class="form-bottom">
								<form role="form" class="login-form">
									<div class="form-group">
										<input <?=$can_logged_in ? '' : 'disabled="disabled"';?> autofocus autocomplete="off" type="text" name="username" placeholder="Username..." class="form-username form-control input-error" id="username">
									</div>
									<div class="form-group">
										<input <?=$can_logged_in ? '' : 'disabled="disabled"';?> type="password" name="password" placeholder="Password..." class="form-password form-control input-error" id="password">
									</div>
									<button <?=$can_logged_in ? '' : 'disabled="disabled"';?> onclick="login(); return false;" class="btn"><i class="fa fa-sign-out"></i> Sign In</button>
								</form>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<p class="login-footer">
							<a href="<?=site_url('lost-password');?>">Lost your password ?</a><br>
							Copyright &copy; 2010 - <?=date('Y');?> padanglawasutarakab.go.id All Rights Reserved<br>
							Powered by <a href="https://padanglawasutarakab.go.id">padanglawasutarakab.go.id</a> &sdot; Back to <a href="<?=base_url()?>"><?=$this->session->userdata('school_name')?></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>