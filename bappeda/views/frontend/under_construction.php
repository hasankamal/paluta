<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$this->session->userdata('school_name')?></title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="<?=$this->session->userdata('meta_keywords');?>"/>
	<meta name="description" content="<?=$this->session->userdata('meta_description');?>"/>
	<meta name="subject" content="Situs Pemkab Padang Lawas Utara">
	<meta name="copyright" content="<?=$this->session->userdata('school_name')?>">
	<meta name="language" content="Indonesia">
	<meta name="robots" content="index,follow" />
	<meta name="revised" content="Sunday, July 18th, 2017, 11:30 pm" />
	<meta name="Classification" content="under construction">
	<meta name="author" content="M. Sahidin, msahidin22@gmail.com">
	<meta name="designer" content="M. Sahidin, msahidin22@gmail.com">
	<meta name="reply-to" content="msahidin22@gmail.com">
	<meta name="owner" content="M. Sahidin">
	<meta name="url" content="https://bappeda.padanglawasutarakab.go.id/">
	<meta name="identifier-URL" content="https://bappeda.padanglawasutarakab.go.id/">
	<meta name="category" content="under construction">
	<meta name="coverage" content="Worldwide">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Copyright" content="<?=$this->session->userdata('school_name');?>" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta name="revisit-after" content="7" />
	<meta name="webcrawlers" content="all" />
	<meta name="rating" content="general" />
	<meta name="spiders" content="all" />
	<meta itemprop="name" content="<?=$this->session->userdata('school_name');?>" />
	<meta itemprop="description" content="<?=$this->session->userdata('meta_description');?>" />
	<meta itemprop="image" content="<?=base_url('media_library/images/'. $this->session->userdata('logo'));?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="icon" href="<?=base_url('media_library/images/'.$this->session->userdata('favicon'));?>">	
		<?=link_tag('views/themes/cosmo/css/font-awesome.min.css')?>
		<?=link_tag('assets/uc/style.css')?>
		<script type="text/javascript">
    		const _BASE_URL = '<?=base_url();?>';
		</script>
	 	<script src="<?=base_url('assets/uc/js/countdown.js');?>"></script>		
	 	<script src="<?=base_url('assets/uc/js/jquery-1.7.2.min.js');?>"></script>	
	 	<script src="<?=base_url('assets/uc/js/script.js');?>"></script>			
</head>
<body>
		<section id="twd-container">
			<h1 class="head">Coming Soon</h1>
			<div id="count-down-container"></div>
			
			<script type="text/javascript">
			var currentyear=new Date().getFullYear()
			var target_date=new cdtime("count-down-container", "October 30, "+currentyear+" 0:0:00")
			target_date.displaycountdown("days", displayCountDown)
			</script>
			
			<div id="twd-form">
				<h3>Kunjungi Media Sosial Kami</h3>


						<ul class="social-network social-circle">
							<li><a href="<?=$this->session->userdata('facebook')?>" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                     <li><a href="<?=$this->session->userdata('twitter')?>" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                     <li><a href="<?=$this->session->userdata('google_plus')?>" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                     <li><a href="<?=$this->session->userdata('linked_in')?>" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>					 
                     <li><a href="<?=$this->session->userdata('youtube')?>" title="Youtube"><i class="fa fa-youtube"></i></a></li>
                     <li><a href="<?=$this->session->userdata('instagram')?>" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                     <li><a href="<?=site_url('feed')?>"><i class="fa fa-rss"></i></a></li>
                 	</ul>
	
			<div id="success">Thank you!</div>
			</div>
		</section>
</body>
</html>