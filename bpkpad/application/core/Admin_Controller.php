<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

	/**
	 * Primary key
	 * @var string
	 */
	protected $pk;

	/**
	 * Table
	 * @var string
	 */
	protected $table;

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();

		// Restrict
		$this->auth->restrict();
		
		// Check privileges Users
		if (!in_array($this->uri->segment(1), $this->session->userdata('user_privileges'))) {
			redirect(base_url());
		}

		// $this->output->enable_profiler();
	}

	/**
	 * deleted data | SET is_deleted to true
	 */
	public function delete() {
		$response = [];
		$ids = explode(',', $this->input->post($this->pk));
		if (count($ids) > 0) {
			if($this->model->delete($ids, $this->table)) {
				$response = [
		        	'action' => 'delete',
					'type' => 'success',
					'message' => 'deleted',
					'id' => $ids
				];
			} else {
				$response = [
					'action' => 'delete',
					'type' => 'error',
					'message' => 'not_deleted'
				];
			}
		} else {
			$response = [
				'action' => 'delete',
				'type' => 'warning',
				'message' => 'not_selected'
			];
			
		}

		$this->output
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
		exit;
	}

	/**
	 * Restored data | SET is_deleted to false
	 */
	public function restore() {
		$response = [];
		$ids = explode(',', $this->input->post($this->pk));
		if (count($ids) > 0) {
			if($this->model->restore($ids, $this->table)) {
				$response = [
		        	'action' => 'restore',
					'type' => 'success',
					'message' => 'restored',					
					'id' => $ids
				];
			} else {
				$response = [
					'action' => 'restore',
					'type' => 'error',
					'message' => 'not_deleted'
				];
			}
		} else {
			$response = [
				'action' => 'restore',
				'type' => 'warning',
				'message' => 'not_restored'
			];
		}

		$this->output
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
		exit;
	}
}