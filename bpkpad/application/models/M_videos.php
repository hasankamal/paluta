<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_videos extends CI_Model {

	/**
	 * Primary key
	 * @var string
	 */
	public static $pk = 'id';

	/**
	 * Table
	 * @var string
	 */
	public static $table = 'posts';

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get data for pagination
	 * @param string
	 * @param int
	 * @param int
	 * @return Query
	 */
	public function get_where($keyword, $limit = 0, $offset = 0, $sort_field = '', $sort_type = 'ASC') {
		$this->db->select('id, post_title, post_content, is_deleted');
		$this->db->where('post_type', 'video');
		$this->db->group_start();
		$this->db->like('post_title', $keyword);
		$this->db->group_end();
		if ($sort_field != '') {
			$this->db->order_by($sort_field, $sort_type);
		}
		if ($limit > 0) {
			$this->db->limit($limit, $offset);
		}
		return $this->db->get(self::$table);
	}

	/**
	 * Get Total row for pagination
	 * @param string
	 * @return int
	 */
	public function total_rows($keyword) {
		return $this->db
			->where('post_type', 'video')
			->group_start()
			->like('post_title', $keyword)
			->group_end()			
			->count_all_results(self::$table);
	}

	/**
	 * Get Recent Videos
	 * @return string
	 */
	public function get_recent_video($limit) {
		return $this->db
			->select('post_content')
			->where('post_type', 'video')
			->where('is_deleted', 'false')
			->limit($limit)
			->get(self::$table);
	}

	/**
	 * Get All Videos
	 * @return string
	 */
	public function get_videos() {
		return $this->db
			->select('id, post_title, post_content')
			->where('post_type', 'video')
			->where('is_deleted', 'false')
			->order_by('created_at', 'DESC')
			->get(self::$table);
	}
}