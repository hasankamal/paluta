<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get Data
	 * @return Query
	 */
	public function widget_box() {
		$query = $this->db->query("
			SELECT (SELECT COUNT(*) FROM comments x1 WHERE x1.comment_type='messages') AS messages
			, (SELECT COUNT(*) FROM comments x1 WHERE x1.comment_type='posts') AS comments
			, (SELECT COUNT(*) FROM posts x1 WHERE x1.post_type='post') AS posts
			, (SELECT COUNT(*) FROM posts x1 WHERE x1.post_type='page') AS pages
			, (SELECT COUNT(*) FROM post_categories) AS categories
			, (SELECT COUNT(*) FROM tags) AS tags
			, (SELECT COUNT(*) FROM links) AS links
			, (SELECT COUNT(*) FROM quotes) AS quotes;
		");
		return $query->row();
	}
}