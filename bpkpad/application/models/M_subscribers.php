<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_subscribers extends CI_Model {

	/**
	 * Primary key
	 * @var string
	 */
	public static $pk = 'id';

	/**
	 * Table
	 * @var string
	 */
	public static $table = 'subscribers';

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get data for pagination
	 * @param string
	 * @param int
	 * @param int
	 * @return Query
	 */
	public function get_where($keyword, $limit = 0, $offset = 0, $sort_field = '', $sort_type = 'ASC') {
		$this->db->select('id, email, is_active');
		$this->db->like('email', $keyword);
		if ($sort_field != '') {
			$this->db->order_by($sort_field, $sort_type);
		}
		if ($limit > 0) {
			$this->db->limit($limit, $offset);
		}
		return $this->db->get(self::$table);
	}

	/**
	 * Get All Data
	 * @return Query
	 */
	public function get_all() {
		return $this->db
			->select('id, email, is_active')
			->get(self::$table);
	}

	/**
	 * Get Total row for pagination
	 * @param string
	 * @return int
	 */
	public function total_rows($keyword) {
		return $this->db
			->like('email', $keyword)
			->count_all_results(self::$table);
	}

	/**
	 * Save Subscriber
	 * @param string
	 * @return bool
	 */
	public function save($email) {
		$count = $this->db
			->where('email', $email)
			->count_all_results(self::$table);
		if ($count === 0) {
			return $this->model->insert(self::$table, ['email' => $email, 'is_active' => 'true']);
		}
		return false;
	}
}