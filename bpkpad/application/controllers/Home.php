<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Public_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Index
	 */
	public function index() {
		$this->vars['title'] = $this->session->userdata('website') . ' | ' . $this->session->userdata('tagline');
		$this->vars['content'] = 'themes/'.theme_folder().'/home';
		$this->load->view('themes/'.theme_folder().'/index', $this->vars);
	}
}