<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Constructor
	 * @access  public
	 */
	public function __construct() {
		parent::__construct();
		if ($this->auth->is_logged_in())
			redirect('dashboard');
	}

	/**
	 * Index
	 * @access  public
	 */
	public function index() {
		$this->vars['title'] = 'Login Bpkpad';
		$this->vars['can_logged_in'] = $this->auth->check_login_attempts($_SERVER['REMOTE_ADDR']);
		$this->vars['login_info'] = $this->vars['can_logged_in'] ? 'Enter your username and password to log on' : 'The login page has been blocked for 30 minutes';
		$this->load->view('users/login', $this->vars);
	}

	/**
	 * process
	 * @access  public
	 */
	public function process() {
		$response = [];
		if ($this->validation()) {
			$user_name = $this->input->post('username', TRUE);
			$user_password = $this->input->post('password', TRUE);
			$ip_address = get_ip_address();
			$logged_in = $this->auth->logged_in($user_name, $user_password, $ip_address) ? 'success' : 'error';
			$response['type'] = $logged_in;
			$response['message'] = $logged_in == 'success' ? 'logged_in' : 'not_logged_in';
			$response['can_logged_in'] = $this->auth->check_login_attempts($_SERVER['REMOTE_ADDR']);
		} else {
			$response['type'] = 'error';
			$response['message'] = validation_errors();
			$response['can_logged_in'] = TRUE;
		}

		$this->output
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
		exit;
	}

	/**
	 * Validations Form
	 * @access  public
	 * @return Bool
	 */
	private function validation() {
		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('username', 'Username', 'trim|required');
		$val->set_rules('password', 'Password', 'trim|required');
		$val->set_error_delimiters('<div>&sdot; ', '</div>');
		return $val->run();
	}
}