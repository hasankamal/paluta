<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Readmore extends Public_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Readmore
	 */
	public function index() {
		$this->load->helper(['text', 'form']);
		$id = (int) $this->uri->segment(2);
		if ($id && $id != 0 && ctype_digit((string) $id)) {
			$this->load->helper(['captcha', 'string']);
			$this->load->model(['m_posts', 'm_pages', 'm_post_comments']);
			$this->m_posts->increase_viewer($id);
			$this->vars['query'] = $this->model->RowObject('posts', 'id', $id);
			$this->vars['post_type'] = 'post';
			if ($this->vars['query']->post_type === 'page') {
				$this->vars['post_type'] = 'page';
			}
			$this->vars['post_author'] = $this->model->RowObject('users', 'id', $this->vars['query']->post_author)->user_full_name;
			$this->vars['content'] = 'themes/'.theme_folder().'/single-post';
			$this->vars['captcha'] = $this->model->set_captcha();
			$this->load->view('themes/'.theme_folder().'/index', $this->vars);
		} else {
			show_404();
		}
	}
}