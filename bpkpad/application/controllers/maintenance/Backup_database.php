<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Backup_database extends Admin_Controller {

	/**
	 * Constructor
	 */
   public function __construct() {
      parent::__construct();
      if ($this->session->userdata('user_type') !== 'super_user')
      	redirect(base_url());
   }

   /**
	 * Backup Database
	 */
	public function index() {
		$this->load->dbutil();
		$prefs = [
			'format'   => 'zip',
			'filename' => 'backup-database-on-'.date("Y-m-d H-i-s").'.sql'
		];
		$backup =& $this->dbutil->backup($prefs); 
		$file_name = 'backup-database-on-'. date("Y-m-d-H-i-s") .'.zip';
		$this->zip->download($file_name);
	}
}