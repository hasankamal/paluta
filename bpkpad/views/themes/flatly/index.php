<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>

<html lang="en">
	<head>
		<title><?=$this->session->userdata('school_name')?></title>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="keywords" content="<?=$this->session->userdata('meta_keywords');?>"/>
		<meta name="description" content="<?=$this->session->userdata('meta_description');?>"/>
		<meta name="subject" content="Situs Bpkpad Kabupaten Padang Lawas Utara">
		<meta name="copyright" content="<?=$this->session->userdata('school_name')?>">
		<meta name="language" content="Indonesia">
		<meta name="robots" content="index,follow" />
		<meta name="revised" content="Sunday, July 18th, 2010, 5:15 pm" />
		<meta name="Classification" content="Kabupaten Padang Lawas Utara">
		<meta name="author" content="M. Sahidin, msahidin22@gmail.com">
		<meta name="designer" content="M. Sahidin, msahidin22@gmail.com">
		<meta name="reply-to" content="msahidin22@gmail.com">
		<meta name="owner" content="M. Sahidin">
		<meta name="url" content="https://bpkpad.padanglawasutarakab.go.id/">
		<meta name="identifier-URL" content="https://bpkpad.padanglawasutarakab.go.id/">
		<meta name="category" content="Goverment">
		<meta name="coverage" content="Worldwide">
		<meta name="distribution" content="Global">
		<meta name="rating" content="General">
		<meta name="revisit-after" content="7 days">
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Copyright" content="<?=$this->session->userdata('school_name');?>" />
		<meta http-equiv="imagetoolbar" content="no" />
		<meta name="revisit-after" content="7" />
		<meta name="webcrawlers" content="all" />
		<meta name="rating" content="general" />
		<meta name="spiders" content="all" />
		<meta itemprop="name" content="<?=$this->session->userdata('school_name');?>" />
		<meta itemprop="description" content="<?=$this->session->userdata('meta_description');?>" />
		<meta itemprop="image" content="<?=base_url('media_library/images/'. $this->session->userdata('logo'));?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="csrf-token" content="<?=$this->session->userdata('csrf_token')?>">
		<link rel="icon" href="<?=base_url('media_library/images/'.$this->session->userdata('favicon'));?>">
		<?=link_tag('views/themes/flatly/css/bootstrap.css')?>
		<?=link_tag('views/themes/flatly/css/font-awesome.min.css')?>
		<?=link_tag('views/themes/flatly/css/sm-core-css.css')?>
		<?=link_tag('views/themes/flatly/css/jquery.smartmenus.bootstrap.css')?>
		<?=link_tag('assets/css/magnific-popup.css');?>
		<?=link_tag('assets/css/toastr.css');?>
		<?=link_tag('assets/css/jssocials.css');?>
		<?=link_tag('assets/css/jssocials-theme-flat.css');?>
		<?=link_tag('assets/css/bootstrap-datepicker.css');?>		
		<?=link_tag('views/themes/flatly/css/style.css')?>
		<script type="text/javascript">
    		const _BASE_URL = '<?=base_url();?>';
		</script>

	 	<script src="<?=base_url('assets/js/frontend.min.js');?>"></script>		
		<script src="<?=base_url('assets/js/pop-up.js');?>"></script>
		<script src="<?=base_url('assets/js/gpr-widget-kominfo.min.js');?>"></script>

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
	</head>
	<body>
		<!-- Header -->
		<div class="container" style="background: #fff;box-shadow: 0 0 10px black; border-radius: 5px;">		
		<div class="header">

	<div class="row">
					<div id="background-head" class="col-md-7 col-xs-12">
						<div class="header-logo">
							<a href="<?=base_url()?>"><img src="<?=base_url('media_library/images/'.$this->session->userdata('logo'))?>" alt="" class="img-responsive"></a>
						</div>
					</div>
					<div id="slider-head" class="col-md-5 col-xs-12">
						<?php $query = get_image_sliders(); if ($query->num_rows() > 0) { ?>
							<!-- Image Slider -->
							<div id="image-slideshow" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner" role="listbox">
											<?php $idx = 0; foreach($query->result() as $row) { ?>
											<div class="item <?=$idx==0?'active':''?>">
												<img src="<?=base_url('media_library/image_sliders/'.$row->image);?>" alt="..." style="    width: 970px; height: 200px;">
												
											</div>
											<?php $idx++; } ?>
										</div>
										
							</div>
							<!-- End Image Slider -->
							<?php } ?>
					</div>
				</div>

		</div>
	<!-- End Header -->
	<!-- Middle Nav -->	
	<nav class="navbar-inverse" style="padding: 0 10px 0 10px;">
			<div class="row">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="row">
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="<?=base_url()?>"> BERANDA</a></li>
						<?php 
						foreach ($menus as $menu) {
							echo '<li>';
							$sub_nav = recursive_list($menu['child']);
							$url = base_url() . $menu['menu_url'];
							if ($menu['menu_type'] == 'links') {
								$url = $menu['menu_url'];
							}
							echo anchor($url, strtoupper($menu['menu_title']). ($sub_nav ? ' <span class="caret"></span>':''), 'target="'.$menu['menu_target'].'"');
							if ($sub_nav) {
								echo '<ul class="dropdown-menu">';
								echo recursive_list($menu['child']);
								echo '</ul>';
							}
							echo '</li>';
						}?>
					</ul>					
				</div>
				</div>
			</div>
			</div>
		</nav>
		<?php $query = get_quotes(); if ($query->num_rows() > 0) { ?>
		<div class="text-slider">
			<div class="container">
				<ul id="marquee" class="marquee">
					<?php foreach($query->result() as $row) { ?>
						<li><?=$row->quote?>. <strong><i><font color="yellow"><?=$row->quote_by?></font></i></strong></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<?php } ?>

		<div class="content">
			<!-- Content -->
			<div class="row">
				<!-- Right Content -->
				<?php $this->load->view($content)?>
			</div>
	 	</div> <!-- /container -->

	 	<!-- FOOTER -->
		<div class="footer">
	 		<div class="container">
		  		<div class="row">
					<div class="col-md-6">
						<h4>HUBUNGI KAMI</h4>
                  <p>
                  <?=$this->session->userdata('street_address')?><br>
                  Email : <?=$this->session->userdata('email')?><br>
                  Telp : <?=$this->session->userdata('phone')?><br>
                  Fax : <?=$this->session->userdata('fax')?>
                  </p>
					</div>					

					<div class="col-md-6">
						<h4>KATEGORI</h4>
					 	<ul>
					 		<?php 
                     $query = get_post_categories(); 
                     if ($query->num_rows() > 0) {
                         foreach($query->result() as $row) {
                             echo '<li>'.anchor(site_url('category/'.$row->slug), $row->category, ['title' => $row->description]).'</li>';
                         }
                     }
                     ?>
						</ul>
					</div>
					</div>
		  		</div>
	 		</div>
		</div>
		<!-- END FOOTER -->
		
		<!-- COPYRIGHT -->
		<div class="copyright">
			<p><?=copyright(2017, base_url(), $this->session->userdata('school_name'))?></p>
		</div>
		<!-- END COPYRIGHT -->
		
		<!-- Back to Top -->
		<a href="javascript:" id="return-to-top"><i class="fa fa-angle-double-up"></i></a>
	 	<script>
		  // Scroll Top
			$(window).scroll(function() {
				if ($(this).scrollTop() >= 50) {
					$('#return-to-top').fadeIn(200);
			 	} else {
					$('#return-to-top').fadeOut(200);
			 	}
			});
			$('#return-to-top').click(function() {
				$('body,html').animate({
					scrollTop : 0
			 	}, 500);
			});
		</script>
		</div>
  	</body>
</html>