<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Sidebar -->
<div class="col-xs-12 col-md-3">

	<?php $query = get_links(); if ($query->num_rows() > 0) { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
	  			<h3 class="panel-title">TUGAS POKOK & FUNGSI</h3>
			</div>
			<div class="list-group">
				<?php foreach($query->result() as $row) { ?>
				<a href="<?=$row->url;?>" title="<?=$row->title;?>" target="<?=$row->target;?>" class="list-group-item"><?=$row->title;?></a>
	        <?php } ?>
			</div>
		</div>
	<?php } ?>

	<?php $query = get_post_categories(); if ($query->num_rows() > 0) { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
	  			<h3 class="panel-title">KATEGORI</h3>
			</div>
			<div class="list-group">
				<?php foreach($query->result() as $row) { ?>
				<a href="<?=site_url('category/'.$row->slug);?>" title="<?=$row->description;?>" class="list-group-item"><?=$row->category;?></a>
	        <?php } ?>
			</div>
		</div>
	<?php } ?>
	
	<?php $query = get_active_question(); if ($query) { ?>
	<div class="panel panel-default">
		<div class="panel-heading">
  			<h3 class="panel-title">JAJAK PENDAPAT</h3>
		</div>
		<div class="panel-body">
			<p><?=$query->question?></p>
			<?php $options = get_answers($query->id); foreach($options->result() as $option) { ?>
			<div class="radio">
			  <label>
			    <input type="radio" name="answer_id" id="answer_id" id="answer_id_<?=$option->id?>" value="<?=$option->id?>">
			    <?=$option->answer?>
			  </label>
			</div>
			<?php } ?>
			<div class="btn-group">
				<button type="submit" onclick="polling(); return false;" class="btn btn-success btn-sm"><i class="fa fa-send"></i> SUBMIT</button>
				<a href="<?=site_url('hasil-jajak-pendapat')?>" class="btn btn-sm btn-warning"><i class="fa fa-bar-chart"></i> LIHAT HASIL</a>		
			</div>			
		</div>
	</div>
	<?php } ?>

	<?php $query = get_banners(); if ($query->num_rows() > 0) { ?>
		<?php foreach($query->result() as $row) { ?>
			<a href="<?=$row->url?>" title="<?=$row->title?>" class="thumbnail"> <img src="<?=base_url('media_library/banners/'.$row->image)?>" style="width: 100%; display: block;"></a>
		<?php } ?>
	<?php } ?>
  	<div class="form-group has-feedback">
		<input onkeydown="if (event.keyCode == 13) { subscriber(); return false; }" type="text" class="form-control" id="subscriber" placeholder="Berlangganan" autocomplete="off">
		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	</div>
		<div id="gpr-kominfo-widget-container"></div>
&nbsp;	
</div>