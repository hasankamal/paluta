<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
		<head>
		<title><?=$this->session->userdata('school_name')?></title>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="keywords" content="<?=$this->session->userdata('meta_keywords');?>"/>
		<meta name="description" content="<?=$this->session->userdata('meta_description');?>"/>
		<meta name="subject" content="Situs Bappeda Kabupaten Padang Lawas Utara">
		<meta name="copyright" content="<?=$this->session->userdata('website_name')?>">
		<meta name="language" content="Indonesia">
		<meta name="robots" content="index,follow" />
		<meta name="revised" content="Sunday, September 25mon, 2017, 6:24 am" />
		<meta name="Classification" content="Diskominfo Kabupaten Padang Lawas Utara">
		<meta name="author" content="M. Sahidin, msahidin22@gmail.com">
		<meta name="designer" content="M. Sahidin, msahidin22@gmail.com">
		<meta name="reply-to" content="msahidin22@gmail.com">
		<meta name="owner" content="M. Sahidin">
		<meta name="url" content="https://bappeda.padanglawasutarakab.go.id/">
		<meta name="identifier-URL" content="https://bappeda.padanglawasutarakab.go.id/">
		<meta name="category" content="Admission, Education">
		<meta name="coverage" content="Worldwide">
		<meta name="distribution" content="Global">
		<meta name="rating" content="General">
		<meta name="revisit-after" content="7 days">
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Copyright" content="<?=$this->session->userdata('website_name');?>" />
		<meta http-equiv="imagetoolbar" content="no" />
		<meta name="revisit-after" content="7" />
		<meta name="webcrawlers" content="all" />
		<meta name="rating" content="general" />
		<meta name="spiders" content="all" />
		<meta itemprop="name" content="<?=$this->session->userdata('website_name');?>" />
		<meta itemprop="description" content="<?=$this->session->userdata('meta_description');?>" />
		<meta itemprop="image" content="<?=base_url('media_library/images/'. $this->session->userdata('logo'));?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="csrf-token" content="<?=$this->session->userdata('csrf_token')?>">
		<link rel="icon" href="<?=base_url('media_library/images/'.$this->session->userdata('favicon'));?>">
		<?=link_tag('views/themes/plato/css/bootstrap.css')?>
		<?=link_tag('views/themes/plato/css/font-awesome.min.css')?>
		<?=link_tag('views/themes/plato/css/sm-core-css.css')?>
		<?=link_tag('views/themes/plato/css/jquery.smartmenus.bootstrap.css')?>
		<?=link_tag('assets/css/magnific-popup.css');?>
		<?=link_tag('assets/css/toastr.css');?>
		<?=link_tag('assets/css/jssocials.css');?>
		<?=link_tag('assets/css/jssocials-theme-flat.css');?>
		<?=link_tag('assets/css/bootstrap-datepicker.css');?>
		<?=link_tag('views/themes/plato/css/style.css')?>
		<?=link_tag('views/themes/plato/css/beige.css')?>
		<script type="text/javascript">
    		const _BASE_URL = '<?=base_url();?>';
		</script>
		<script src="<?=base_url('assets/js/frontend.min.js');?>"></script>
	</head>
	<body>
		<!-- Top Nav -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
	<div class="col-md-3 col-xs-9" style="padding-left: 0;">
						<div id="header-logo">
							<a href="<?=base_url()?>"><img src="<?=base_url('media_library/images/'.$this->session->userdata('logo'))?>" alt="" class="img-responsive"></a>
						</div>
</div>	
	<div class="col-md-9 col-xs-3"  style="padding: 0 0;">
<div id="desktop">
					<ul class="social-network social-circle pull-left">
                     <li><a href="<?=$this->session->userdata('facebook')?>" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                     <li><a href="<?=$this->session->userdata('twitter')?>" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                     <li><a href="<?=$this->session->userdata('google_plus')?>" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                     <li><a href="<?=$this->session->userdata('youtube')?>" title="Youtube"><i class="fa fa-youtube"></i></a></li>
                     <li><a href="<?=$this->session->userdata('instagram')?>" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                     <li><a href="<?=site_url('feed')?>"><i class="fa fa-rss"></i></a></li>
                 	</ul>
</div>

<form id="desktop" style="margin-top: 15px;" class="navbar-form navbar-left" action="<?=site_url('hasil-pencarian')?>" method="POST">
			        <div class="form-group">
			          <input type="text" class="form-control input-sm" placeholder="Pencarian" name="keyword">
			        </div>
			        <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-search"></i></button>
</form>	
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

</div>
				
				
		<div class="col-md-9 col-xs-12">				
				<div id="navbar" class="navbar-collapse collapse">					
		<!-- js nav -->
<form id="mobile" style="margin-top: 10px;" class="navbar-form navbar-left" action="<?=site_url('hasil-pencarian')?>" method="POST">
			        <div class="form-group">
			          <input type="text" class="form-control input-sm" placeholder="Pencarian" name="keyword">
			        </div>
			        <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-search"></i></button>
</form>
</div>	
</div>		
			</div>
		</nav>
		
		<!-- Header -->
<div class="header">	
<div id="slider">
<div id="slidenya"><!-- Image Slider -->
						<?php $query = get_image_sliders(); if ($query->num_rows() > 0) { ?>						
							<div id="image-slider" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner" role="listbox">
											<?php $idx = 0; foreach($query->result() as $row) { ?>
											<div class="item <?=$idx==0?'active':''?>">
												<img src="<?=base_url('media_library/image_sliders/'.$row->image);?>">
												
											</div>
											<?php $idx++; } ?>
										</div>
										
							</div>
							
							<?php } ?>
</div><!-- End Image Slider -->
</div><!-- Slider -->		
</div>
			</div>
		</div>
		<!-- End Header -->

		<?php $query = get_quotes(); if ($query->num_rows() > 0) { ?>
		<div class="text-slider">
			<div class="container">
				<ul id="marquee" class="marquee">
					<?php foreach($query->result() as $row) { ?>
						<li><?=$row->quote?>. <strong><i><font color="yellow"><?=$row->quote_by?></font></i></strong></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<?php } ?>

		<div class="container">
			<!-- Content -->
			<div class="row">
				<!-- Right Content -->
				<?php $this->load->view($content)?>
			</div>
	 	</div> <!-- /container -->

	 	<!-- FOOTER -->
		<div class="footer">
	 		<div class="container">
		  		<div class="row">
					<div class="col-md-3">
						<h4>TAUTAN</h4>
				 		<ul>
			 				<?php 
                    	$links = get_links(); 
                    	if ($links->num_rows() > 0) {
                     	foreach($links->result() as $row) {
                        	echo '<li>'. anchor($row->url, $row->title, ['target' => $row->target]) . '</li>';
                     	}  
                    	}
                    	?>
						</ul>
					</div>
					<div class="col-md-3">
						<h4>KATEGORI</h4>
					 	<ul>
					 		<?php 
                     $query = get_post_categories(); 
                     if ($query->num_rows() > 0) {
                         foreach($query->result() as $row) {
                             echo '<li>'.anchor(site_url('category/'.$row->slug), $row->category, ['title' => $row->description]).'</li>';
                         }
                     }
                     ?>
						</ul>	
											<h4>TAGS</h4>
					 	<ul>
				 			<?php 
                 		$query = get_tags();
                    	if ($query->num_rows() > 0) {
                     	foreach ($query->result() as $row) {
                     		echo '<li>'.anchor(site_url('tag/'.$row->slug), $row->tag).'</li>';
                     	}
                    	}
                    	?>
						</ul>	
					</div>
						<div class="col-md-3">
	<?php $query = get_active_question(); if ($query) { ?>
	<div class="panel panel-default">
		<div class="panel-heading">
  			<h3 class="panel-title">JAJAK PENDAPAT</h3>
		</div>
		<div class="panel-body">
			<p><h5><?=$query->question?></h5></p>
			<?php $options = get_answers($query->id); foreach($options->result() as $option) { ?>
			<div class="radio">
			  <label>
			    <input type="radio" name="answer_id" id="answer_id" id="answer_id_<?=$option->id?>" value="<?=$option->id?>">
			    <?=$option->answer?>
			  </label>
			</div>
			<?php } ?>
			<div class="btn-group">
				<button type="submit" onclick="polling(); return false;" class="btn btn-success btn-sm"><i class="fa fa-send"></i> SUBMIT</button>
				<a href="<?=site_url('hasil-jajak-pendapat')?>" class="btn btn-sm btn-warning"><i class="fa fa-bar-chart"></i> LIHAT HASIL</a>		
			</div>			
		</div>
	</div>
	<?php } ?>				
						<h4>HUBUNGI KAMI</h4>
                  <p>
                  <?=$this->session->userdata('street_address')?><br>
                  Email : <?=$this->session->userdata('email')?><br>
                  Telp : <?=$this->session->userdata('phone')?><br>
                  Fax : <?=$this->session->userdata('fax')?>
                  </p>
					</div>
						<div class="col-md-3">				
		<div class="fb-page" data-href="https://www.facebook.com/kabpadanglawasutara" data-tabs="timeline, messages, share," data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kabpadanglawasutara" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kabpadanglawasutara">Pemkab Paluta</a></blockquote></div>				
					</div>

		  		</div>
	 		</div>
		</div>
		<!-- END FOOTER -->
		
		<!-- COPYRIGHT -->
		<div class="copyright">
			<p><?=copyright(2017, base_url(), $this->session->userdata('school_name'))?></p>
		  	<p>Developed oleh <a href="https://padanglawasutarakab.go.id/">Kab. Padang Lawas Utara</a></p>
		</div>
		<!-- END COPYRIGHT -->
		
		<!-- Back to Top -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10&appId=640211006121837";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
		<a href="javascript:" id="return-to-top"><i class="fa fa-angle-double-up"></i></a>
	 	<script>
		  // Scroll Top
			$(window).scroll(function() {
				if ($(this).scrollTop() >= 50) {
					$('#return-to-top').fadeIn(200);
			 	} else {
					$('#return-to-top').fadeOut(200);
			 	}
			});
			$('#return-to-top').click(function() {
				$('body,html').animate({
					scrollTop : 0
			 	}, 500);
			});
		</script>
  	</body>
</html>