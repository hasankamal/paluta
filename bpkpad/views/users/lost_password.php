<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$title;?></title>
		<?=link_tag('assets/css/bootstrap.css');?>
		<?=link_tag('assets/css/font-awesome.css');?>
		<?=link_tag('assets/css/toastr.css');?>
		<?=link_tag('assets/css/login-form-elements.css');?>
		<?=link_tag('assets/css/login-style.css');?>
		<link rel="icon" href="<?=base_url('assets/img/favicon.png');?>">
		<script type="text/javascript">
			const _BASE_URL = '<?=base_url();?>', _CURRENT_URL = '<?=current_url();?>';
		</script>
		<script src="<?=base_url('assets/js/jquery.js');?>"></script>
		<script src="<?=base_url('assets/js/bootstrap.min.js');?>"></script>
		<script src="<?=base_url('assets/plugins/toastr/toastr.js');?>"></script>
		<script src="<?=base_url('assets/js/global.js')?>" type="text/javascript"></script>
	</head>
	<body>
		<div class="top-content">
			<div class="inner-bg">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3 form-box">
							<div class="form-top">
								<div class="form-top-left">
									<h2>Lost Password</h2>
									<p>Enter your email to reset your account</p>
								</div>
								<div class="form-top-right">
									<i class="fa fa-key"></i>
								</div>
							</div>
							<div class="form-bottom">
								<form role="form" action="" method="post" class="login-form">
									<div class="form-group">
										<label class="sr-only" for="email">Email</label>
										<input type="text" name="email" placeholder="Email..." class="email form-control" id="email">
									</div>
									<button type="submit" class="btn"><i class="fa fa-sign-out"></i> Get New Password</button>
								</form>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<p class="login-footer">
							<a href="<?=site_url('login');?>">Log In</a><br>
							Copyright &copy; 2014 - <?=date('Y');?> padanglawasutarakab.go.id All Rights Reserved<br>
							Powered by <a href="https://padanglawasutarakab.go.id">padanglawasutarakab.go.id</a> &sdot; Back to <a href="<?=base_url()?>"><?=$this->session->userdata('school_name')?></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>