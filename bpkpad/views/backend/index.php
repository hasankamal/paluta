<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$this->session->userdata('school_name')?></title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="<?=$this->session->userdata('meta_keywords');?>"/>
	<meta name="description" content="<?=$this->session->userdata('meta_description');?>"/>
	<meta name="subject" content="Situs Bpkpad Kabupaten Padang Lawas Utara">
	<meta name="copyright" content="<?=$this->session->userdata('school_name')?>">
	<meta name="language" content="Indonesia">
	<meta name="robots" content="index,follow" />
	<meta name="revised" content="Sunday, July 18th, 2010, 5:15 pm" />
	<meta name="Classification" content="Kabupaten Padang Lawas Utara">
	<meta name="author" content="M Sahidin, msahidin22@gmail.com">
	<meta name="designer" content="M Sahidin, msahidin22@gmail.com">
	<meta name="reply-to" content="msahidin22@gmail.com">
	<meta name="owner" content="m Sahidin">
	<meta name="url" content="https://bpkpad.padanglawasutarakab.go.id">
	<meta name="identifier-URL" content="https://bpkpad.padanglawasutarakab.go.id">
	<meta name="category" content="Kabupaten Padang Lawas Utara">
	<meta name="coverage" content="Worldwide">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Copyright" content="<?=$this->session->userdata('school_name');?>" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta name="revisit-after" content="7" />
	<meta name="webcrawlers" content="all" />
	<meta name="rating" content="general" />
	<meta name="spiders" content="all" />
	<meta itemprop="name" content="<?=$this->session->userdata('school_name');?>" />
	<meta itemprop="description" content="<?=$this->session->userdata('meta_description');?>" />
	<meta itemprop="image" content="<?=base_url('media_library/images/'. $this->session->userdata('logo'));?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="icon" href="<?=base_url('media_library/images/'.$this->session->userdata('favicon'));?>">
	<?=link_tag('assets/css/bootstrap.css');?>
	<?=link_tag('assets/css/font-awesome.css');?>
	<?=link_tag('assets/css/toastr.css');?>
	<?=link_tag('assets/css/bootstrap-datepicker.css');?>
	<?=link_tag('assets/css/AdminLTE.css');?>
	<?=link_tag('assets/css/jquery.timepicker.css');?>
	<?=link_tag('assets/css/backend-style.css');?>
	<?=link_tag('assets/css/magnific-popup.css');?>
	<?=link_tag('assets/css/bootstrap-colorpicker.css');?>
	<?=link_tag('assets/css/jquery-clockpicker.css');?>
	<?=link_tag('assets/css/select2.css');?>
	<?=link_tag('assets/css/jquery.tagsinput.min.css');?>
	<?=link_tag('assets/css/jquery-ui.css');?>
	<script type="text/javascript">
		const _BASE_URL = '<?=base_url();?>', _CURRENT_URL = '<?=current_url();?>';
	</script>
	<script type="text/javascript" src="<?=base_url('assets/dist/js/backend.min.js');?>"></script>
</head>
<!-- sidebar-collapse -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<header class="main-header">
		<a href="#" class="logo">
			<span class="logo-mini"><i class="fa fa-cogs"></i></span>
			<span class="logo-lg"><b>ADMIN</b> BPKPAD</span>
		</a>
	 	<nav class="navbar navbar-static-top">
			<a onclick="sidebar_collapse(); return false;" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<div class="collapse navbar-collapse pull-right" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<?php if ($this->session->userdata('user_type') === 'super_user' || $this->session->userdata('user_type') === 'administrator') { ?>
					<li <?=isset($user_profile) ? 'class="active"' : '';?>><a href="<?=site_url('profile');?>"><i class="fa fa-edit"></i> Ubah Profil</a></li>
					<?php } ?>
					<li <?=isset($change_password) ? 'class="active"' : '';?>><a href="<?=site_url('change_password');?>"><i class="fa fa-key"></i> Ubah Kata Sandi</a></li>
					<li class="logout"><a href="<?=site_url('logout');?>"><i class="fa fa-power-off"></i> Keluar</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<aside class="main-sidebar">
		<?php $this->load->view('backend/sidebar');?>
	</aside>
	<div class="content-wrapper">
		<?php $this->load->view($content);?>
	</div>
	<div class="modal" id="cms-info">
		<div class="modal-dialog">
			
		</div>
	</div>

	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<p>Powered by <a href="<?=$this->config->item('website');?>" target="_blank"><?=$this->config->item('apps');?> <?=$this->config->item('version');?></a></p>
		</div>
		<p>Copyright &copy; <?=date('Y');?> Bpkpad Kabupaten Padang Lawas Utara. All rights reserved.</p>
	</footer>
	<div class="control-sidebar-bg"></div>
</div>
<a href="javascript:" id="return-to-top"><i class="fa fa-angle-double-up"></i></a>
</body>
<script type="text/javascript">
	// Scroll Top
	$(window).scroll(function() {
		if ($(this).scrollTop() >= 50) {
			$('#return-to-top').fadeIn(200);
	 	} else {
			$('#return-to-top').fadeOut(200);
	 	}
	});

	// Return to Top
	$('#return-to-top').click(function() {
		$('body,html').animate({
			scrollTop : 0
	 	}, 500);
	});
</script>
</html>
