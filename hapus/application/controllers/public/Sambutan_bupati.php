<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sambutan_bupati extends Public_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Index
	 */
	public function index() {
		$this->vars['page_title'] = 'Sambutan Bupati';
		$this->vars['content'] = 'themes/'.theme_folder().'/sambutan-bupati';
		$this->load->view('themes/'.theme_folder().'/index', $this->vars);
	}
}