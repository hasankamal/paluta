<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'third_party/tcpdf/tcpdf.php';

class PDF extends tcpdf {
    public function Header(){}
    public function Footer() {
    	$content = '<table width="100%" border="0" cellpadding="3" cellspacing="0" style="border-top:1px solid #000000;">';
    	$content .= '<tbody>';
    	$content .= '<tr>';
    	$content .= '<td align="left" width="60%">Simpanlah lembar pendaftaran ini sebagai bukti pendaftaran Anda.</td>';
    	$content .= '<td align="right" width="40%">Dicetak tanggal '.indo_date(date('Y-m-d')).' pukul '.date('H:i:s').'</td>';
    	$content .= '</tr>';
    	$content .= '</tbody>';
    	$content .= '</table>';
    	$this->setY(-1);
    	$this->writeHTML($content, true, false, true, false, 'L');
    }
}

$PDF = new PDF('P', 'Cm', 'F4', true, 'UTF-8', false);
$PDF->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$PDF->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$PDF->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$PDF->SetAutoPageBreak(TRUE, 1);
$PDF->setImageScale(PDF_IMAGE_SCALE_RATIO);
$school_level = $this->session->userdata('school_level');
if ($school_level == 5) {
	$PDF->SetTitle('FORMULIR PENERIMAAN MAHASISWA BARU TAHUN '.$this->session->userdata('admission_year'));	
} else {
	$PDF->SetTitle('FORMULIR PENERIMAAN PESERTA DIDIK BARU TAHUN '.$this->session->userdata('admission_year'));	
}
$PDF->SetAuthor('http://sekolahku.web.id');
$PDF->SetSubject($this->session->userdata('school_name'));
$PDF->SetKeywords($this->session->userdata('school_name'));
$PDF->SetCreator('http://sekolahku.web.id');
$PDF->SetMargins(2, 1, 2, true);
$PDF->AddPage();
$PDF->SetFont('freesans', '', 10);
$content = '
	<!DOCTYPE html>
	<html>
	<head>
		<title></title>
	</head>
	<body>
';
$content .= '	
	<table width="100%" border="0" cellpadding="3" cellspacing="0" style="border-bottom:1px solid #000000;">
		<tbody>
			<tr>
				<td width="20%" align="center">
					<img src="'.base_url('media_library/images/'.$this->session->userdata('logo')).'">
				</td>
				<td width="80%" valign="top" align="left">
					<h1>'.strtoupper($this->session->userdata('school_name')).'</h1>
					<p>'.$this->session->userdata('street_address').'<br>
					Telp : '.$this->session->userdata('phone'). ' &sdot; Fax : ' .$this->session->userdata('fax').' &sdot; Kode Pos : ' .$this->session->userdata('postal_code').'<br>
					Email : '.$this->session->userdata('email'). ' &sdot; Website : ' .str_replace(['http://', 'https://', 'www.'], '', $this->session->userdata('website')).'</p>
				</td>
			</tr>
		</tbody>
	</table>';
$content .= '
	<h3>'.strtoupper($title).'</h3>
';
$content .= '
	<table width="100%" border="0" cellpadding="3" cellspacing="0" style="border-bottom:1px solid #a5a5a5;">
		<tbody>
		<tr>
			<td colspan="3" align="left"><h3>Registrasi '.($school_level == 5 ? 'Mahasiswa' : 'Peserta Didik').'</h3></td>
		</tr>
		<tr>
			<td width="35%" align="right">Jenis Pendaftaran</td>
			<td width="5%" align="center">:</td>
			<td width="60%" align="left">'.$query['is_transfer'].'</td>
		</tr>
		<tr>
			<td align="right">Nomor Pendaftaran</td>
			<td align="center">:</td>
			<td align="left">'.$query['registration_number'].'</td>
		</tr>
		<tr>
			<td align="right">Tanggal Pendaftaran</td>
			<td align="center">:</td>
			<td align="left">'.$query['created_at'].'</td>
		</tr>';
		if ($school_level == 4 || $school_level == 5) {
			$content .= '
			<tr>
				<td align="right">Pilihan I (Satu)</td>
				<td align="center">:</td>
				<td align="left">'.$query['first_choice'].'</td>
			</tr>
			<tr>
				<td align="right">Pilihan II (Dua)</td>
				<td align="center">:</td>
				<td align="left">'.$query['second_choice'].'</td>
			</tr>';
		}		
		$content .= '
		<tr>
			<td colspan="3"></td>
		</tr>
		</tbody>
	</table>';
$content .= '
	<table width="100%" cellpadding="3" cellspacing="0" style="border-bottom:1px solid #a5a5a5;">
		<tbody>
		<tr >
			<td colspan="3" align="left"><h3>Data Pribadi</h3></td>
		</tr>
		<tr>
			<td width="35%" align="right">Nama Lengkap</td>
			<td width="5%" align="center">:</td>
			<td width="60%" align="left">'.$query['full_name'].'</td>
		</tr>
		<tr>
			<td align="right">Jenis Kelamin</td>
			<td align="center">:</td>
			<td align="left">'.$query['gender'].'</td>
		</tr>';
		if ($school_level == 2 || $school_level == 3 || $school_level == 4) {
			$content .= '
			<tr>
				<td align="right">NISN</td>
				<td align="center">:</td>
				<td align="left">'.$query['nisn'].'</td>
			</tr>
			<tr>
				<td align="right">NIK</td>
				<td align="center">:</td>
				<td align="left">'.$query['nik'].'</td>
			</tr>
			';
		}
		$content .= '			
		<tr>
			<td align="right">Tempat Lahir</td>
			<td align="center">:</td>
			<td align="left">'.$query['birth_place'].'</td>
		</tr>
		<tr>
			<td align="right">Tanggal Lahir</td>
			<td align="center">:</td>
			<td align="left">'.indo_date($query['birth_date']).'</td>
		</tr>
		<tr>
			<td align="right">Agama</td>
			<td align="center">:</td>
			<td align="left">'.$query['religion'].'</td>
		</tr>
		<tr>
			<td align="right">Kebutuhan Khusus</td>
			<td align="center">:</td>
			<td align="left">'.$query['special_needs'].'</td>
		</tr>
		<tr>
			<td colspan="3"></td>
		</tr>
		</tbody>
	</table>';
$content .= '
	<table width="100%" cellpadding="3" cellspacing="0">
		<tbody>
		<tr>
			<td colspan="3" align="left"><h3>Alamat</h3></td>
		</tr>
		<tr>
			<td width="35%" align="right">Alamat Jalan</td>
			<td width="5%" align="center">:</td>
			<td width="60%" align="left">'.$query['street_address'].'</td>
		</tr>
		<tr>
			<td align="right">RT</td>
			<td align="center">:</td>
			<td align="left">'.$query['rt'].'</td>
		</tr>
		<tr>
			<td align="right">RW</td>
			<td align="center">:</td>
			<td align="left">'.$query['rw'].'</td>
		</tr>
		<tr>
			<td align="right">Dusun</td>
			<td align="center">:</td>
			<td align="left">'.$query['sub_village'].'</td>
		</tr>
		<tr>
			<td align="right">Kelurahan / Desa</td>
			<td align="center">:</td>
			<td align="left">'.$query['village'].'</td>
		</tr>
		<tr>
			<td align="right">Kecamatan</td>
			<td align="center">:</td>
			<td align="left">'.$query['sub_district'].'</td>
		</tr>
		<tr>
			<td align="right">Kabupaten</td>
			<td align="center">:</td>
			<td align="left">'.$query['district'].'</td>
		</tr>
		<tr>
			<td align="right">Kode Pos</td>
			<td align="center">:</td>
			<td align="left">'.$query['postal_code'].'</td>
		</tr>
		<tr>
			<td align="right">Email</td>
			<td align="center">:</td>
			<td align="left">'.$query['email'].'</td>
		</tr>
		<tr>
			<td colspan="3"></td>
		</tr>
		</tbody>
	</table>';
$content .= '
	<table width="100%" cellpadding="3" cellspacing="0" border="0">
		<tbody>
		<tr>
			<td align="left">Saya yang bertandatangan dibawah ini menyatakan bahwa data yang tertera diatas adalah yang sebenarnya.</td>
		</tr>
		<tr><td></td></tr>
		<tr align="left">
			<td align="right">'. $query['district'] .', '.indo_date(date('Y-m-d')).'</td>
		</tr>
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr><td align="right">'.$query['full_name'].'</td></tr>
		</tbody>
	</table>';
$content .='</body></html>';
$PDF->writeHTML($content, true, false, true, false, 'C');
$PDF->Output(FCPATH.'media_library/students/'.$file_name, 'F');