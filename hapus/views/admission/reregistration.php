<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<script type="text/javascript">
	$( document ).ready( function() {
		/* Registrants */
		$('#registrants').tagsInput({
			'width': 'auto',
			'height':'300px',
			'autocomplete_url': _BASE_URL + 'admission/reregistration/autocomplete',
		   'interactive':true,
		   'defaultText':'Add New Registrants',
		   'delimiter': [', '],
		   'removeWithBackspace' : true,
		   'minChars' : '3',
		   'maxChars' : 0,
		   'placeholderColor' : '#666666'
		});

		/* Submit */
		$('#submit').on('click', function(e) {
			e.preventDefault();
			var values = {
				registrants: $('#registrants').val()
			};
			$.post( _BASE_URL + 'admission/reregistration/save', values, function(response) {
				var res = H.stringToJSON(response);
				H.growl(res.type, H.message(res.message));
				if (res.type == 'success') {
					$('#registrants').importTags('');
				}
			});
		});
	});
</script>
<section class="content-header">
   <h1><i class="fa fa-search text-red"></i> <?=ucwords(strtolower($title));?></h1>
 </section>
 <section class="content">
 	<div class="callout callout-success">
		<h4>Petunjuk Singkat</h4>
		<p>Menu <strong>Verifikasi Calon <?=($this->session->userdata('school_level') == 5 ? 'Mahasiswa Baru' : 'Peserta Didik Baru')?></strong> ini digunakan untuk menandai calon <?=($this->session->userdata('school_level') == 5 ? 'Mahasiswa' : 'Peserta Didik')?> yang sudah melakukan pendaftaran ulang secara langsung ke <?=($this->session->userdata('school_level') == 5 ? 'kampus' : 'sekolah')?>. Dimana hanya <strong>Calon <?=($this->session->userdata('school_level') == 5 ? 'Mahasiswa Baru' : 'Peserta Didik Baru')?> yang melakukan verifikasi pendaftaran</strong> saja yang akan dipanggil ketika melakukan proses seleksi Penerimaan <?=($this->session->userdata('school_level') == 5 ? 'Mahasiswa Baru' : 'Peserta Didik Baru')?>.</p>
	</div>
 	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal">
				<div class="box-body">
               <div class="form-group">
                  <label for="registrants" class="col-sm-3 control-label">Masukan Nomor Pendaftaran Calon <?=($this->session->userdata('school_level') == 5 ? 'Mahasiswa Baru' : 'Peserta Didik Baru')?></label>
                  <div class="col-sm-9">
                    	<textarea name="registrants" id="registrants" class="form-control tm-input-info tm-tag-mini" rows="10" placeholder="Masukan Nomor Pendaftaran Disini..."></textarea>
                  </div>
               </div>               
               <div class="btn-group col-md-9 col-md-offset-3">
                  <button id="submit" class="btn btn-primary submit"><i class="fa fa-save"></i> SIMPAN</button>
               </div>
      	  	</div>
         </form>
		</div>
	</div>
 </section>