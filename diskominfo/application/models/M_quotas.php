<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_quotas extends CI_Model {

	/**
	 * Primary key
	 * @var string
	 */
	public static $pk = 'id';

	/**
	 * Table
	 * @var string
	 */
	public static $table = 'registration_quotas';

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get data for pagination
	 * @param string
	 * @param int
	 * @param int
	 * @return Query
	 */
	public function get_where($keyword, $limit = 0, $offset = 0, $sort_field = '', $sort_type = 'ASC') {
		$this->db->select('x1.id, x1.year, x2.major, x1.quota, x1.is_deleted');
		$this->db->join('majors x2', 'x1.major_id = x2.id', 'left');
		$this->db->like('x1.year', $keyword);
		$this->db->or_like('x2.major', $keyword);
		$this->db->or_like('x1.quota', $keyword);
		if ($sort_field != '') {
			$this->db->order_by('x1.'.$sort_field, $sort_type);
		}
		if ($limit > 0) {
			$this->db->limit($limit, $offset);
		}
		return $this->db->get(self::$table.' x1');
	}

	/**
	 * Get All Data
	 * @return Query
	 */
	public function get_all() {
		return $this->db
			->select('x1.id, x1.year, x2.major, x1.quota, x1.is_deleted')
			->join('major x2', 'x1.major_id = x2.id', 'left')
			->get('registration_quotas x1');
	}

	/**
	 * Get Total row for pagination
	 * @param string
	 * @return int
	 */
	public function total_rows($keyword) {
		return $this->db
			->join('majors x2', 'x1.major_id = x2.id', 'left')
			->like('x1.year', $keyword)
			->or_like('x2.major', $keyword)
			->or_like('x1.quota', $keyword)
			->count_all_results('registration_quotas x1');
	}
}