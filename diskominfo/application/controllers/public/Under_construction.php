<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Under_construction extends CI_Controller {

	/**
	 * Constructor
	 * @access  public
	 */
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('site_maintenance') == 'false') {
			redirect('home', 'refresh');
		}
	}

	/**
	 * Index
	 * @access  public
	 */
	public function index() {
		$this->load->view('frontend/under_construction');
	}
}