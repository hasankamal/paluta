<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Triger {
	
	/**
     * The CodeIgniter super object
     *
     * @var object
     * @access private
     */
    private $CI;

	/**
     * Class constructor
     */
    public function __construct() {
		$this->CI = &get_instance();
	}

	/**
     * Set Session Here
     */
	public function index() {
		$this->CI->load->model(['m_settings', 'm_themes']);
		$settings = [];
		if (! $this->CI->auth->is_logged_in()) {
			$settings = $this->CI->m_settings->get_setting_values('public');
		} else {
			$settings = $this->CI->m_settings->get_setting_values($this->CI->session->userdata('user_type'));
		}
		if (count($settings) > 0) {
			$session_data = [];
			foreach($settings as $key => $value) {
				if ($value) {
					$session_data[$key] = $value;
				}
			}
		}

		// Get Active Theme
		$session_data['theme'] = $this->CI->m_themes->get_active_themes();
		
		// Set Session Data
		$this->CI->session->set_userdata($session_data);
	}
}