<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div id="col-atas" class="col-md-12">
<div class="col-lg-9 col-md-12">
	<?php $query = get_recent_posts(10); if ($query->num_rows() > 0) { ?>
	<!-- Image Slider -->
	<div class="row slider">
		<div class="col-md-12">
			<div id="image-slider" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
					<?php $idx = 0; foreach($query->result() as $row) { ?>
					<div class="item <?=$idx==0?'active':''?>">
						<img src="<?=base_url('media_library/posts/large/'.$row->post_image)?>" alt="...">
						<div class="carousel-caption">
							<?=$row->post_title;?>
						</div>
					</div>
					<?php $idx++; } ?>
				</div>
				<a class="left carousel-control" href="#image-slider" role="button" data-slide="prev">
		        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		        <span class="sr-only">Previous</span>
		      </a>
		      <a class="right carousel-control" href="#image-slider" role="button" data-slide="next">
		        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		        <span class="sr-only">Next</span>
		      </a>
			</div>
		</div>
	</div>
	<!-- End Image Slider -->
	<?php } ?>
</div>	
<div class="col-lg-3 col-md-12">
	<?php if ($this->uri->segment(1) != 'sambutan-kepala-dinas') { ?>
	<div class="thumbnail">
		<img src="<?=base_url('media_library/images/').$this->session->userdata('headmaster_photo');?>" alt="..." style="width: 100%">
 		<div class="caption">
			<h4><?=$this->session->userdata('headmaster')?></h4>
			<p><?=word_limiter(strip_tags(get_welcome()), 20);?></p>
	 	</div>
  	</div>
  	<?php } ?>
</div>
</div>

<div class="col-md-12" style="padding:0;">
<div class="tabs-featured" style="text-align: center;">
			<ul class="nav nav-tabs">
			  <li class="col-md-2 col-xs-12"><a href="#col1" data-toggle="tab" aria-expanded="true"><i class="pull-left"></i>BERITA TERBARU</a></li>
			  <li class="col-md-2 col-xs-12"><a href="#col2" data-toggle="tab" aria-expanded="false"><i class="pull-left"></i>BERITA POPULER</a></li>
			  <li class="col-md-2 col-xs-12"><a href="#col3" data-toggle="tab" aria-expanded="false"><i class="pull-left"></i>KATEGORI BERITA</a></li>
			  <li class="col-md-2 col-xs-12"><a href="#col4" data-toggle="tab" aria-expanded="false"><i class="pull-left"></i>GALERI FOTO</a></li>
			  <li class="col-md-2 col-xs-12"><a href="#col5" data-toggle="tab" aria-expanded="false"><i class="pull-left"></i>GALERI VIDEO</a></li>
			  <li class="col-md-2 col-xs-12"><a href="#col6" data-toggle="tab" aria-expanded="false"><i class="pull-left"></i>POJOK IT</a></li>		  
			</ul>
			</div>
			
<div class="tab-content">			
<div class="tab-pane fade active in" id="col1"><!-- BERITA TERBARU -->
					<div id="tab-content" class="col-sm-12 col-md-12" style="text-align: left;">
	<!-- Recent Posts -->
	<?php 
	$query = get_recent_posts(5); if ($query->num_rows() > 0) { 
		$posts = [];
		foreach ($query->result() as $post) {
			array_push($posts, $post);
		}
	?>
	<!-- Title -->
	<ol class="breadcrumb post-header">
		<li>BERITA TERBARU</li>
	</ol>	
	<div class="row">
		<div class="col-md-6">
			<?php if (count(array_slice($posts, 0, 1)) > 0) { ?>
					<?php foreach(array_slice($posts, 0, 1) as $row) { ?>
					<div class="thumbnail no-border">
						<img src="<?=base_url('media_library/posts/medium/'.$row->post_image)?>" style="width: 100%; display: block;">
						<div class="caption">
							<h4><a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>"><?=$row->post_title?></a></h4>
							<p class="by-author"><?=day_name(date('N', strtotime($row->created_at)))?>, <?=indo_date($row->created_at)?> | oleh <?=$row->post_author?></p>
							<p align="justify"><?=substr(strip_tags($row->post_content), 0, 165)?></p>
							<p>
								<a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>" class="btn btn-success btn-sm" role="button">Selengkapnya <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
		<div class="col-md-6">
			<?php if (count(array_slice($posts, 1)) > 0) { ?>
			<ul class="media-list main-list">
				<?php foreach(array_slice($posts, 1) as $row) { ?>
				<li class="media">
					<a class="pull-left" href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>">
						<img class="media-object" src="<?=base_url('media_library/posts/thumbnail/'.$row->post_image)?>" alt="...">
					</a>
					<div class="media-body">
						<h4><a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>"><?=$row->post_title?></a></h4>
						<p class="by-author"><?=day_name(date('N', strtotime($row->created_at)))?>, <?=indo_date($row->created_at)?></p>
					</div>
				</li>
				<?php } ?>
			</ul>
			<?php } ?>
		</div>
	</div>	
	<?php } ?>
	<!-- End Recent Posts -->
					</div>
				</div>
					
<div class="tab-pane fade" id="col2"><!-- BERITA POPULER -->
					<div id="tab-content" class="col-sm-12 col-md-12" style="text-align: left;">
	<!-- Popular Posts -->
	<?php 
	$query = get_popular_posts(5); if ($query->num_rows() > 0) { 
		$posts = [];
		foreach ($query->result() as $post) {
			array_push($posts, $post);
		}
	?>
	<!-- Title -->
	<ol class="breadcrumb post-header">
		<li>BERITA POPULER</li>
	</ol>		
	<div class="row">
		<div class="col-md-6">
			<?php if (count(array_slice($posts, 0, 1)) > 0) { ?>
					<?php foreach(array_slice($posts, 0, 1) as $row) { ?>
					<div class="thumbnail no-border">
						<img src="<?=base_url('media_library/posts/medium/'.$row->post_image)?>" style="width: 100%; display: block;">
						<div class="caption">
							<h4><a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>"><?=$row->post_title?></a></h4>
							<p class="by-author"><?=day_name(date('N', strtotime($row->created_at)))?>, <?=indo_date($row->created_at)?> | oleh <?=$row->post_author?></p>
							<p align="justify"><?=substr(strip_tags($row->post_content), 0, 165)?></p>
							<p>
								<a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>" class="btn btn-success btn-sm" role="button">Selengkapnya <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
		<div class="col-md-6">
			<?php if (count(array_slice($posts, 1)) > 0) { ?>
			<ul class="media-list main-list">
				<?php foreach(array_slice($posts, 1) as $row) { ?>
				<li class="media">
					<a class="pull-left" href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>">
						<img class="media-object" src="<?=base_url('media_library/posts/thumbnail/'.$row->post_image)?>" alt="...">
					</a>
					<div class="media-body">
						<h4><a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>"><?=$row->post_title?></a></h4>
						<p class="by-author"><?=day_name(date('N', strtotime($row->created_at)))?>, <?=indo_date($row->created_at)?></p>
					</div>
				</li>
				<?php } ?>
			</ul>
			<?php } ?>
		</div>
	</div>	
	<?php } ?>
	<!-- End Popular Posts -->
					</div>					
</div>

<div class="tab-pane fade" id="col3"><!-- KATEGORI BERITA -->
					<div id="tab-content" class="col-sm-12 col-md-12" style="text-align: left;">
	<?php $query = get_post_categories(5); if ($query->num_rows() > 0) { ?>
		<?php foreach($query->result() as $row) {
			$posts = get_post_category($row->id, 5); 
			if ($posts->num_rows() > 0) {
				$arr_posts = [];
          	foreach ($posts->result() as $post) {
            	array_push($arr_posts, $post);
       		}
			?>
				<ol class="breadcrumb post-header">
					<li><?=strtoupper($row->category)?></li>
					<span class="pull-right"><a href="<?=site_url('category/'.$row->slug)?>"><i class="fa fa-search"></i></a></span>
				</ol>
				<div class="row">
					<div class="col-md-6">
						<?php if (count(array_slice($arr_posts, 0, 1)) > 0) { ?>
								<?php foreach(array_slice($arr_posts, 0, 1) as $post) { ?>
								<div class="thumbnail no-border">
									<img src="<?=base_url('media_library/posts/medium/'.$post->post_image)?>" style="width: 100%; display: block;">
									<div class="caption">
										<h4><a href="<?=site_url('read/'.$post->id.'/'.$post->post_slug)?>"><?=$post->post_title?></a></h4>
										<p class="by-author"><?=day_name(date('N', strtotime($post->created_at)))?>, <?=indo_date($post->created_at)?> | oleh <?=$post->post_author?></p>
										<p align="justify"><?=substr(strip_tags($post->post_content), 0, 165)?></p>
										<p>
											<a href="<?=site_url('read/'.$post->id.'/'.$post->post_slug)?>" class="btn btn-success btn-sm" role="button">Selengkapnya <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
										</p>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<div class="col-md-6">
						<?php if (count(array_slice($arr_posts, 1)) > 0) { ?>
						<ul class="media-list main-list">
							<?php foreach(array_slice($arr_posts, 1) as $post) { ?>
							<li class="media">
								<a class="pull-left" href="<?=site_url('read/'.$post->id.'/'.$post->post_slug)?>">
									<img class="media-object" src="<?=base_url('media_library/posts/thumbnail/'.$post->post_image)?>" alt="...">
								</a>
								<div class="media-body">
									<h4><a href="<?=site_url('read/'.$post->id.'/'.$post->post_slug)?>"><?=$post->post_title?></a></h4>
									<p class="by-author"><?=day_name(date('N', strtotime($post->created_at)))?>, <?=indo_date($post->created_at)?></p>
								</div>
							</li>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
				</div>	
			<?php } ?>
		<?php } ?>
	<?php } ?>
</div>					
</div>
			 
<div class="tab-pane fade" id="col4"><!-- GALERI FOTO -->
					<div id="tab-content" class="col-sm-12 col-md-12">
	<?php $query = get_albums(2); if ($query->num_rows() > 0) { ?>
	<!-- Gallery Photo -->
	<ol class="breadcrumb post-header">
		<li><i class="fa fa-camera"></i> PHOTO TERBARU</li>
		<span class="pull-right"><a href="<?=site_url('gallery-photo')?>"><i class="fa fa-search"></i></a></span>
	</ol>
	<div class="row">
		<?php foreach($query->result() as $row) { ?>
		<div class="col-md-6 col-xs-12">
			<div class="thumbnail">
				<img style="cursor: pointer; width: 100%; height: 250px;" onclick="preview(<?=$row->id?>)" src="<?=base_url('media_library/albums/'.$row->album_cover)?>">
				<div class="caption">
					<h4><?=$row->album_title?></h4>
					<p><?=$row->album_description?></p>
					<button onclick="preview(<?=$row->id?>)" class="btn btn-success btn-sm"><i class="fa fa-search"></i></button>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<?php } ?>
					</div>					
				 </div>
				 
	<div class="tab-pane fade" id="col5"><!-- GALERI VIDEO -->
					<div id="tab-content" class="col-sm-12 col-md-12">
	<?php $query = get_recent_video(2); if ($query->num_rows() > 0) { ?>
	<!-- Gallery Video -->
	<ol class="breadcrumb post-header">
		<li><i class="fa fa-film"></i> VIDEO TERBARU</li>
		<span class="pull-right"><a href="<?=site_url('gallery-video')?>"><i class="fa fa-search"></i></a></span>
	</ol>
	<div class="row">
		<?php foreach($query->result() as $row) { ?>
		<div class="col-md-6 col-xs-12">
			<div class="thumbnail" style="width: 100%; display: block;">
				<?=$row->post_content?>	
			</div>
		</div>
		<?php } ?>	
	</div>
	<?php } ?>
					</div>					
				 </div>
			 
	<div class="tab-pane fade" id="col6"><!-- POJOK IT -->
					<div id="tab-content" class="col-sm-12 col-md-12">
	<ol class="breadcrumb post-header">
		<li><i class="fa fa-search"></i> POJOK IT</li>
		<span class="pull-right"><a href="<?=site_url('gallery-video')?>"><i class="fa fa-search"></i></a></span>
	</ol>
					</div>					
				 </div>			 

</div><!-- tab content -->
</div><!-- colt -->
<div id="col" class="col-md-12">
</div>
<div class="col-xs-12 col-md-12" style="padding:0;">
<div class="col-xs-12 col-md-3">
	<?php $query = get_post_categories(); if ($query->num_rows() > 0) { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
	  			<h3 class="panel-title">KATEGORI</h3>
			</div>
			<div class="list-group">
				<?php foreach($query->result() as $row) { ?>
				<a href="<?=site_url('category/'.$row->slug);?>" title="<?=$row->description;?>" class="list-group-item"><?=$row->category;?></a>
	        <?php } ?>
			</div>
		</div>
	<?php } ?>
</div>

<div class="col-xs-12 col-md-3">
	<?php $query = get_links(); if ($query->num_rows() > 0) { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
	  			<h3 class="panel-title">TAUTAN</h3>
			</div>
			<div class="list-group">
				<?php foreach($query->result() as $row) { ?>
				<a href="<?=$row->url;?>" title="<?=$row->title;?>" target="<?=$row->target;?>" class="list-group-item"><?=$row->title;?></a>
	        <?php } ?>
			</div>
		</div>
	<?php } ?>
</div>

<div class="col-xs-12 col-md-3">		
	<?php $query = get_archive_year(); if ($query->num_rows() > 0) { ?>
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<?php $idx = 0; foreach($query->result() as $row) { ?>
			<div class="panel panel-default">
	  			<div class="panel-heading" role="tab" id="heading_<?=$row->year?>">
		 			<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#archive_<?=$row->year?>" aria-expanded="true" aria-controls="archive_<?=$row->year?>">ARSIP <?=$row->year?></a>
		 			</h4>
	  			</div>
	  			<div id="archive_<?=$row->year?>" class="panel-collapse collapse <?=$idx==0?'in':''?>" role="tabpanel" aria-labelledby="heading_<?=$row->year?>">
		 			<div class="list-group">
		 				<?php $archives = get_archives($row->year); if ($archives->num_rows() > 0) { ?>
							<?php foreach($archives->result() as $archive) { ?>
								<a href="<?=site_url('archives/'.$row->year.'/'.$archive->code)?>" class="list-group-item"><?=bulan($archive->code)?> (<?=$archive->count?>)</a>
							<?php } ?>
						<?php } ?>
		 			</div>
	  			</div>
			</div>
			<?php $idx++; } ?>
	</div>
	<?php } ?>
</div>
<div class="col-xs-12 col-md-3">
	<?php $query = get_active_question(); if ($query) { ?>
	<div class="panel panel-default">
		<div class="panel-heading">
  			<h3 class="panel-title">JAJAK PENDAPAT</h3>
		</div>
		<div class="panel-body">
			<p><?=$query->question?></p>
			<?php $options = get_answers($query->id); foreach($options->result() as $option) { ?>
			<div class="radio">
			  <label>
			    <input type="radio" name="answer_id" id="answer_id" id="answer_id_<?=$option->id?>" value="<?=$option->id?>">
			    <?=$option->answer?>
			  </label>
			</div>
			<?php } ?>
			<div class="btn-group">
				<button type="submit" onclick="polling(); return false;" class="btn btn-success btn-sm"><i class="fa fa-send"></i> SUBMIT</button>
				<a href="<?=site_url('hasil-jajak-pendapat')?>" class="btn btn-sm btn-warning"><i class="fa fa-bar-chart"></i> LIHAT HASIL</a>		
			</div>			
		</div>
	</div>
	<?php } ?>
	</div>
</div>