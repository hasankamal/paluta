<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="col-xs-12 col-md-9">
	<div class="thumbnail no-border">
		<?php if ($post_type == 'post' && file_exists('./media_library/posts/large/'.$query->post_image)) { ?>
		<img src="<?=base_url('media_library/posts/large/'.$query->post_image)?>" style="width: 100%; display: block;">
		<?php } ?>
		<div class="caption">
			<h3><?=$query->post_title?></h3>
			<p class="by-author">
				<?=day_name(date('N', strtotime($query->created_at)))?>, 
				<?=indo_date(substr($query->created_at, 0, 10))?> 
				~ Oleh <?=$post_author?>
				~ Dilihat <?=$query->post_counter?> Kali
			</p>
			<?=$query->post_content?>
			<div id="share1"></div>
			<script>
			$("#share1").jsSocials({
				shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
			});
			</script>
			<?php 
        if ($query->post_tags) {
            $post_tags = explode(',', $query->post_tags);
            foreach ($post_tags as $tag) {
            	echo '<a style="margin-right:3px;" href="'.site_url('tag/'.url_title(strtolower(trim($tag)))).'">';
            	echo '<span class="label label-success">';
            	echo '<i class="fa fa-tags"></i> '.ucwords(strtolower(trim($tag)));
            	echo '</span>';
            	echo '</a>';
            }
        }
        ?>
		</div>
	</div>

<h3><span>Komentar Anda</span></h3>
<div class="fb-comments" data-width="100%" data-numposts="5"></div>	
	<div id="disqus_thread"></div>
		<script>

		/**
		*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
		*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
		/*
		var disqus_config = function () {
		this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
		this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
		};
		*/
		(function() { // DON'T EDIT BELOW THIS LINE
		var d = document, s = d.createElement('script');
		s.src = 'https://paluta.disqus.com/embed.js';
		s.setAttribute('data-timestamp', +new Date());
		(d.head || d.body).appendChild(s);
		})();
		</script>
		<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a>
	</noscript>	
	
	<?php 
	if ($query->post_type == 'post') {
		$posts = get_related_posts($query->post_categories, $this->uri->segment(2), 10); 
		if ($posts->num_rows() > 0) {
			$arr_posts = [];
			foreach ($posts->result() as $post) {
				array_push($arr_posts, $post);
			}
			?>
			<ol class="breadcrumb post-header">
				<li><i class="fa fa-sign-out"></i> TULISAN TERKAIT</li>
			</ol>
			<?php $idx = 2; $rows = $posts->num_rows(); foreach($posts->result() as $row) { ?>
				<?=($idx % 2 == 0) ? '<div class="row">':''?>
					<div class="col-md-6">
						<ul class="media-list main-list">
							<li class="media">
								<a class="pull-left" href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>">
									<img class="media-object" src="<?=base_url('media_library/posts/thumbnail/'.$row->post_image)?>" alt="...">
								</a>
								<div class="media-body">
									<h4><a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>"><?=$row->post_title?></a></h4>
									<p class="by-author"><?=day_name(date('N', strtotime($row->created_at)))?>, <?=indo_date($row->created_at)?></p>
								</div>
							</li>
						</ul>
					</div>
				<?=(($idx % 2 == 1) || ($rows+1 == $idx)) ? '</div>':''?>
			<?php $idx++; } ?>
		<?php } ?>
	<?php } ?>
</div>
<?php $this->load->view('themes/plato/sidebar')?>