<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Sidebar -->
<div class="col-xs-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
	  			<h3 class="panel-title">KADIS KOMINFO</h3>
			</div>
			</div>
	<?php if ($this->uri->segment(1) != 'sambutan-kepala-dinas') { ?>
	<div class="thumbnail">
		<img src="<?=base_url('media_library/images/').$this->session->userdata('headmaster_photo');?>" alt="..." style="width: 100%">
 		<div class="caption">
			<h5><?=$this->session->userdata('headmaster')?></h5>
			<p><?=word_limiter(strip_tags(get_welcome()), 20);?></p>
			<p>
				<a href="<?=site_url('sambutan-kepala-dinas');?>" class="btn btn-success btn-sm" role="button">Selengkapnya <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
			</p>
	 	</div>
  	</div>
  	<?php } ?>

	<?php $query = get_archive_year(); if ($query->num_rows() > 0) { ?>
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<?php $idx = 0; foreach($query->result() as $row) { ?>
			<div class="panel panel-default">
	  			<div class="panel-heading" role="tab" id="heading_<?=$row->year?>">
		 			<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#archive_<?=$row->year?>" aria-expanded="true" aria-controls="archive_<?=$row->year?>">ARSIP <?=$row->year?></a>
		 			</h4>
	  			</div>
	  			<div id="archive_<?=$row->year?>" class="panel-collapse collapse <?=$idx==0?'in':''?>" role="tabpanel" aria-labelledby="heading_<?=$row->year?>">
		 			<div class="list-group">
		 				<?php $archives = get_archives($row->year); if ($archives->num_rows() > 0) { ?>
							<?php foreach($archives->result() as $archive) { ?>
								<a href="<?=site_url('archives/'.$row->year.'/'.$archive->code)?>" class="list-group-item"><?=bulan($archive->code)?> (<?=$archive->count?>)</a>
							<?php } ?>
						<?php } ?>
		 			</div>
	  			</div>
			</div>
			<?php $idx++; } ?>
	</div>
	<?php } ?>	
	
  	<div class="form-group has-feedback">
		<input onkeydown="if (event.keyCode == 13) { subscriber(); return false; }" type="text" class="form-control" id="subscriber" placeholder="Berlangganan" autocomplete="off">
		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	</div>
</div>