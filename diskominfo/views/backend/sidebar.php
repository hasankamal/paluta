<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="sidebar">
	<ul class="sidebar-menu">
		
		<?php if (in_array('dashboard', $this->session->userdata('user_privileges'))) { ?>
			<li class="<?=isset($dashboard) ? 'active':'';?>">
				<a href="<?=site_url('dashboard');?>">
					<i class="fa fa-dashboard"></i> <span>Beranda</span>
				</a>
			</li>
		<?php } ?>

		<li>
			<a href="<?=base_url();?>" target="_blank">
				<i class="fa fa-rocket"></i> <span>Lihat Situs</span>
			</a>
		</li>
		
		<!-- @namespace Employee Menu -->
		<?php if ($this->session->userdata('user_type') === 'employee' && in_array('employee_profile', $this->session->userdata('user_privileges'))) { ?>
			<li class="<?=isset($employee_profile) ? 'active':'';?>">
				<a href="<?=site_url('employee_profile');?>">
					<i class="fa fa-user"></i> <span>Profil</span>
				</a>
			</li>
		<?php } ?>

		<!-- @namespace Administrator Menu -->
		<?php if ($this->session->userdata('user_type') === 'super_user' || $this->session->userdata('user_type') === 'administrator') { ?>
			<?php if (in_array('blog', $this->session->userdata('user_privileges'))) { ?>
			<li class="treeview <?=isset($blog) ? 'active':'';?>">
				<a href="#">
					<i class="fa fa-edit"></i> <span>Berita & Informasi</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">				
					<li <?=isset($image_sliders) ? 'class="active"':'';?>><a href="<?=site_url('blog/image_sliders');?>"><i class="fa fa-picture-o"></i> Gambar Slide</a></li>
					<li <?=isset($messages) ? 'class="active"':'';?>><a href="<?=site_url('blog/messages');?>"><i class="fa fa-envelope-o"></i> Pesan Masuk</a></li>
					<li <?=isset($links) ? 'class="active"':'';?>><a href="<?=site_url('blog/links');?>"><i class="fa fa-link"></i> Tautan</a></li>	
					<li <?=isset($pages) ? 'class="active"':'';?>><a href="<?=site_url('blog/pages');?>"><i class="fa fa-file-o"></i>Halaman Informasi</a></li>
					<li <?=isset($posts) ? 'class="active"':'';?>>
						<a href="#"><i class="fa fa-file-text-o"></i> Berita <i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
							<li <?=isset($all_posts) ? 'class="active"':'';?>><a href="<?=site_url('blog/posts');?>"><i class="fa fa-navicon"></i> Semua Berita</a></li>
							<li <?=isset($post_create) ? 'class="active"':'';?>><a href="<?=site_url('blog/posts/create');?>"><i class="fa fa-pencil"></i> Tambah Baru</a></li>
							<li <?=isset($post_categories) ? 'class="active"':'';?>><a href="<?=site_url('blog/post_categories');?>"><i class="fa fa-list-ul"></i> Kategori Berita</a></li>
							<li <?=isset($post_comments) ? 'class="active"':'';?>><a href="<?=site_url('blog/post_comments');?>"><i class="fa fa-comments-o"></i> Komentar</a></li>
							<li <?=isset($tags) ? 'class="active"':'';?>><a href="<?=site_url('blog/tags');?>"><i class="fa fa-tags"></i> Tags</a></li>
						</ul>
					</li>
					<li <?=isset($quotes) ? 'class="active"':'';?>><a href="<?=site_url('blog/quotes');?>"><i class="fa fa-quote-right"></i> Pengumuman</a></li>
					<li <?=isset($welcome) ? 'class="active"':'';?>><a href="<?=site_url('blog/welcome');?>"><i class="fa fa-bullhorn"></i> Sambutan Kepala Dinas</a></li>
					<li <?=isset($subscribers) ? 'class="active"':'';?>><a href="<?=site_url('blog/subscribers');?>"><i class="fa fa-folder-open-o"></i> Subscribers</a></li>
				</ul>
			</li>
			<?php } ?>
		<?php } ?>

		<?php if ($this->session->userdata('user_type') === 'super_user' || $this->session->userdata('user_type') === 'administrator') { ?>
			<?php if (in_array('plugins', $this->session->userdata('user_privileges'))) { ?>
			<li class="treeview <?=isset($plugins) ? 'active':'';?>">
				<a href="#">
					<i class="fa fa-plug"></i> <span>Plugins</span> <i class="fa fa-angle-left pull-right"></i>
				</a>			
				<ul class="treeview-menu">
					<li <?=isset($banners) ? 'class="active"':'';?>><a href="<?=site_url('plugins/banners');?>"><i class="fa fa-newspaper-o"></i> Aplikasi Banner</a></li>
					<li <?=isset($pollings) ? 'class="active"':'';?>>
						<a href="#"><i class="fa fa-question-circle"></i> Jajak Pendapat <i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
							<li <?=isset($questions) ? 'class="active"':'';?>><a href="<?=site_url('plugins/questions');?>"><i class="fa fa-circle-thin"></i> Pertanyaan</a></li>
							<li <?=isset($answers) ? 'class="active"':'';?>><a href="<?=site_url('plugins/answers');?>"><i class="fa fa-circle-thin"></i> Jawaban</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<?php } ?>
		<?php } ?>
		
		<?php if ($this->session->userdata('user_type') === 'super_user' || $this->session->userdata('user_type') === 'administrator') { ?>
			<?php if (in_array('media', $this->session->userdata('user_privileges'))) { ?>
			<li class="treeview <?=isset($media) ? 'active':'';?>">
				<a href="#">
					<i class="fa fa-upload"></i> <span>Media</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li <?=isset($files) ? 'class="active"':'';?>><a href="<?=site_url('media/files');?>"><i class="fa fa-paperclip"></i> File</a></li>
					<li <?=isset($file_categories) ? 'class="active"':'';?>><a href="<?=site_url('media/file_categories');?>"><i class="fa fa-tasks"></i> Kategori File</a></li>
					<li <?=isset($albums) ? 'class="active"':'';?>><a href="<?=site_url('media/albums');?>"><i class="fa fa-camera"></i> Album Photo</a></li>
					<li <?=isset($videos) ? 'class="active"':'';?>><a href="<?=site_url('media/videos');?>"><i class="fa fa-film"></i> Video</a></li>
				</ul>
			</li>
			<?php } ?>
		<?php } ?>

		<?php if ($this->session->userdata('user_type') === 'super_user' || $this->session->userdata('user_type') === 'administrator') { ?>
			<?php if (in_array('appearance', $this->session->userdata('user_privileges'))) { ?>
			<li class="treeview <?=isset($appearance) ? 'active':'';?>">
				<a href="#">
					<i class="fa fa-paint-brush"></i> <span>Tampilan</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li <?=isset($menus) ? 'class="active"':'';?>><a href="<?=site_url('appearance/menus');?>"><i class="fa fa-mouse-pointer"></i> Menu</a></li>
					<li <?=isset($themes) ? 'class="active"':'';?>><a href="<?=site_url('appearance/themes');?>"><i class="fa fa-desktop"></i> Tema</a></li>
				</ul>
			</li>
			<?php } ?>
		<?php } ?>

		<?php if ($this->session->userdata('user_type') === 'super_user' || $this->session->userdata('user_type') === 'administrator') { ?>
			<?php if (in_array('acl', $this->session->userdata('user_privileges'))) { ?>
			<li class="treeview <?=isset($acl) ? 'active':'';?>">
				<a href="#">
					<i class="fa fa-cogs"></i> <span>Pengguna</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<?php if ($this->session->userdata('user_type') == 'super_user') { ?>
					<li <?=isset($administrator) ? 'class="active"':'';?>><a href="<?=site_url('acl/administrator');?>"><i class="fa fa-smile-o"></i> Administrator</a></li>
					<?php } ?>
					<li <?=isset($user_groups) ? 'class="active"':'';?>><a href="<?=site_url('acl/user_groups');?>"><i class="fa fa-tasks"></i> Grup Pengguna</a></li>
					<li <?=isset($user_privileges) ? 'class="active"':'';?>><a href="<?=site_url('acl/user_privileges');?>"><i class="fa fa-random"></i> Hak Akses</a></li>
				</ul>
			</li>
			<?php } ?>
		<?php } ?>

		<?php if ($this->session->userdata('user_type') === 'super_user' || $this->session->userdata('user_type') === 'administrator') { ?>
			<?php if (in_array('settings', $this->session->userdata('user_privileges'))) { ?>
			<li class="treeview <?=isset($settings) ? 'active':'';?>">
				<a href="#">
					<i class="fa fa-wrench"></i> <span>Pengaturan</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li <?=isset($discussion_settings) ? 'class="active"':'';?> style="display: none;"><a href="<?=site_url('settings/discussion');?>"><i class="fa fa-comments-o"></i> Diskusi</a></li>
					<li <?=isset($mail_server_settings) ? 'class="active"':'';?>><a href="<?=site_url('settings/mail_server');?>"><i class="fa fa-envelope"></i> Email Server</a></li>
					<li <?=isset($social_account_settings) ? 'class="active"':'';?>><a href="<?=site_url('settings/social_account');?>"><i class="fa fa-globe"></i> Jejaring Sosial</a></li>
					<li <?=isset($media_settings) ? 'class="active"':'';?> style="display: none;"><a href="<?=site_url('settings/media');?>"><i class="fa fa-folder-open-o"></i> Media</a></li>
					<li <?=isset($writing_settings) ? 'class="active"':'';?> style="display: none;"><a href="<?=site_url('settings/writing');?>"><i class="fa fa-pencil-square-o"></i> Menulis</a></li>
					<li <?=isset($reading_settings) ? 'class="active"':'';?> style="display: none;"><a href="<?=site_url('settings/reading');?>"><i class="fa fa-book"></i> Membaca</a></li>					
					<li <?=isset($school_profile_settings) ? 'class="active"':'';?>><a href="<?=site_url('settings/school_profile');?>"><i class="fa fa-home"></i> Profil <?=$this->session->userdata('school_level') == 5 ? 'Kampus' : 'Dinas'?></a></li>
					<li <?=isset($general_settings) ? 'class="active"':'';?>><a href="<?=site_url('settings/general');?>"><i class="fa fa-sign-out"></i> Umum</a></li>
				</ul>
			</li>
			<?php } ?>
		<?php } ?>
		
			<?php if ($this->session->userdata('user_type') === 'super_user' || $this->session->userdata('user_type') === 'administrator') { ?>
			<?php if (in_array('maintenance', $this->session->userdata('user_privileges'))) { ?>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-medkit"></i> <span>Pemeliharaan</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu" style="margin-bottom: 20px">
					<li><a href="<?=site_url('maintenance/backup_database');?>"><i class="fa fa-database"></i> Backup Database</a></li>
					<li><a href="<?=site_url('maintenance/backup_apps');?>"><i class="fa fa-download"></i> Backup Apps</a></li>
				</ul>
			</li>
			<?php } ?>
		<?php } ?>	
</ul>
<br>
 </section>