<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="bread">
	<div class="container">
		<div class="col-xs-12">
		<div class="bread-title gal-video"><i class="fa fa-film"></i> GALLERY VIDEO</div>
		</div>
	</div>
</div>
<div class="container">
		<div class="posting">
			<div class="col-xs-12 col-md-12 video-list">
				<!-- <ol class="breadcrumb post-header">
					<li><i class="fa fa-film"></i> GALLERY VIDEO</li>
				</ol> -->
				<?php $idx = 3; $rows = $query->num_rows(); foreach($query->result() as $row) { ?>
					<?=($idx % 3 == 0) ? '<div class="row">':''?>
						<div class="col-md-4 col-xs-12">
							<div class="thumb-video">
								<?=$row->post_content?>				
								<div class="caption">
									<h4><?=$row->post_title?></h4>
								</div>
							</div>
						</div>
					<?=(($idx % 3 == 2) || ($rows+2 == $idx)) ? '</div>':''?>
				<?php $idx++; } ?>
			</div>
		</div>
</div>