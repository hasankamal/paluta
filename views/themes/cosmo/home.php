<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!--sambutan -->
<section class="sambutan">
  
  <?php $query = get_image_sliders(); if ($query->num_rows() > 0) { ?>													
          <div id="image-slider" class="carousel slide" data-ride="carousel">										
            <div class="carousel-inner" role="listbox">											
              <?php $idx = 0; foreach($query->result() as $row) { ?>											
              <div class="item <?=$idx==0?'active':''?>">												
                <img src="<?=base_url('media_library/image_sliders/'.$row->image);?>" class="img-responsive">																							
              </div>											
              <?php $idx++; } ?>										
            </div>							
          </div>							
          <?php } ?>
</section>
<!--end of sambutan -->
<!--recent news-->
<section class="recent">
  <div class="container">
      <!-- Recent Posts -->	
        <?php 	$query = get_recent_posts(4); if ($query->num_rows() > 0) { 		$posts = [];		foreach ($query->result() as $post) {			array_push($posts, $post);		}	?>	
        <!-- Title -->	
        <div class="breadcrumb recent-post">		
      
            <i class="fa fa-sign-out">
            </i> Berita PALUTA Terbaru
        </div>	
        <div class="row">				
            <?php if (count(array_slice($posts, 0, 1)) > 0) { ?>					
            <?php foreach(array_slice($posts, 0, 4) as $row) { ?>
              <div class="col-md-3 col-xs-12">				
                <div class="recent-thumbnail">						
                <img src="<?=base_url('media_library/posts/medium/'.$row->post_image)?>">						
                <div class="caption">	
                  <div class="entry-header-title">						
                  <h4>
                    <a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>">
                      <?=$row->post_title?>
                    </a>
                  </h4>		
                  </div>					
                  <!-- <p class="by-author"> 
                  | oleh 
                    <?=$row->post_author?>
                  </p>							 -->
                  <div class="isi-recent-news">
                    <p align="justify">
                      <?=substr(strip_tags($row->post_content), 0, 165)?>
                    </p>
                  </div>							
                  <p>								
                    <a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>" class="btn btn-success btn-sm" role="button">Selengkapnya 
                      <i class="fa fa-angle-double-right" aria-hidden="true">
                      </i>
                    </a>							
                  </p>						
                </div>					
              </div>
            </div>				
            <?php } ?>			
            <?php } ?>
            </div>
            <div class="btn-another">
            <a href="#" class="btn btn-success">Lihat Semua Berita</a>
            </div>
  <?php } ?>	
  <!-- End Recent Posts -->		
  </div>
</section>

<section class="aplikasi">
  <div class="container">	
        <div class="breadcrumb recent-post">	
            <i class="fa fa-sign-out">
            </i> Aplikasi Kabupaten Padang Lawas Utara
        </div>			
          <!-- <span class="pull-right">
            <a href="<?=site_url('aplikasi.html')?>">
              <i class="fa fa-plus">
              </i>
            </a>
          </span>						
        </ol> -->
        <div id="aplikasi">		
          <?php $query = get_banners(3); if ($query->num_rows() > 0) { ?>	
          <div class="row">		
            <?php foreach($query->result() as $row) { ?>		
            <div class="col-md-2">			
              <a href="<?=$row->url?>" title="<?=$row->title?>" class="thumbnail"> 
                <img src="<?=base_url('media_library/banners/'.$row->image)?>" style="width: 100%; display: block;">
              </a>			
            </div>					
            <?php } ?>		
          </div>		
          <?php } ?>	
  </div> 
</section>

<!-- new block for twiter, berita kategori -->
<section class="photo">
  <div class="container">        
          <!-- Gallery Photo -->
          <?php $query = get_albums(4); if ($query->num_rows() > 0) { ?>	
          <div class="breadcrumb recent-post">		
         
            <i class="fa fa-camera">
            </i> PHOTO & VIDEO TERBARU
          		
          <span class="pull-right">
            <a href="<?=site_url('gallery-photo')?>">
              <i class="fa fa-plus">
              </i>
            </a>
          </span>	
          </div>
        
        <div class="row">		
          <?php foreach($query->result() as $row) { ?>		
              <div class="col-md-3 col-xs-12">			
                <div class="thumbnail">				
                <img onclick="preview(<?=$row->id?>)" src="<?=base_url('media_library/albums/'.$row->album_cover)?>">				
                  <div class="gal-depan">					
                    <h4>
                      <?=$row->album_title?>
                    </h4>			
                  </div>			
                </div>		
              </div>		
            <?php } ?>	
          </div>	
          <?php } ?>
          <!-- End Gallery Photo -->
  </div>
</section>

<section class="home-video">
  <div class="container">
      <!-- Gallery Video -->	
          <?php $query = get_recent_video(4); if ($query->num_rows() > 0) { ?>	
            <!-- <div class="breadcrumb recent-post">		
                <i class="fa fa-film">
                </i> VIDEO TERBARU
              <span class="pull-right">
                <a href="<?=site_url('gallery-video')?>">
                  <i class="fa fa-plus">
                  </i>
                </a>
              </span>	
          </div>	 -->
          <div class="row">		
            <?php foreach($query->result() as $row) { ?>		
            <div class="col-md-3">			
              <div class="video" style="width: 100%; display: block;">				
                <?=$row->post_content?>				
              </div>		
            </div>		
            <?php } ?>		
          </div>	
          <?php } ?>	
    <!-- End Gallery Video -->	
  </div>
</section>
<section class="map">	
      <!-- <div class="breadcrumb recent-post no-batas">			 
          <i class="fa fa-sign-out">
          </i> Peta Padang Lawas Utara
      </div>			 -->
      <iframe src="https://www.google.com/maps/d/embed?mid=1upqipQUWNoQ3AggvUINvA3w992A" width="100%" height="650">
      </iframe>
</section>