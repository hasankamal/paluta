<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">		
  <head>		
    <title>
      <?=$this->session->userdata('school_name')?>
    </title>		
    <meta charset="utf-8" />		
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />		
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />		
    <meta name="keywords" content="<?=$this->session->userdata('meta_keywords');?>"/>		
    <meta name="description" content="<?=$this->session->userdata('meta_description');?>"/>		
    <meta name="subject" content="Situs Kabupaten Padang Lawas Utara">		
    <meta name="copyright" content="<?=$this->session->userdata('school_name')?>">		
    <meta name="language" content="Indonesia">		
    <meta name="robots" content="index,follow" />	
    <meta name="google-site-verification" content="10vTDj8myQOpZw2JW4eIS6kAE1waA1cdw250oflCwN8" />
    <meta name="revised" content="Sunday, July 18th, 2010, 5:15 pm" />		
    <meta name="Classification" content="Kabupaten Padang Lawas Utara">		
    <meta name="author" content="M. Sahidin, msahidin22@gmail.com">		
    <meta name="designer" content="M. Sahidin, msahidin22@gmail.com">		
    <meta name="reply-to" content="msahidin22@gmail.com">		
    <meta name="owner" content="M. Sahidin">		
    <meta name="url" content="https://padanglawasutarakab.go.id/">		
    <meta name="identifier-URL" content="https://padanglawasutarakab.go.id/">		
    <meta name="category" content="Admission, Education">		
    <meta name="coverage" content="Worldwide">		
    <meta name="distribution" content="Global">		
    <meta name="rating" content="General">		
    <meta name="revisit-after" content="7 days">		
    <meta http-equiv="Expires" content="0">		
    <meta http-equiv="Pragma" content="no-cache">		
    <meta http-equiv="Cache-Control" content="no-cache">		
    <meta http-equiv="Copyright" content="<?=$this->session->userdata('school_name');?>" />		
    <meta http-equiv="imagetoolbar" content="no" />		
    <meta name="revisit-after" content="7" />		
    <meta name="webcrawlers" content="all" />		
    <meta name="rating" content="general" />		
    <meta name="spiders" content="all" />		
    <meta itemprop="name" content="<?=$this->session->userdata('school_name');?>" />		
    <meta itemprop="description" content="<?=$this->session->userdata('meta_description');?>" />		
    <meta itemprop="image" content="<?=base_url('media_library/images/'. $this->session->userdata('logo'));?>" />		
    <meta name="viewport" content="width=device-width, initial-scale=1" />		
    <meta name="csrf-token" content="<?=$this->session->userdata('csrf_token')?>">		
    <link rel="icon" href="<?=base_url('media_library/images/'.$this->session->userdata('favicon'));?>">		
    <?=link_tag('views/themes/cosmo/css/bootstrap.css')?>		
    <?=link_tag('views/themes/cosmo/css/font-awesome.min.css')?>		
    <?=link_tag('views/themes/cosmo/css/sm-core-css.css')?>		
    <?=link_tag('views/themes/cosmo/css/jquery.smartmenus.bootstrap.css')?>			
    <?=link_tag('assets/css/toastr.css');?>		
    <?=link_tag('assets/css/jssocials.css');?>		
    <?=link_tag('assets/css/jssocials-theme-flat.css');?>		
    <?=link_tag('assets/css/bootstrap-datepicker.css');?>				
    <?=link_tag('views/themes/cosmo/css/style.css')?>		
    <script type="text/javascript">    		const _BASE_URL = '<?=base_url();?>';
    </script>	 	
    <script src="<?=base_url('assets/js/frontend.min.js');?>">
    </script>				
    <script src="<?=base_url('assets/js/gpr-widget-kominfo.min.js');?>">
    </script>
    <!--[if lt IE 9]>      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->	
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107954945-1">
    </script>
    <script>  window.dataLayer = window.dataLayer || [];
      function gtag(){
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'UA-107954945-1');
    </script>	
  </head>	
  <body>
  <header id="head">
          <!-- Image Slider 				
          <?php $query = get_image_sliders(); if ($query->num_rows() > 0) { ?>													
          <div id="image-slider" class="carousel slide" data-ride="carousel">										
            <div class="carousel-inner" role="listbox">											
              <?php $idx = 0; foreach($query->result() as $row) { ?>											
              <div class="item <?=$idx==0?'active':''?>">												
                <img src="<?=base_url('media_library/image_sliders/'.$row->image);?>" class="img-responsive">																							
              </div>											
              <?php $idx++; } ?>										
            </div>							
          </div>							
          <?php } ?>
     End Image Slider -->
         <!-- Top Nav -->		
      <nav class="navbar navbar-inverse fixed-top">			
      <div class="container">				
        <div class="navbar-header">
        <div class="header-logo">				
                <a href="<?=base_url()?>">
                  <img src="<?=base_url('media_library/images/'.$this->session->userdata('logo'))?>" alt="" class="img-responsive">
                </a>
          </div>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">						
            <span class="sr-only">Toggle navigation
            </span>						
            <span class="icon-bar">
            </span>						
            <span class="icon-bar">
            </span>						
            <span class="icon-bar">
            </span>					
          </button>				
        </div>				
        <div id="navbar" class="navbar-collapse collapse">		
          <div class="header-logo">				
                <a href="<?=base_url()?>">
                  <img src="<?=base_url('media_library/images/'.$this->session->userdata('logo'))?>" alt="" class="img-responsive">
                </a>
          </div>			
          <ul class="col-md-9 col-xs-12 nav navbar-nav pull-right">						
            <li>
              <a href="<?=base_url()?>">
                <i class="fa fa-home">
                </i>
              </a>
            </li>						
            <?php 						foreach ($menus as $menu) {							echo '<li>';							$sub_nav = recursive_list($menu['child']);							$url = base_url() . $menu['menu_url'];							if ($menu['menu_type'] == 'links') {								$url = $menu['menu_url'];							}							echo anchor($url, strtoupper($menu['menu_title']). ($sub_nav ? ' <span class="caret"></span>':''), 'target="'.$menu['menu_target'].'"');							if ($sub_nav) {								echo '<ul class="dropdown-menu">';								echo recursive_list($menu['child']);								echo '</ul>';							}							echo '</li>';						}?>					
          </ul>					
        </div>			
      </div>		
    </nav>
              </Header>
    <div class="main">
          <div class="content">					
            <!-- Content -->							
              <!-- Right Content -->						
              <?php $this->load->view($content)?>				
          </div>		
        <!-- /container -->
    </div>	
        <!-- FOOTER -->				
        <div class="footer">	 		
          <div class="container">			
            <div class="row">				
              <div class="col-md-3">				
                <h4>TAUTAN
                </h4>				 		
                <ul>			 					
                  <?php                     			$links = get_links();     		if ($links->num_rows() > 0) {    		foreach($links->result() as $row) {    		echo '<li>'. anchor($row->url, $row->title, ['target' => $row->target]) . '</li>';                     	}                      	}                    	?>										
                </ul>									
              </div>									
              <div class="col-md-3">										
                <h4>KATEGORI
                </h4>					 					
                <ul>					 						
                  <?php                      				$query = get_post_categories();				if ($query->num_rows() > 0) {  				foreach($query->result() as $row) {       				echo '<li>'.anchor(site_url('category/'.$row->slug), $row->category, ['title' => $row->description]).'</li>';    				}                     }                     ?>									
                </ul>							
              </div>						
              <div class="col-md-3">				
                <h4>TAGS
                </h4>				
                <ul>				
                  <?php                  						$query = get_tags();				if ($query->num_rows() > 0) {					foreach ($query->result() as $row) { 					echo '<li>'.anchor(site_url('tag/'.$row->slug), $row->tag).'</li>';					}                    	}                    	?>					
                </ul>					
              </div>					
              <div class="col-md-3">					
                <h4>HUBUNGI KAMI
                </h4> 					
                <p>                  					
                  <?=$this->session->userdata('street_address')?>					
                  <br>					Email : 
                  <?=$this->session->userdata('email')?>
                  <br>					Telp : 
                  <?=$this->session->userdata('phone')?>
                  <br>					Fax : 
                  <?=$this->session->userdata('fax')?> 					
                </p>										
              </div>		  							
            </div>	 							
          </div>					
        </div>							
        <!-- END FOOTER -->					
        <!-- COPYRIGHT -->					
        <div class="copyright">
          <div class="container">
            <div class="col-xs-12 row">	
              <div class="col-xs-3 row sosial">
              <ul class="social-network social-circle pull-left">		
                <li>
                  <a href="<?=$this->session->userdata('facebook')?>" title="Facebook">
                    <i class="fa fa-facebook">
                    </i>
                  </a>
                </li>        
                <li>
                  <a href="<?=$this->session->userdata('twitter')?>" title="Twitter">
                    <i class="fa fa-twitter">
                    </i>
                  </a>
                </li>				         
                <li>
                  <a href="<?=$this->session->userdata('youtube')?>" title="Youtube">
                    <i class="fa fa-youtube">
                    </i>
                  </a>
                </li>        
                <li>
                  <a href="<?=$this->session->userdata('instagram')?>" title="Instagram">
                    <i class="fa fa-instagram">
                    </i>
                  </a>
                </li>
              </ul>
                </div>
                <div class="col-md-6 col-xs-12 copy">
                      <p>
                       <?=copyright(2017, base_url(), $this->session->userdata('school_name'))?>
                      </p>					
                    <p>Developed by 
                      <a href="https://padanglawasutarakab.go.id">padanglawasutarakab.go.id
                    </a>
                    </p>
                </div>
                <div class="col-md-3 col-xs-12 sosial">
                </div>
                </div>
                </div>				
        </div>							
        <!-- END COPYRIGHT -->					
        <!-- Back to Top -->									
        <a href="javascript:" id="return-to-top">
          <i class="fa fa-angle-double-up">
          </i>
        </a>					
        <script>					// Scroll Top			$(window).scroll(function() {						if ($(this).scrollTop() >= 50) {							$('#return-to-top').fadeIn(200);							} else {								$('#return-to-top').fadeOut(200);								}			});								$('#return-to-top').click(function() {									$('body,html').animate({										scrollTop : 0			 	}, 500);			});										</script>										
        <div id="fb-root">
        </div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.8&appId=640211006121837";
            fjs.parentNode.insertBefore(js, fjs);
          }
                 (document, 'script', 'facebook-jssdk'));
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107954945-1">
        </script>
        <script>  window.dataLayer = window.dataLayer || [];
          function gtag(){
            dataLayer.push(arguments);
          }
          gtag('js', new Date());
          gtag('config', 'UA-107954945-1');
        </script>
        </body>						
      </html>	
