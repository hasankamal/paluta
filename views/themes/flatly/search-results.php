<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="col-xs-12 col-md-9">
	<ol class="breadcrumb post-header">
		<li><?=$title?></li>
	</ol>
	<?php if ($posts && $posts->num_rows() > 0) { ?>
		<?php foreach($posts->result() as $row) { ?>
			<div class="panel panel-inverse" style="margin-bottom: -30px;">
				<div class="panel-heading" style="padding-bottom: 0px">
					<h4><a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>"><?=$row->post_title?></a></h4>
				</div>
				<div class="panel-body" style="padding-top: 0px">
					<p style="text-align: justify;"><?=word_limiter(strip_tags($row->post_content), 30)?></p>
				</div>
			</div>
		<?php } ?>
	<?php } ?>
	<?php if ($pages && $pages->num_rows() > 0) { ?>
		<?php foreach($pages->result() as $row) { ?>
			<div class="panel panel-inverse" style="margin-bottom: -30px;">
				<div class="panel-heading" style="padding-bottom: 0px">
					<h4><a href="<?=site_url('read/'.$row->id.'/'.$row->post_slug)?>"><?=$row->post_title?></a></h4>
				</div>
				<div class="panel-body" style="padding-top: 0px">
					<p style="text-align: justify;"><?=word_limiter(strip_tags($row->post_content), 30)?></p>
				</div>
			</div>
		<?php } ?>
	<?php } ?>
</div>
<?php $this->load->view('themes/flatly/sidebar')?>