<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('backend/grid_index');?>
<script type="text/javascript">
    var _grid = 'OPTIONS', 
        _form1 = _grid + '_FORM1',
        _form2 = _grid + '_FORM2',
        _form3 = _grid + '_FORM3',
        _form4 = _grid + '_FORM4',
        _form5 = _grid + '_FORM5',
        _form6 = _grid + '_FORM6',
        _form7 = _grid + '_FORM7',     
        _form8 = _grid + '_FORM8',
        _form9 = _grid + '_FORM9',
        _form10 = _grid + '_FORM10',
        _form11 = _grid + '_FORM11',
        _form12 = _grid + '_FORM12',
        _form13 = _grid + '_FORM13',
        _form14 = _grid + '_FORM14',
        _form15 = _grid + '_FORM15',
        _form16 = _grid + '_FORM16',
        _form17 = _grid + '_FORM17',
        _form18 = _grid + '_FORM18',
        _form19 = _grid + '_FORM19',
        _form20 = _grid + '_FORM20',
        _form21 = _grid + '_FORM21',
        _form22 = _grid + '_FORM22',
        _form23 = _grid + '_FORM23',    
        _form24 = _grid + '_FORM24',
        _form25 = _grid + '_FORM25';
	new GridBuilder( _grid , {
        controller:'settings/school_profile',
        fields: [
            {
                header: '<i class="fa fa-cog"></i>', 
                renderer:function(row) {
                    if (row.variable == 'npsn') {
                        return A(_form1 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'school_name') {
                        return A(_form2 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'tagline') {
                        return A(_form8 + '.OnEdit(' + row.id + ')');
                    }                    
                    if (row.variable == 'logo') {
                        return UPLOAD(_form9 + '.OnUpload(' + row.id + ')', 'image', 'Upload Logo');
                    }
                    if (row.variable == 'sub_district') {
                        return A(_form14 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'district') {
                        return A(_form15 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'postal_code') {
                        return A(_form16 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'street_address') {
                        return A(_form17 + '.OnEdit(' + row.id + ')');
                    } 
                    if (row.variable == 'latitude') {
                        return A(_form18 + '.OnEdit(' + row.id + ')');
                    } 
                    if (row.variable == 'longitude') {
                        return A(_form19 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'phone') {
                        return A(_form20 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'fax') {
                        return A(_form21 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'email') {
                        return A(_form22 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'website') {
                        return A(_form23 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'headmaster') {
                        return A(_form24 + '.OnEdit(' + row.id + ')');
                    }
                    if (row.variable == 'headmaster_photo') {
                        return UPLOAD(_form25 + '.OnUpload(' + row.id + ')', 'image', 'Upload Photo Bupati');
                    }
                },
                exclude_excel : true,
                sorting: false
            },
            { 
                header: '<i class="fa fa-search-plus"></i>', 
                renderer:function(row) {
                    if (row.variable == 'logo' || row.variable == 'headmaster_photo') {
                        var image = "'" + row.value + "'";
                        return row.value ? 
                            '<a title="Preview" onclick="preview(' + image + ')"  href="#"><i class="fa fa-search-plus"></i></a>' : '';
                    }
                },
                sorting: false
            },
    		{ header:'Setting Name', renderer: 'description' },
            { 
                header:'Setting Value', 
                renderer: function(row){
                    if (row.variable == 'school_level') {
                        return row.value ? DS.SchoolLevel[ row.value ] : '';
                    }
                    if (row.variable == 'school_status') {
                        return row.value ? DS.SchoolStatus[ row.value ] : '';
                    }
                    if (row.variable == 'ownership_status') {
                        return row.value ? DS.OwnershipStatus[ row.value ] : '';
                    }
                    return row.value ? row.value : '';
                },
                sorting: false
            }
    	],
        can_add: false,
        can_delete: false,
        can_restore: false,
        reduce_column: 3,
        per_page: 50,
        per_page_options: [50, 100]
    });

    /**
     * School Name
     */
    new FormBuilder( _form2 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Nama Kabupaten', name:'value' }
        ]
    });

    /**
     * meta_keywords
     */
    new FormBuilder( _form5 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Status Kepemilikan', name:'value', type:'select', datasource:DS.OwnershipStatus }
        ]
    });

    /**
     * Tagline
     */
    new FormBuilder( _form8 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Slogan', name:'value', placeholder:'Slogan' }
        ]
    });

    /**
     * Logo
     */
    new FormBuilder( _form9 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Logo', name:'value' }
        ]
    });

    /**
     * district
     */
    new FormBuilder( _form15 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Kabupaten', name:'value' }
        ]
    });

    /**
     * postal_code
     */
    new FormBuilder( _form16 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Kode Pos', name:'value' }
        ]
    });
    
    /**
     * street_address
     */
    new FormBuilder( _form17 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Alamat Jalan', name:'value' }
        ]
    });

    /**
     * latitude
     */
    new FormBuilder( _form18 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Latitude', name:'value' }
        ]
    });

    /**
     * longitude
     */
    new FormBuilder( _form19 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Longitude', name:'value' }
        ]
    });

    /**
     * phone
     */
    new FormBuilder( _form20 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Telepon', name:'value' }
        ]
    });

    /**
     * Fax
     */
    new FormBuilder( _form21 , {
        controller:'settings/school_profile',
        fields: [
            { label:'Fax', name:'value' }
        ]
    });

    /**
     * email
     */
    new FormBuilder( _form22, {
        controller:'settings/school_profile',
        fields: [
            { label:'Email', name:'value' }
        ]
    });

    /**
     * website
     */
    new FormBuilder( _form23, {
        controller:'settings/school_profile',
        fields: [
            { label:'Website', name:'value' }
        ]
    });

    /**
     * Kepala Sekolah
     */
    new FormBuilder( _form24, {
        controller:'settings/school_profile',
        fields: [
            { label:'Nama Bupati', name:'value' }
        ]
    });

    /**
     * Photo Kepala Sekolah
     */
    new FormBuilder( _form25, {
        controller:'settings/school_profile',
        fields: [
            { label:'Photo Bupati', name:'value' }
        ]
    });

    /**
     * Preview Image
     */
    function preview(image) {
        $.magnificPopup.open({
          items: {
            src: '<?=base_url()?>media_library/images/' + image
          },
          type: 'image'
        });
    }
</script>