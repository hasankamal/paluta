<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Sidebar -->
<div class="col-xs-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
	  			<h3 class="panel-title">Bupati & Wakil Bupati</h3>
			</div>
			</div>
	<?php if ($this->uri->segment(1) != 'sambutan-bupati') { ?>
	<div class="thumbnail">
		<img src="<?=base_url('media_library/images/').$this->session->userdata('headmaster_photo');?>" alt="..." style="width: 100%">
  	</div>
  	<?php } ?>
  	
  	<div class="form-group has-feedback">
		<input onkeydown="if (event.keyCode == 13) { subscriber(); return false; }" type="text" class="form-control" id="subscriber" placeholder="Berlangganan" autocomplete="off">
		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	</div>

	<div id="gpr-kominfo-widget-container"></div>
&nbsp;	
	<?php $query = get_post_categories(); if ($query->num_rows() > 0) { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
	  			<h3 class="panel-title">KATEGORI</h3>
			</div>
			<div class="list-group">
				<?php foreach($query->result() as $row) { ?>
				<a href="<?=site_url('category/'.$row->slug);?>" title="<?=$row->description;?>" class="list-group-item"><?=$row->category;?></a>
	        <?php } ?>
			</div>
		</div>
	<?php } ?>

	<?php $query = get_links(); if ($query->num_rows() > 0) { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
	  			<h3 class="panel-title">TAUTAN</h3>
			</div>
			<div class="list-group">
				<?php foreach($query->result() as $row) { ?>
				<a href="<?=$row->url;?>" title="<?=$row->title;?>" target="<?=$row->target;?>" class="list-group-item"><?=$row->title;?></a>
	        <?php } ?>
			</div>
		</div>
	<?php } ?>
	<?php $query = get_active_question(); if ($query) { ?>
	<div class="panel panel-default">
		<div class="panel-heading">
  			<h3 class="panel-title">JAJAK PENDAPAT</h3>
		</div>
		<div class="panel-body">
			<p><?=$query->question?></p>
			<?php $options = get_answers($query->id); foreach($options->result() as $option) { ?>
			<div class="radio">
			  <label>
			    <input type="radio" name="answer_id" id="answer_id" id="answer_id_<?=$option->id?>" value="<?=$option->id?>">
			    <?=$option->answer?>
			  </label>
			</div>
			<?php } ?>
			<div class="btn-group">
				<button type="submit" onclick="polling(); return false;" class="btn btn-success btn-sm"><i class="fa fa-send"></i> SUBMIT</button>
				<a href="<?=site_url('hasil-jajak-pendapat')?>" class="btn btn-sm btn-warning"><i class="fa fa-bar-chart"></i> LIHAT HASIL</a>		
			</div>			
		</div>
	</div>
	<?php } ?>
	

	<a href="https://smscenter.padanglawasutarakab.go.id/index.php/login" title="SMS CENTER" target="_blank" class="thumbnail"> <img src="https://padanglawasutarakab.go.id/media_library/banners/sms-center.jpg" style="width: 100%; display: block;"></a>		
<center>
<a href="https://info.flagcounter.com/Ouzl"><img src="https://s04.flagcounter.com/count2/Ouzl/bg_FFFFFF/txt_000000/border_CCCCCC/columns_3/maxflags_12/viewers_0/labels_0/pageviews_1/flags_0/percent_0/" alt="Flag Counter" border="0"></a>	
	</center>
&nbsp;
<div class="fb-page" data-href="https://www.facebook.com/PemkabPadangLawasUtara/" data-tabs="timeline, messages, share," data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/PemkabPadangLawasUtara/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/PemkabPadangLawasUtara/">Pemkab Padang Lawas Utara</a></blockquote></div>
	</div>